//
//  ViewController.swift
//  kiddos
//
//  Created by mac on 11/04/22.
//

import UIKit

class ViewController: UIViewController {
    
    //MARK: - Viewlifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let param: [String:Any] = ["theme_id": APP_THEMEID]
        getextraURL(param)
    }
    
    //MARK: - IBActions
    
    @IBAction func onclickLoginButton(_ sender: UIButton) {
        let loginVC = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        navigationController?.pushViewController(loginVC, animated: true)
    }
    
    @IBAction func onclickSignupButton(_ sender: UIButton) {
        let siginVC = storyboard?.instantiateViewController(withIdentifier: "SigninVC") as! SigninVC
        navigationController?.pushViewController(siginVC, animated: true)
    }
    
    @IBAction func onclickContinuasGuest(_ sender: UIButton) {
        UserDefaults.standard.set("", forKey: userDefaultsKeys.KEY_USERID)
        UserDefaults.standard.set("", forKey: userDefaultsKeys.KEY_APP_TOKEN)
        let cartdata = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
        if cartdata == "" {
            setValueToUserDefaults(value: "" as AnyObject, key: userDefaultsKeys.KEY_SAVED_CART)
        }
        
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "MainTabbarVC") as! MainTabbarVC
        navigationController?.pushViewController(vc, animated: true)
    }
    //MARK: APIFunctios
    func getextraURL(_ param: [String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_Extra, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 9{
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                print("data IS not Found")
                            }
                            break
                        }
                    }else if let data = json["data"] as? [String:Any]{
                        let terms = data["terms"] as? String
                        UserDefaults.standard.set(terms, forKey: userDefaultsKeys.KEY_TERMS)
                        let contectus = data["contact_us"] as? String
                        UserDefaults.standard.set(contectus, forKey: userDefaultsKeys.KEY_CONTECTUS)
                        let returnpolicy = data["return_policy"] as? String
                        UserDefaults.standard.set(returnpolicy, forKey: userDefaultsKeys.KEY_RETURNPOLICY)
                        let insta = data["insta"] as? String
                        UserDefaults.standard.set(insta, forKey: userDefaultsKeys.KEY_INSTA)
                        let youtube = data["youtube"] as? String
                        UserDefaults.standard.set(youtube, forKey: userDefaultsKeys.KEY_YOUTUVBE)
                        let massanger = data["messanger"] as? String
                        UserDefaults.standard.set(massanger, forKey: userDefaultsKeys.KEY_MASSANGER)
                        let twitter = data["twitter"] as? String
                        UserDefaults.standard.set(twitter, forKey: userDefaultsKeys.KEY_TWITTER)
                    }
                    
//                    print(json)
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
    }
    }
}

