import Foundation

// MARK: - Welcome
struct HeaderContent: Codable {
    let status: Int
    let message: String
    let data: DataClass
}

// MARK: - DataClass
struct DataClass: Codable {
    let themJSON: ThemJSON

    enum CodingKeys: String, CodingKey {
        case themJSON = "them_json"
    }
}

// MARK: - ThemJSON
struct ThemJSON: Codable {
    let homepageHeader: HomepageHeader
    let homepagePromotions: HomepagePromotions
    let homepageProducts: HomepageProducts
    let homepageCategories: HomepageCategories
    let homepageBestsellers: HomepageBestsellers
    let homepageLoyaltyProgram: HomepageLoyaltyProgram

    enum CodingKeys: String, CodingKey {
        case homepageHeader = "homepage-header"
        case homepagePromotions = "homepage-promotions"
        case homepageProducts = "homepage-products"
        case homepageCategories = "homepage-categories"
        case homepageBestsellers = "homepage-bestsellers"
        case homepageLoyaltyProgram = "homepage-loyalty-program"
    }
}

// MARK: - HomepageBestsellers
struct HomepageBestsellers: Codable {
    let homepageBestsellersHeading: String

    enum CodingKeys: String, CodingKey {
        case homepageBestsellersHeading = "homepage-bestsellers-heading"
    }
}

// MARK: - HomepageCategories
struct HomepageCategories: Codable {
    let homepageCategoriesHeading: String

    enum CodingKeys: String, CodingKey {
        case homepageCategoriesHeading = "homepage-categories-heading"
    }
}

// MARK: - HomepageHeader
struct HomepageHeader: Codable {
    let homepageHeaderTitle, homepageHeaderSubText, homepageHeaderImage, homepageHeaderButton: String

    enum CodingKeys: String, CodingKey {
        case homepageHeaderTitle = "homepage-header-title"
        case homepageHeaderSubText = "homepage-header-sub-text"
        case homepageHeaderImage = "homepage-header-image"
        case homepageHeaderButton = "homepage-header-button"
    }
}

// MARK: - HomepageLoyaltyProgram
struct HomepageLoyaltyProgram: Codable {
    let homepageLoyaltyProgramTitle, homepageLoyaltyProgramSubText, homepageLoyaltyProgramBgImage, homepageLoyaltyProgramButton: String

    enum CodingKeys: String, CodingKey {
        case homepageLoyaltyProgramTitle = "homepage-loyalty-program-title"
        case homepageLoyaltyProgramSubText = "homepage-loyalty-program-sub-text"
        case homepageLoyaltyProgramBgImage = "homepage-loyalty-program-bg-image"
        case homepageLoyaltyProgramButton = "homepage-loyalty-program-button"
    }
}

// MARK: - HomepageProducts
struct HomepageProducts: Codable {
    let homepageProductsHeading: String

    enum CodingKeys: String, CodingKey {
        case homepageProductsHeading = "homepage-products-heading"
    }
}

// MARK: - HomepagePromotions
struct HomepagePromotions: Codable {
    let homepagePromotionsIconImage: [String]

    enum CodingKeys: String, CodingKey {
        case homepagePromotionsIconImage = "homepage-promotions-icon-image"
    }
}

