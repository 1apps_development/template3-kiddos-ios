//
//  OrderPlacedVC.swift
//  kiddos
//
//  Created by mac on 19/04/22.
//

import UIKit

class OrderPlacedVC: UIViewController {
    
    //MARK: - Variables
    
    var ordercomTitle = String()
    var ordercomDescription = String()
    var parentVC: UIViewController!

    //MARK: - Outlets
    
    @IBOutlet weak var placeOrderTitle: UILabel!
    @IBOutlet weak var placeorderDescription: UILabel!
    
    //MARK: - ViewLifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        placeOrderTitle.text = ordercomTitle
        placeorderDescription.text = ordercomDescription
        // Do any additional setup after loading the view.
    }
    
    //MARK: - Actions
    @IBAction func onClickBack(_ sender: Any){
        dismiss(animated: true) {
            
            UserDefaults.standard.set([[String:Any]](), forKey: userDefaultsKeys.KEY_GESTUSEROBJ)
            UserDefaults.standard.set([[String:Any]](), forKey: userDefaultsKeys.KEY_GESTUSERPRODUCTARRAY)
            UserDefaults.standard.set([[String:Any]](), forKey: userDefaultsKeys.KEY_GESTTEXTARRAY)
            UserDefaults.standard.set([:], forKey: userDefaultsKeys.KEY_GESTTEXTARRAY)
            UserDefaults.standard.set([:], forKey: userDefaultsKeys.KEY_CUPENOBJECT)
            UserDefaults.standard.set([:], forKey: userDefaultsKeys.KEY_BILLINGOBJ)
            
            UserDefaults.standard.set("", forKey: userDefaultsKeys.KEY_PAYMENTTYPE)
            UserDefaults.standard.set("", forKey: userDefaultsKeys.KEY_PAYMENTDESCRIPTION)
            UserDefaults.standard.set("", forKey: userDefaultsKeys.KEY_DELIVARYID)
            UserDefaults.standard.set("", forKey: userDefaultsKeys.KEY_DELIVARYDESCRIPTION)
            UserDefaults.standard.set("", forKey: userDefaultsKeys.KEY_SAVED_CART)
            
            let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "MainTabbarVC") as! MainTabbarVC
//            vc.catid = self.arrMenuItems[indexPath.row].id
            self.parentVC.navigationController?.pushViewController(vc, animated: true)
            
//            let objVC = MainstoryBoard.instantiateViewController(withIdentifier: "BestsellersVC") as! BestsellersVC
//            let TabViewController = MainstoryBoard.instantiateViewController(withIdentifier: "MainTabbarVC") as! MainTabbarVC
//            let appNavigation: UINavigationController = UINavigationController(rootViewController: objVC)
//            appNavigation.setNavigationBarHidden(true, animated: true)
//            keyWindow?.rootViewController = TabViewController
        }
    }

}
