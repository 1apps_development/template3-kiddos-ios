//
//  MenuViewController.swift
//  Taskly
//
//  Created by mac on 09/03/22.
//

import UIKit
import SOTabBar
import Alamofire

protocol MenuOptionSelectDelegate: NSObject{
    func menuOptionSelected(_ title: String)
}

class MenuViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var tableItems: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btn_login: UIButton!
    
    //MARK: - Variables
    var mDelegate: MenuOptionSelectDelegate?
    var parentVC: UIViewController!
    var selectedItem: String!
    var arrMenuItems: [DataCategories] = []

    //MARK: - ViewlifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""  {
            self.btn_login.isHidden = false
            tableItems.register(UINib.init(nibName: "MenuTVCell", bundle: nil), forCellReuseIdentifier: "MenuTVCell")
            tableItems.tableFooterView = UIView()
            let param: [String:Any] = ["theme_id": APP_THEMEID]
            getNavigationData(param)
        }
        else {
            self.btn_login.isHidden = true
            tableItems.register(UINib.init(nibName: "MenuTVCell", bundle: nil), forCellReuseIdentifier: "MenuTVCell")
            tableItems.tableFooterView = UIView()
            let param: [String:Any] = ["theme_id": APP_THEMEID]
            getNavigationData(param)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.view.alpha = 1
    }
    
    //MARK: - IBActions
    
    @IBAction func onClickClose(_ sender: Any){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_login(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        let nav : UINavigationController = UINavigationController(rootViewController: objVC)
        nav.navigationBar.isHidden = true
        keyWindow?.rootViewController = nav
    }
    
    @IBAction func onclickShareButton(_ sender: UIButton) {
        if sender.tag == 1 {
            guard let url = URL(string: UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_YOUTUVBE)!) else {
                return
            }
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        else if sender.tag == 2 {
            guard let url = URL(string: UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_MASSANGER)!) else {
                return
            }
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }else if sender.tag == 3 {
            guard let url = URL(string: UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_INSTA)!) else {
                return
            }
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }else if sender.tag == 4 {
            guard let url = URL(string: UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_TWITTER)!) else {
                return
            }
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
    
    //MARK: - CustomFunctions
    
    //MARK: - APIFunctions
    func getNavigationData(_ param: [String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_Navigation, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let categories = json["data"] as? [[String:Any]]{
                               self.arrMenuItems = Navigation.init(categories).categories
                               self.tableItems.reloadData()
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let msg = json["data"] as! [String:Any]
                                let massage = msg["message"] as! String
                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
//                    print(json)
                    }
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
    }
    }
}

extension MenuViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMenuItems.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"MenuTVCell", for: indexPath) as! MenuTVCell
        cell.selectionStyle = .none
        cell.configureCell(arrMenuItems[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dismiss(animated: true) {
            let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "CategoriesDetailVC") as! CategoriesDetailVC
            vc.catid = self.arrMenuItems[indexPath.row].id!
            vc.isHome = "yes"
            vc.isMenu = "Menu"
            self.parentVC.navigationController?.pushViewController(vc, animated: true)
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
}
