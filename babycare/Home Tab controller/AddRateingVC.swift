//
//  AddRateingVC.swift
//  kiddos
//
//  Created by mac on 03/11/22.
//

import UIKit

protocol FeedbackDelegate {
    func refreshData(id:Int,rating_no:String,title:String,description:String)
}

class AddRateingVC: UIViewController, UITextViewDelegate {

    @IBOutlet weak var view_addrate: CosmosView!
    @IBOutlet weak var textViewAboutRateing: UITextView!
    @IBOutlet weak var txt_addYourWords: UITextField!
    
    var delegate: FeedbackDelegate!
    var product_id = Int()
    var rattingscount = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textViewAboutRateing.text = "Leave a message, if you want"
        self.textViewAboutRateing.textColor = UIColor.lightGray
        self.textViewAboutRateing.delegate = self
        view_addrate.didFinishTouchingCosmos = { rating in
            print(rating)
            self.rattingscount = "\(rating)"
        }
        // Do any additional setup after loading the view.
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.init(named: "App_bg_Color")
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Leave a message, if you want"
            textView.textColor = UIColor.lightGray
        }
    }
    
    
    @IBAction func onclickCancel(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func onclickRateNow(_ sender: UIButton) {
        if self.textViewAboutRateing.text == "Leave a message, if you want" || self.txt_addYourWords.text == "" || self.rattingscount == ""
        {
            let alertVC = UIAlertController(title: Bundle.main.applicationName, message: "Please enter message", preferredStyle: .alert)
            let yesAction = UIAlertAction(title: "okay", style: .default)
            alertVC.addAction(yesAction)
            self.present(alertVC,animated: true,completion: nil)
        }
        else{
            if self.textViewAboutRateing.text == "Leave a message, if you want"
            {
                self.textViewAboutRateing.text = ""
            }
            self.delegate.refreshData(id: self.product_id, rating_no: self.rattingscount, title: self.txt_addYourWords.text!, description: self.textViewAboutRateing.text!)
            self.navigationController?.popViewController(animated: false)
        }
    }
}
