//
//  GuestAddressVC.swift
//  kiddos
//
//  Created by mac on 13/10/22.
//

import UIKit
import iOSDropDown

class GuestAddressVC: UIViewController {
    
    //MARK: - Outlets
    
    @IBOutlet weak var txt_firstname: UITextField!
    @IBOutlet weak var txt_lastname: UITextField!
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_telephone: UITextField!
    @IBOutlet weak var txt_address: UITextField!
    @IBOutlet weak var txt_country: DropDown!
    @IBOutlet weak var txt_state: DropDown!
    @IBOutlet weak var txt_city: DropDown!
    @IBOutlet weak var txt_postcode: UITextField!
    
    @IBOutlet weak var btn_cheakBox: UIButton!
    @IBOutlet weak var txt_address1: UITextField!
    @IBOutlet weak var txt_saveaddressas: UITextField!
    
    @IBOutlet weak var delivaryAddressView: UIView!
    @IBOutlet weak var delivaryaddressHeght: NSLayoutConstraint!
    @IBOutlet weak var txt_countryDelivary: DropDown!
    @IBOutlet weak var txt_stateDelivary: DropDown!
    @IBOutlet weak var txt_cityDelivary: DropDown!
    @IBOutlet weak var txt_postcodeDelivary: UITextField!
    
    //MARK: - Variables
    
    var selectedCountryid = Int()
    var selectedStateid = Int()
    var selectedCityid = String()
    var ArrayCountry: [CountryLists] = []
    var ArraySatate: [StateLists] = []
    var ArrayCity: [CityLists] = []
    
    var selectedDelivaryCountryid = Int()
    var selectedDelivaryStateid = Int()
    var selectedDelivaryCityid = String()
    var ArrayDelivaryCountry: [CountryLists] = []
    var ArrayDelivarySatate: [StateLists] = []
    var ArrayDelivaryCity: [CityLists] = []
    
    //MARK: - Viewlifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delivaryAddressView.isHidden = true
        self.delivaryaddressHeght.constant = 0.0
        txt_postcode.delegate = self
        txt_postcodeDelivary.delegate = self
        let param: [String:Any] = ["theme_id": APP_THEMEID]
        getCountries(param)
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.btn_cheakBox.setImage(UIImage.init(named: "squareFill"), for: .normal)
    }
    //MARK: - Actions
    
    
    @IBAction func onclickBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onclickCheakBox(_ sender: UIButton) {
        if self.btn_cheakBox.imageView?.image == UIImage.init(named: "sqaure")
        {
            self.btn_cheakBox.setImage(UIImage.init(named: "squareFill"), for: .normal)
            self.delivaryAddressView.isHidden = true
            self.delivaryaddressHeght.constant = 0.0
        }
        else{
            self.btn_cheakBox.setImage(UIImage.init(named: "sqaure"), for: .normal)
            self.delivaryAddressView.isHidden = false
            self.delivaryaddressHeght.constant = 500.0
        }
    }
    
    @IBAction func onClickCountry(_ sender: DropDown){
        var countryName = [String]()
        for country in ArrayCountry{
            countryName.append(country.name!)
        }
        self.txt_country.textColor = hexStringToUIColor(hex: "#3E4F46")
        self.txt_country.checkMarkEnabled = false
        self.txt_country.optionArray = countryName
        self.txt_country.selectedRowColor = hexStringToUIColor(hex: "#B7D4C5")
        self.txt_country.rowBackgroundColor  = hexStringToUIColor(hex: "#B7D4C5")
        self.txt_country.didSelect(completion: { selected, index, id in
            self.txt_country.text = selected
            self.selectedCountryid = self.ArrayCountry[index].id!
            let param: [String:Any] = ["country_id": self.selectedCountryid,
                                       "theme_id": APP_THEMEID]
            self.getState(param)
               })
    }
    @IBAction func onClickState(_ sender: DropDown){
        var statename = [String]()
        for state in ArraySatate{
            statename.append(state.name!)
        }
        self.txt_state.textColor = hexStringToUIColor(hex: "#3E4F46")
        self.txt_state.checkMarkEnabled = false
        self.txt_state.optionArray = statename
        self.txt_state.selectedRowColor = hexStringToUIColor(hex: "#B7D4C5")
        self.txt_state.rowBackgroundColor  = hexStringToUIColor(hex: "#B7D4C5")
        self.txt_state.didSelect(completion: { selected, index, id in
            self.txt_state.text = selected
            self.selectedStateid = self.ArraySatate[index].id!
            let param: [String:Any] = ["state_id": self.selectedStateid,
                                       "theme_id": APP_THEMEID]
            self.getCity(param)
               })
    }
    @IBAction func onClickCity(_ sender: DropDown){
        var cityname = [String]()
        for city in ArrayCity{
            cityname.append(city.name!)
        }
        self.txt_city.textColor = hexStringToUIColor(hex: "#3E4F46")
        self.txt_city.checkMarkEnabled = false
        self.txt_city.optionArray = cityname
        self.txt_city.selectedRowColor = hexStringToUIColor(hex: "#B7D4C5")
        self.txt_city.rowBackgroundColor  = hexStringToUIColor(hex: "#B7D4C5")
        self.txt_city.didSelect(completion: { selected, index, id in
            self.txt_city.text = selected
//            self.selectedCountryid = self.ArrayCountry[index].id!
               })
    }
    
    @IBAction func onClickDelivaryCountry(_ sender: DropDown){
        var countryName = [String]()
        for country in ArrayDelivaryCountry{
            countryName.append(country.name!)
        }
        self.txt_countryDelivary.textColor = hexStringToUIColor(hex: "#3E4F46")
        self.txt_countryDelivary.checkMarkEnabled = false
        self.txt_countryDelivary.optionArray = countryName
        self.txt_countryDelivary.selectedRowColor = hexStringToUIColor(hex: "#B7D4C5")
        self.txt_countryDelivary.rowBackgroundColor  = hexStringToUIColor(hex: "#B7D4C5")
        self.txt_countryDelivary.didSelect(completion: { selected, index, id in
            self.txt_countryDelivary.text = selected
            self.selectedDelivaryCountryid = self.ArrayDelivaryCountry[index].id!
            let param: [String:Any] = ["country_id": self.selectedDelivaryCountryid,
                                       "theme_id": APP_THEMEID]
            self.getState(param)
               })
    }
    @IBAction func onClickDelivaryState(_ sender: DropDown){
        var statename = [String]()
        for state in ArrayDelivarySatate{
            statename.append(state.name!)
        }
        self.txt_stateDelivary.textColor = hexStringToUIColor(hex: "#3E4F46")
        self.txt_stateDelivary.checkMarkEnabled = false
        self.txt_stateDelivary.optionArray = statename
        self.txt_stateDelivary.selectedRowColor = hexStringToUIColor(hex: "#B7D4C5")
        self.txt_stateDelivary.rowBackgroundColor  = hexStringToUIColor(hex: "#B7D4C5")
        self.txt_stateDelivary.didSelect(completion: { selected, index, id in
            self.txt_stateDelivary.text = selected
            self.selectedDelivaryStateid = self.ArrayDelivarySatate[index].id!
            let param: [String:Any] = ["state_id": self.selectedDelivaryStateid,
                                       "theme_id": APP_THEMEID]
            self.getCity(param)
               })
    }
    @IBAction func onClickDelivaryCity(_ sender: DropDown){
        var cityname = [String]()
        for city in ArrayDelivaryCity{
            cityname.append(city.name!)
        }
        self.txt_cityDelivary.textColor = hexStringToUIColor(hex: "#3E4F46")
        self.txt_cityDelivary.checkMarkEnabled = false
        self.txt_cityDelivary.optionArray = cityname
        self.txt_cityDelivary.selectedRowColor = hexStringToUIColor(hex: "#B7D4C5")
        self.txt_cityDelivary.rowBackgroundColor  = hexStringToUIColor(hex: "#B7D4C5")
        self.txt_cityDelivary.didSelect(completion: { selected, index, id in
            self.txt_city.text = selected
//            self.selectedCountryid = self.ArrayCountry[index].id!
               })
    }
    
    
    @IBAction func onclickContinue(_ sender: UIButton) {
        
        if self.txt_firstname.text! == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter first name")
        }
        else if self.txt_lastname.text! == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter last name.")
        }
        if self.txt_email.text! == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter the email address.")
        }
        else if self.txt_telephone.text! == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter phone number.")
        }
        else if isValidEmail(self.txt_email.text!) == false
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter the valid email address.")
        }
        else if isValidEmail(self.txt_email.text!) == false
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter the valid email address.")
        }
        else if self.txt_address.text! == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter address.")
        }
       
        else if self.txt_country.text == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter country.")
        }
        else if self.txt_state.text == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter region / state.")
        }
        else if self.txt_city.text! == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter city.")
        }
        else if self.txt_postcode.text! == ""
        {
            showAlertMessage(titleStr: "", messageStr: "Please enter postcode.")
        }
        else{
            if self.btn_cheakBox.imageView?.image == UIImage(named: "squareFill"){
                UserDefaults.standard.set(self.txt_country.text, forKey: userDefaultsKeys.KEY_SELECTEDCOUNTRY)
                UserDefaults.standard.set(self.txt_state.text, forKey: userDefaultsKeys.KEY_SELECTEDSTATE)
                
                UserDefaults.standard.set(self.txt_countryDelivary.text, forKey: userDefaultsKeys.KEY_SELECTEDCOUNTRYDELIVARY)
                UserDefaults.standard.set(self.txt_stateDelivary.text, forKey: userDefaultsKeys.KEY_SELECTEDSTATEDELIVARY)
                
                let billingObj = ["firstname":self.txt_firstname.text!,"lastname":self.txt_lastname.text!,"email":self.txt_email.text!,"billing_user_telephone":self.txt_telephone.text!,"billing_address":self.txt_address.text!,"billing_postecode":self.txt_postcode.text!,"billing_country":self.selectedCountryid,"billing_state":self.selectedStateid,"billing_city":self.txt_city.text!,"delivery_address":self.txt_address.text!,"delivery_postcode":self.txt_postcode.text!,"delivery_country":self.selectedCountryid,"delivery_state":self.selectedStateid,"delivery_city":self.txt_city.text!] as [String : Any]
                UserDefaults.standard.set(billingObj, forKey: userDefaultsKeys.KEY_BILLINGOBJ)
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShippingVC") as! ShippingVC
                self.navigationController?.pushViewController(vc, animated: true)
                
            }else{
                if self.txt_saveaddressas.text! == ""
                {
                    showAlertMessage(titleStr: "", messageStr: "Please enter delivery address.")
                }
                else if self.txt_countryDelivary.text == ""
                {
                    showAlertMessage(titleStr: "", messageStr: "Please enter delivery country.")
                }
                else if self.txt_stateDelivary.text == ""
                {
                    showAlertMessage(titleStr: "", messageStr: "Please enter delivery region / state.")
                }
                else if self.txt_cityDelivary.text! == ""
                {
                    showAlertMessage(titleStr: "", messageStr: "Please enter delivery city.")
                }
                else if self.txt_postcodeDelivary.text! == ""
                {
                    showAlertMessage(titleStr: "", messageStr: "Please enter delivery postcode.")
                }
                else{
                    UserDefaults.standard.set(self.txt_country.text, forKey: userDefaultsKeys.KEY_SELECTEDCOUNTRY)
                    UserDefaults.standard.set(self.txt_state.text, forKey: userDefaultsKeys.KEY_SELECTEDSTATE)
                    
                    UserDefaults.standard.set(self.txt_countryDelivary.text, forKey: userDefaultsKeys.KEY_SELECTEDCOUNTRYDELIVARY)
                    UserDefaults.standard.set(self.txt_stateDelivary.text, forKey: userDefaultsKeys.KEY_SELECTEDSTATEDELIVARY)
                    
                    let delivaryObject = ["firstname":self.txt_firstname.text!,"lastname":self.txt_lastname.text!,"email":self.txt_email.text!,"billing_user_telephone":self.txt_telephone.text!,"billing_address":self.txt_address1.text!,"billing_postecode":self.txt_postcodeDelivary.text!,"billing_country":self.selectedDelivaryCountryid,"billing_state":self.selectedDelivaryStateid,"billing_city":self.txt_cityDelivary.text!,"delivery_address":self.txt_address1.text!,"delivery_postcode":self.txt_postcodeDelivary.text!,"delivery_country":self.selectedDelivaryCountryid,"delivery_state":self.selectedDelivaryStateid,"delivery_city":self.txt_cityDelivary.text!] as [String : Any]
                    
                    UserDefaults.standard.set(delivaryObject, forKey: userDefaultsKeys.KEY_BILLINGOBJ)
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShippingVC") as! ShippingVC
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    //MARK: - ApiFuctions
    func getCountries(_ param: [String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_Country, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [[String:Any]]{
                                self.ArrayCountry = Country.init(data).countrylists
                                self.ArrayDelivaryCountry = Country.init(data).countrylists
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
     }
    func getState(_ param: [String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_State, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [[String:Any]]{
                                self.ArraySatate = State.init(data).statelists
                                self.ArrayDelivarySatate = State.init(data).statelists
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
//                    print(json)
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
     }
    
   func getCity(_ param: [String:Any]){
       AIServiceManager.sharedManager.callPostApi(URL_City, params: param, nil) { response in
           switch response.result{
           case let .success(result):
               HIDE_CUSTOM_LOADER()
               if let json = result as? [String: Any]{
                   if let status = json["status"] as? Int{
                       if status == 1{
                           if let data = json["data"] as? [[String:Any]]{
                               self.ArrayCity = City.init(data).citylists
                               self.ArrayDelivaryCity = City.init(data).citylists
                           }
                       }else if status == 9 {
                           let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                           let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                           let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                           nav.navigationBar.isHidden = true
                           keyWindow?.rootViewController = nav
                       }else {
                           let msg = json["data"] as! [String:Any]
                           let massage = msg["message"] as! String
                           showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                       }
                   }
               }
           case let .failure(error):
               print(error.localizedDescription)
               HIDE_CUSTOM_LOADER()
               break
           }
       }
    }
    
    //MARK: - Functions
    
}
extension GuestAddressVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 6
        if textField == txt_postcodeDelivary {
            let currentString: NSString = txt_postcodeDelivary.text! as NSString
            let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        let currentString: NSString = txt_postcode.text! as NSString
        let newString: NSString =
        currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
}
