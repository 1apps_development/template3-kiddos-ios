//
//  BillingAddressVC.swift
//  kiddos
//
//  Created by mac on 19/04/22.
//

import UIKit
import iOSDropDown
import StoreKit
import Alamofire

class BillingAddressVC: UIViewController {
    
    //MARK: - Outelts
    
    @IBOutlet weak var txt_lastname: UITextField!
    @IBOutlet weak var txt_firstname: UITextField!
    @IBOutlet weak var txt_email: UITextField!
    @IBOutlet weak var txt_telephone: UITextField!
    @IBOutlet weak var btn_check: UIButton!
    
    @IBOutlet weak var addresslistTableView: UITableView!
    @IBOutlet weak var addressListHeight: NSLayoutConstraint!
    
    @IBOutlet weak var BilingAddressView: UIView!
    @IBOutlet weak var BilingAddressViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var txt_company: UITextField!
    @IBOutlet weak var txt_address1: UITextField!
    @IBOutlet weak var txt_city: DropDown!
    @IBOutlet weak var txt_postcode: UITextField!
    @IBOutlet weak var txt_state: DropDown!
    @IBOutlet weak var txt_country: DropDown!
    
    //MARK: - Variables
    
    var pageIndex = 1
    var lastIndex = 0
    var selectedindex = 0
    var selectedCountryid = Int()
    var selectedStateid = Int()
    var selectedCityid = String()
    var ArrayAddressList: [AddressLists] = []
    var ArrayCountry: [CountryLists] = []
    var ArraySatate: [StateLists] = []
    var ArrayCity: [CityLists] = []
    
    //MARK: - ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.BilingAddressView.isHidden = true
        self.BilingAddressViewHeight.constant = 0.0
        txt_postcode.delegate = self
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.txt_firstname.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_FIRSTNAME)
        self.txt_lastname.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_LASTNAME)
        self.txt_email.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_EMAIL)
        self.txt_telephone.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_PHONE)
        
        self.btn_check.setImage(UIImage.init(named: "squareFill"), for: .normal)
        self.addresslistTableView.delegate = self
        self.addresslistTableView.dataSource = self
        self.addresslistTableView.register(UINib(nibName: "AddressCell", bundle: nil), forCellReuseIdentifier: "AddressCell")
        self.addressListHeight.constant = 0.0
        self.addresslistTableView.estimatedRowHeight = UITableView.automaticDimension
        self.addressListHeight.constant = UITableView.automaticDimension
        let addressparam: [String:Any] = ["user_id": getID(),
                                          "theme_id": APP_THEMEID
        ]
        getAddressList(addressparam)
        self.addresslistTableView.reloadData()
    }
    //MARK: - Actions
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickAddress(_ sender: Any){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChangeAddressVC") as! ChangeAddressVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func onClickContinue(_ sender: Any){
        if self.txt_firstname.text! == ""
        {
            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: "Please enter first name.")
        }
        else if self.txt_lastname.text! == ""
        {
            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: "Please enter last name.")
        }
        if self.txt_email.text! == ""
        {
            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: "Please enter the email address.")
        }
        else if self.txt_telephone.text! == ""
        {
            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: "Please enter phone number.")
        }
//        else if isValidEmail(self.txt_email.text!)
//        {
//            showAlertMessage(titleStr: "", messageStr: "Please enter the valid email address.")
//        }
        else{
            if self.ArrayAddressList.count != 0 {
                let data = self.ArrayAddressList[selectedindex]
                print(data)
                if self.btn_check.imageView?.image == UIImage(named: "squareFill"){
                    let data = self.ArrayAddressList[selectedindex]
                    let billingObject = ["firstname":self.txt_firstname.text!,"lastname":self.txt_lastname.text!,"email":self.txt_email.text!,"billing_user_telephone":self.txt_telephone.text!,"billing_address":data.address!,"billing_postecode":data.postcode!,"billing_country":data.countryid!,"billing_state":data.stateid!,"billing_city":data.city!,"delivery_address":data.address!,"delivery_postcode":data.postcode!,"delivery_country":data.countryid!,"delivery_state":data.stateid!,"delivery_city":data.cityname!] as [String : Any]
                    
                    UserDefaults.standard.set(billingObject, forKey: userDefaultsKeys.KEY_BILLINGOBJ)
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShippingVC") as! ShippingVC
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else{
                    let adddata = self.ArrayAddressList[selectedindex]
                    if self.txt_address1.text! == ""
                    {
                        showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: "ENTER_ADDRESS_MESAAGE")
                    }
                    else if self.txt_country.text == ""
                    {
                        showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: "ENTER_COUNTRY_MESAAGE")
                    }
                    else if self.txt_state.text == ""
                    {
                        showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: "ENTER_STATE_MESAAGE")
                    }
                    else if self.txt_city.text == ""
                    {
                        showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: "ENTER_CITY_MESAAGE")
                    }
                    else if self.txt_postcode.text! == ""
                    {
                        showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: "ENTER_POSTCODE_MESAAGE")
                    }else{
                        let data = self.ArrayAddressList[selectedindex]
                        let billingObject = ["firstname":self.txt_firstname.text!,"lastname":self.txt_lastname.text!,"email":self.txt_email.text!,"billing_user_telephone":self.txt_telephone.text!,"billing_address":adddata.address!,"billing_postecode":data.postcode!,"billing_country":data.countryid!,"billing_state":adddata.stateid!,"billing_city":adddata.city!,"delivery_address":self.txt_address1.text!,"delivery_postcode":Int(self.txt_postcode.text!)!,"delivery_country":self.selectedCountryid,"delivery_state":self.selectedStateid,"delivery_city":self.txt_city.text!] as [String : Any]
                        UserDefaults.standard.set(billingObject, forKey: userDefaultsKeys.KEY_BILLINGOBJ)
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShippingVC") as! ShippingVC
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
        }
        
    }
    
    @IBAction func onClickCountry(_ sender: DropDown){
        var countryName = [String]()
        for country in ArrayCountry{
            countryName.append(country.name!)
        }
        self.txt_country.textColor = hexStringToUIColor(hex: "#3E4F46")
        self.txt_country.checkMarkEnabled = false
        self.txt_country.optionArray = countryName
        self.txt_country.selectedRowColor = hexStringToUIColor(hex: "#B7D4C5")
        self.txt_country.rowBackgroundColor  = hexStringToUIColor(hex: "#B7D4C5")
        self.txt_country.didSelect(completion: { selected, index, id in
            self.txt_country.text = selected
            self.selectedCountryid = self.ArrayCountry[index].id!
            let param: [String:Any] = ["country_id": self.selectedCountryid,
                                       "theme_id": APP_THEMEID]
            self.getState(param)
               })
    }
    @IBAction func onClickState(_ sender: DropDown){
        var statename = [String]()
        for state in ArraySatate{
            statename.append(state.name!)
        }
        self.txt_state.textColor = hexStringToUIColor(hex: "#3E4F46")
        self.txt_state.checkMarkEnabled = false
        self.txt_state.optionArray = statename
        self.txt_state.selectedRowColor = hexStringToUIColor(hex: "#B7D4C5")
        self.txt_state.rowBackgroundColor  = hexStringToUIColor(hex: "#B7D4C5")
        self.txt_state.didSelect(completion: { selected, index, id in
            self.txt_state.text = selected
            self.selectedStateid = self.ArraySatate[index].id!
            let param: [String:Any] = ["state_id": self.selectedStateid,
                                       "theme_id": APP_THEMEID]
            self.getCity(param)
               })
    }
    @IBAction func onClickCity(_ sender: DropDown){
        var cityname = [String]()
        for city in ArrayCity{
            cityname.append(city.name!)
        }
        self.txt_city.textColor = hexStringToUIColor(hex: "#3E4F46")
        self.txt_city.checkMarkEnabled = false
        self.txt_city.optionArray = cityname
        self.txt_city.selectedRowColor = hexStringToUIColor(hex: "#B7D4C5")
        self.txt_city.rowBackgroundColor  = hexStringToUIColor(hex: "#B7D4C5")
        self.txt_city.didSelect(completion: { selected, index, id in
            self.txt_city.text = selected
//            self.selectedCountryid = self.ArrayCountry[index].id!
               })
    }
    @IBAction func onclickCheakButton(_ sender: UIButton) {
        if self.btn_check.imageView?.image == UIImage.init(named: "checkbox")
        {
            self.btn_check.setImage(UIImage.init(named: "squareFill"), for: .normal)
            self.BilingAddressView.isHidden = true
            self.BilingAddressViewHeight.constant = 0.0
        }
        else{
            self.btn_check.setImage(UIImage.init(named: "checkbox"), for: .normal)
            self.BilingAddressView.isHidden = false
            self.BilingAddressViewHeight.constant = 500.0
        }
    }
    
    //MARK: - API Functions
    func getAddressList(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
        AIServiceManager.sharedManager.callPostApi(URL_Address, params: param, headers) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [String:Any]{
                               if let addressdata = data["data"] as? [[String:Any]]{
                                   self.ArrayAddressList = Address.init(addressdata).addresslist
                                   self.addresslistTableView.reloadData()
                                   self.addressListHeight.constant = CGFloat(100 * self.ArrayAddressList.count)
                                   let param: [String:Any] = ["theme_id": APP_THEMEID]
                                   self.getCountries(param)
                               }
                           }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
//                    print(json)
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
      }
    }
    func getCountries(_ param: [String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_Country, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [[String:Any]]{
                                self.ArrayCountry = Country.init(data).countrylists
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else {
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
     }
    func getState(_ param: [String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_State, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [[String:Any]]{
                               self.ArraySatate = State.init(data).statelists
                           }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
     }
    
   func getCity(_ param: [String:Any]){
       AIServiceManager.sharedManager.callPostApi(URL_City, params: param, nil) { response in
           switch response.result{
           case let .success(result):
               HIDE_CUSTOM_LOADER()
               if let json = result as? [String: Any]{
                   if let status = json["status"] as? Int{
                       if status == 1{
                           if let data = json["data"] as? [[String:Any]]{
                              self.ArrayCity = City.init(data).citylists
                          }
                       }else if status == 9{
                           let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                           let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                           let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                           nav.navigationBar.isHidden = true
                           keyWindow?.rootViewController = nav
                       }else{
                           let msg = json["data"] as! [String:Any]
                           let massage = msg["message"] as! String
                           showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                       }
                   }
               }
           case let .failure(error):
               print(error.localizedDescription)
               HIDE_CUSTOM_LOADER()
               break
           }
       }
    }
    //MARK: - CommenFunctions
}
extension BillingAddressVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ArrayAddressList.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddressCell", for: indexPath) as! AddressCell
        if indexPath.row == self.selectedindex{
            cell.cellView.borderWidth = 1
            cell.cellView.borderColor = hexStringToUIColor(hex: "#3E4F46")
            cell.img_selected.image = UIImage(named: "ic_cheaked")
        }else{
            cell.cellView.borderWidth = 1
            cell.cellView.borderColor = hexStringToUIColor(hex: "#E8E8E8")
            cell.img_selected.image = UIImage(named: "ic_uncheacked")
        }
        cell.configCell(ArrayAddressList[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedindex = indexPath.item
        self.addresslistTableView.reloadData()
    }
}
extension BillingAddressVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 6
        let currentString: NSString = txt_postcode.text! as NSString
        let newString: NSString =
        currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
}
