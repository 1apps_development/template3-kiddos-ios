//
//  PaymentVC.swift
//  kiddos
//
//  Created by mac on 19/04/22.
//

import UIKit
import SDWebImage

class PaymentVC: UIViewController, UITextViewDelegate{

    // MARK: - Outlets
    @IBOutlet weak var btn_selected: UIButton!
    @IBOutlet weak var descriptTextView: UITextView!
    @IBOutlet weak var heightPaymentTableView: NSLayoutConstraint!
    @IBOutlet weak var paymentTableView: UITableView!
    @IBOutlet weak var termsandCondition: UIButton!
    
    // MARK: - Variables
    var selectedindex = 0
    var ArrayPayment: [PaymentList] = []
    var ArrayOnStatus: [PaymentList] = []
    
    // MARK: - ViewLifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.paymentTableView.isHidden = true
        self.paymentTableView.register(UINib(nibName: "DelivaryCell", bundle: nil), forCellReuseIdentifier: "DelivaryCell")
        self.btn_selected.setImage(UIImage(named: "sqaure"), for: .normal)
        self.descriptTextView.text = "Description"
        self.descriptTextView.textColor = UIColor.lightGray
        self.descriptTextView.delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let param: [String:Any] = ["theme_id": APP_THEMEID]
        getPaymentList(param)
    }
    // MARK: - Actions
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func onclickbtnSelected(_ sender: UIButton) {
        if self.btn_selected.imageView?.image == UIImage.init(named: "sqaure")
        {
            self.btn_selected.setImage(UIImage.init(named: "squareFill"), for: .normal)
        }
        else{
            self.btn_selected.setImage(UIImage.init(named: "sqaure"), for: .normal)
        }
    }
    
    @IBAction func onclickTerms(_ sender: UIButton) {
        guard let url = URL(string: UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_TERMS)!) else {
            return
        }
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func onclickContinue(_ sender: UIButton) {
        if self.btn_selected.imageView?.image == UIImage(named: "sqaure"){
            showAlertMessage(titleStr: "", messageStr: "Please agree to the Terms & Conditions.")
        }else{
            if self.descriptTextView.text == "Description"{
                self.descriptTextView.text = ""
            }
            let data = ArrayOnStatus[selectedindex]
            if data.nameString == "COD"{
                UserDefaults.standard.set("cod", forKey: userDefaultsKeys.KEY_PAYMENTTYPE)
                UserDefaults.standard.set(data.image, forKey: userDefaultsKeys.KEY_PAYMENTIMAGETYPE)
            }else if data.nameString == "Bank Transfer"{
                UserDefaults.standard.set("bank_transfer", forKey: userDefaultsKeys.KEY_PAYMENTTYPE)
                UserDefaults.standard.set(data.image, forKey: userDefaultsKeys.KEY_PAYMENTIMAGETYPE)
            }else if data.nameString == "other_payment"{
                UserDefaults.standard.set("Other Payment", forKey: userDefaultsKeys.KEY_PAYMENTTYPE)
                UserDefaults.standard.set(data.image, forKey: userDefaultsKeys.KEY_PAYMENTIMAGETYPE)
            }else if data.nameString == "Stripe"{
                UserDefaults.standard.set("stripe", forKey: userDefaultsKeys.KEY_PAYMENTTYPE)
                UserDefaults.standard.set(data.image, forKey: userDefaultsKeys.KEY_PAYMENTIMAGETYPE)
            }
            UserDefaults.standard.set(self.descriptTextView.text, forKey: userDefaultsKeys.KEY_PAYMENTDESCRIPTION)
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CheckoutVC") as! CheckoutVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    // MARK: - APIFunctions
    func getPaymentList(_ param: [String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_PaymentList, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [[String:Any]]{
                                self.ArrayPayment = Payment.init(data).paymentlist
                                self.ArrayOnStatus = self.ArrayPayment.filter({$0.status == "on"})
                                self.paymentTableView.estimatedRowHeight = 120
                                self.paymentTableView.rowHeight = UITableView.automaticDimension
                                self.paymentTableView.reloadData()
                                self.heightPaymentTableView.constant = CGFloat(120 * self.ArrayPayment.count)
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                                    self.heightPaymentTableView.constant = self.paymentTableView.contentSize.height
                                    self.paymentTableView.isHidden = false
                                }
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else {
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
//                    print(json)
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
     }
    
    // MARK: - CustomFunctions
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.white
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Description"
            textView.textColor = UIColor.lightGray
        }
    }
}
extension PaymentVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ArrayOnStatus.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DelivaryCell", for: indexPath) as! DelivaryCell
        let datas = ArrayOnStatus[indexPath.row]
        if indexPath.row == self.selectedindex{
            cell.cellView.borderWidth = 1
            cell.cellView.borderColor = hexStringToUIColor(hex: "#3E4F46")
            cell.img_selected.image = UIImage(named: "ic_cheaked")
        }else{
            cell.cellView.borderWidth = 1
            cell.cellView.borderColor = hexStringToUIColor(hex: "#E8E8E8")
            cell.img_selected.image = UIImage(named: "ic_uncheacked")
        }
        cell.lbl_title.text = datas.nameString
        cell.lbl_address.text = datas.detail
        cell.lbl_Price.isHidden = true
        cell.aditinalprice.isHidden = true
        let imageURL = "\(URL_PaymentImage)/\(datas.image!)"
        cell.img_payment.sd_setImage(with: URL(string: imageURL)) { image, error, type, url in
            cell.img_payment.image = image
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = self.ArrayOnStatus[indexPath.row]
        if data.nameString == "COD"
        {
            UserDefaults.standard.set("cod", forKey: userDefaultsKeys.KEY_PAYMENTTYPE)
            UserDefaults.standard.set(data.image, forKey: userDefaultsKeys.KEY_PAYMENTIMAGETYPE)
        }
        else if data.nameString == "Bank Transfer"
        {
            UserDefaults.standard.set("bank_transfer", forKey: userDefaultsKeys.KEY_PAYMENTTYPE)
            UserDefaults.standard.set(data.image, forKey: userDefaultsKeys.KEY_PAYMENTIMAGETYPE)
        }
        else if data.nameString == "Stripe"{
            UserDefaults.standard.set("stripe", forKey: userDefaultsKeys.KEY_PAYMENTTYPE)
            UserDefaults.standard.set(data.image, forKey: userDefaultsKeys.KEY_PAYMENTIMAGETYPE)
        }
        self.selectedindex = indexPath.item
        self.paymentTableView.reloadData()
    }
}
