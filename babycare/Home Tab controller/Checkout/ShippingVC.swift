//
//  ShippingVC.swift
//  kiddos
//
//  Created by mac on 19/04/22.
//

import UIKit

class ShippingVC: UIViewController, UITextViewDelegate{
    
    //MARK: - outlects
    @IBOutlet weak var delivaryTableView: UITableView!
    @IBOutlet weak var heightDelivaryTableview: NSLayoutConstraint!
    @IBOutlet weak var DescriptionTextView: UITextView!
    
    //MARK: - Variables
    var selectedindex = 0
    var ArrayDelivary: [DelivaryList] = []
    
    //MARK: - viewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delivaryTableView.register(UINib(nibName: "DelivaryCell", bundle: nil), forCellReuseIdentifier: "DelivaryCell")
        self.delivaryTableView.isHidden = true
        self.DescriptionTextView.text = "Description"
        self.DescriptionTextView.textColor = UIColor.lightGray
        self.DescriptionTextView.delegate = self
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let param: [String:Any] = ["theme_id": APP_THEMEID]
        getDelivaryList(param)
    }
    //MARK: - Actions
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onclickContinue(_ sender: UIButton) {
        if self.DescriptionTextView.text == "Description"{
            self.DescriptionTextView.text = ""
        }
        let data = ArrayDelivary[selectedindex]
        UserDefaults.standard.set(data.id, forKey: userDefaultsKeys.KEY_DELIVARYID)
        UserDefaults.standard.set(data.imagepath, forKey: userDefaultsKeys.KEY_DELIVARYIMAGE)
        
        UserDefaults.standard.set(self.DescriptionTextView.text, forKey: userDefaultsKeys.KEY_DELIVARYDESCRIPTION)
        
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: - API function
    func getDelivaryList(_ param: [String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_DelivaryList, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [[String:Any]]{
                               self.ArrayDelivary = Delivary.init(data).delivarylist
                                self.delivaryTableView.estimatedRowHeight = 120
                                self.delivaryTableView.rowHeight = UITableView.automaticDimension
                                
                                self.heightDelivaryTableview.constant = CGFloat(120 * self.ArrayDelivary.count)
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                                    self.heightDelivaryTableview.constant = self.delivaryTableView.contentSize.height
                                    self.delivaryTableView.isHidden = false
                                }
                               self.delivaryTableView.isHidden = false
                               self.delivaryTableView.reloadData()
                           }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
     }

    
    //MARK: - CustomFunction
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = hexStringToUIColor(hex: "#3E4F46")
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Description"
            textView.textColor = UIColor.lightGray
        }
    }
}
extension ShippingVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ArrayDelivary.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DelivaryCell", for: indexPath) as! DelivaryCell
        if indexPath.row == self.selectedindex{
            cell.cellView.borderWidth = 1
            cell.cellView.borderColor = hexStringToUIColor(hex: "#3E4F46")
            cell.img_selected.image = UIImage(named: "ic_cheaked")
        }else{
            cell.cellView.borderWidth = 1
            cell.cellView.borderColor = hexStringToUIColor(hex: "#E8E8E8")
            cell.img_selected.image = UIImage(named: "ic_uncheacked")
        }
        cell.configCell(ArrayDelivary[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = self.ArrayDelivary[indexPath.row]
        self.selectedindex = indexPath.item
        self.delivaryTableView.reloadData()
        UserDefaults.standard.set(data.id, forKey: userDefaultsKeys.KEY_DELIVARYID)
        UserDefaults.standard.set(data.imagepath, forKey: userDefaultsKeys.KEY_DELIVARYIMAGE)
    }
}
