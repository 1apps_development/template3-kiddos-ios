//
//  AddressBookVC.swift
//  kiddos
//
//  Created by mac on 19/10/22.
//

import UIKit
import Alamofire

class AddressBookVC: UIViewController{

    @IBOutlet weak var tblviewaddressbook: UITableView!
    @IBOutlet weak var Viewempty: UIView!
    @IBOutlet weak var nodataFound: UILabel!
    
    var selectedindex = 0
    var addressList = [AddressLists]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblviewaddressbook.register(UINib(nibName: "AddressBookcell", bundle: nil), forCellReuseIdentifier: "AddressBookcell")
        Viewempty.isHidden = true
        nodataFound.isHidden = true
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let param: [String:Any] = ["user_id": getID(),
                                   "theme_id": APP_THEMEID]
        getAddressList(param)
        self.tblviewaddressbook.reloadData()
    }
    
    
    @IBAction func onclickaddAddress(_ sender: UIButton) {
        let vc = MainstoryBoard.instantiateViewController(withIdentifier: "ChangeAddressVC") as! ChangeAddressVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func onclickBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func getAddressList(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
        AIServiceManager.sharedManager.callPostApi(URL_Address, params: param, headers) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [String:Any]{
                                if let addressdata = data["data"] as? [[String:Any]]{
                                    self.addressList = Address.init(addressdata).addresslist
                                    self.tblviewaddressbook.reloadData()
                                }
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
//                    print(json)
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
      }
    }
    
    func deleteAddressList(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
        AIServiceManager.sharedManager.callPostApi(URL_deleteAddress, params: param, headers) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [String:Any]{
                                let param: [String:Any] = ["user_id": getID(),
                                                           "theme_id": APP_THEMEID]
                                self.getAddressList(param)
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
//                    print(json)
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
      }
    }

}
extension AddressBookVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.addressList.count == 0 {
            self.Viewempty.isHidden = false
            self.nodataFound.isHidden = false
        }else{
            self.Viewempty.isHidden = true
            self.nodataFound.isHidden = true
            return self.addressList.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblviewaddressbook.dequeueReusableCell(withIdentifier: "AddressBookcell") as! AddressBookcell
        let data = self.addressList[indexPath.row]
        cell.lbl_title.text = data.title!
        cell.lbl_discription.text = "\(data.address!), \(data.cityname!) , \(data.statename!), \(data.countryname!) - \(data.postcode!)"
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedindex = indexPath.item
        self.tblviewaddressbook.reloadData()
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
    func tableView(_ tableView: UITableView,trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        let deleteAction = UIContextualAction(
            style: .normal,
            title:  nil,
            handler: { (_, _, success: (Bool) -> Void) in
                success(true)
                let data = self.addressList[indexPath.row]
                let param: [String:Any] = ["address_id": data.id!,
                                           "theme_id": APP_THEMEID]
                self.deleteAddressList(param)
            }
        )
        let EditAction = UIContextualAction(
            style: .normal,
            title:  nil,
            handler: { (_, _, success: (Bool) -> Void) in
                success(true)
                let data = self.addressList[indexPath.row]
                let vc = MainstoryBoard.instantiateViewController(withIdentifier: "ChangeAddressVC") as! ChangeAddressVC
                vc.isedit = 1
                vc.EditedData = data
                self.navigationController?.pushViewController(vc, animated: true)
            }
        )
        
        EditAction.image = UISwipeActionsConfiguration.makeTitledImage(
            image: UIImage(named: "password eye"),
            title: "Edit", textColor: hexStringToUIColor(hex: "#FBD9B3"))
        EditAction.backgroundColor = UIColor.init(named: "#FBD9B3")
        
        deleteAction.image = UISwipeActionsConfiguration.makeTitledImage(
            image: UIImage(named: "ic_trash"),
            title: "Delete", textColor: hexStringToUIColor(hex: "#FBD9B3"))
        deleteAction.backgroundColor = UIColor.init(named: "#FBD9B3")
        
        return UISwipeActionsConfiguration(actions: [deleteAction,EditAction])
    }
    
}
