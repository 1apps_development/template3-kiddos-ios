import UIKit
import Alamofire
import SDWebImage

class CategoriesDetailVC: UIViewController,UIScrollViewDelegate {
    
    //MARK: - Outlets
    @IBOutlet weak var CVProducts: UICollectionView!
    @IBOutlet weak var CVCategories: UICollectionView!
    @IBOutlet weak var lbl_categories: UILabel!
    @IBOutlet weak var heightCategories: NSLayoutConstraint!
    @IBOutlet weak var heightProducts: NSLayoutConstraint!
    @IBOutlet weak var totalProducts: UILabel!
    @IBOutlet weak var title_back: UILabel!
    @IBOutlet weak var backBtnView: UIView!
    @IBOutlet weak var lbl_cartCount: UILabel!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var ScrollView: UIScrollView!
    @IBOutlet weak var img_bannerView: UIImageView!
    
    //MARK: - Variables
    var catid: Int?
    var arrCategories: [CategoryData] = []
    var arrProducts: [Products] = []
    var catID: Int?
    var trending: String?
    var bestseller: String?
    var selectedIndex: Int = 0
    var iscategoriSelected: Bool = false
    var isHome = String()
    var isMenu = String()
    var productid = Int()
    var varientId = Int()
    var pageindex = 1
    var lastindex = 0
    var productpageindex = 1
    var productlastindex = 0
    var trendingPageindex = 1
    var trendinglastindex = 0
    
    //MARK: - ViewlifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        CVCategories.register(UINib(nibName: "CatgoriesDummycell", bundle: nil), forCellWithReuseIdentifier: "CatgoriesDummycell")
        CVCategories.register(UINib(nibName: "CategoriesCell", bundle: nil), forCellWithReuseIdentifier: "CategoriesCell")
        CVProducts.register(UINib.init(nibName: "ToysCell", bundle: nil), forCellWithReuseIdentifier: "ToysCell")
        self.ScrollView.delegate = self
        if isMenu == "Menu"{
            CVCategories.isHidden = true
            heightCategories.constant = 0.0
            lbl_categories.isHidden = true
            backBtnView.isHidden = false
            lbl_title.text = "Product List"
        }
        let catParam: [String: Any] = ["theme_id": APP_THEMEID]
        getCategoriesData(catParam)
        let param: [String:Any] = ["theme_id": APP_THEMEID]
        getProductBanner(param)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.lbl_cartCount.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
    }
    
    //MARK: - Actions
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickCarts(_ sender: Any){
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "ShoppingCartVC") as! ShoppingCartVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    //MARK: - Customfunctions
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (Int(self.ScrollView.contentOffset.y) >=  Int(self.ScrollView.contentSize.height - self.ScrollView.frame.size.height)) {
            if self.productpageindex != self.productlastindex {
                self.productpageindex = self.productpageindex + 1
                if self.arrProducts.count != 0 {
                    if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""
                    {
                        if trending == "Trending"{
                            self.MakeAPICallforTrendingProductsGuest(["theme_id": APP_THEMEID])
                        }else if bestseller == "BestSeller" {
                            self.getProductsGuestData(["theme_id": APP_THEMEID])
                        }else if isHome == "yes" {
                            let param: [String: Any] = ["maincategory_id": self.catid!,
                                                        "theme_id": APP_THEMEID]
                            getProductsGuestData(param)
                        }else{
                            let param: [String: Any] = ["maincategory_id": self.catid!,
                                                        "theme_id": APP_THEMEID]
                            getProductsGuestData(param)
                        }
                    }
                    else{
                        if trending == "Trending" {
                            self.MakeAPICallforTrending(["theme_id": APP_THEMEID])
                        }else if bestseller == "BestSeller" {
                            self.getbestSellersData(["theme_id": APP_THEMEID])
                        }else if isHome == "yes"{
                            let proParam: [String:Any] = ["maincategory_id": self.catid!,
                                                          "theme_id": APP_THEMEID]
                            getbestSellersData(proParam)
                        }else{
                            let proParam: [String:Any] = ["maincategory_id": self.catid!,
                                                          "theme_id": APP_THEMEID]
                            getbestSellersData(proParam)
                        }
                    }
                }
            }
        }
    }
    //MARK: - APIFunctions
    func getCategoriesData(_ param: [String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_HomeCategories + "\(pageindex)", params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [String:Any]{
                                if self.pageindex == 1 {
                                    let lastpage = data["last_page"] as! Int
                                    self.lastindex = lastpage
                                    self.arrCategories.removeAll()
                                }
                                if let categories = data["data"] as? [[String:Any]]{
                                    self.arrCategories.append(contentsOf: HomeCategroies.init(categories).categoriesData)
                                    self.CVCategories.reloadData()
                                }
                                if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
                                    if self.trending == "Trending" {
                                        self.productpageindex = 1
                                        self.productlastindex = 0
                                        self.MakeAPICallforTrendingProductsGuest(["theme_id": APP_THEMEID])
                                    }else if self.bestseller == "BestSeller"{
                                        self.productpageindex = 1
                                        self.productlastindex = 0
                                        let param: [String:Any] = ["theme_id": APP_THEMEID]
                                        self.getProductsGuestData(param)
                                    }else if self.isHome == "yes"{
                                        self.productpageindex = 1
                                        self.productlastindex = 0
                                        let param: [String:Any] = ["maincategory_id": self.catid!,
                                                                   "theme_id": APP_THEMEID]
                                        self.getProductsGuestData(param)
                                    }else{
                                        self.backBtnView.isHidden = true
                                        self.title_back.isHidden = true
                                        self.productpageindex = 1
                                        self.productlastindex = 0
                                        self.catid = 0
                                        let param: [String:Any] = ["maincategory_id": self.catid!,
                                                                   "theme_id": APP_THEMEID]
                                        self.getProductsGuestData(param)
                                    }
                                }else{
                                    if self.trending == "Trending" {
                                        self.productpageindex = 1
                                        self.productlastindex = 0
                                        self.MakeAPICallforTrending(["theme_id": APP_THEMEID])
                                    }else if self.bestseller == "BestSeller" {
                                        self.productpageindex = 1
                                        self.productlastindex = 0
                                        self.getbestSellersData(["theme_id": APP_THEMEID])
                                    }else if self.isHome == "yes"{
                                        self.productpageindex = 1
                                        self.productlastindex = 0
                                        let param: [String:Any] = ["maincategory_id": self.catid!,
                                                                   "theme_id": APP_THEMEID]
                                        self.getbestSellersData(param)
                                    }else{
                                        self.backBtnView.isHidden = true
                                        self.productpageindex = 1
                                        self.productlastindex = 0
                                        self.catid = 0
                                        let param: [String:Any] = ["maincategory_id": self.catid!,
                                                                   "theme_id": APP_THEMEID]
                                        self.getbestSellersData(param)
                                    }
                                }
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
                    //                    print(json)
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
    }
    func MakeAPICallforTrendingProductsGuest(_ param:[String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_TrendingProductsGuest + "\(productpageindex)", params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let data = json["data"] as? [String:Any]{
                        if let msg = data["message"] as? String{
                            self.view.makeToast(msg)
                        }else if let aData = data["data"] as? [[String:Any]]{
                            if self.productpageindex == 1 {
                                let lastpage = data["last_page"] as! Int
                                self.productlastindex = lastpage
                                self.arrProducts.removeAll()
                            }
                            self.arrProducts.append(contentsOf: KidsToys.init(aData).productdata)
                            self.CVProducts.reloadData()
                            let productTotal = data["total"] as! Int
                            self.totalProducts.text = "\(productTotal) products"
                        }
                    }
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    func MakeAPICallforTrending(_ param:[String:Any]){
        let headers: HTTPHeaders = ["Authorization": "Bearer \(getToken())"]
        AIServiceManager.sharedManager.callPostApi(URL_TrendingProducts + "\(productpageindex)", params: param, headers) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let data = json["data"] as? [String:Any]{
                        if let msg = data["message"] as? String{
                            self.view.makeToast(msg)
                        }else if let aData = data["data"] as? [[String:Any]]{
                            if self.productpageindex == 1 {
                                let lastpage = data["last_page"] as! Int
                                self.productlastindex = lastpage
                                self.arrProducts.removeAll()
                            }
                            self.arrProducts.append(contentsOf: KidsToys.init(aData).productdata)
                            self.CVProducts.reloadData()
                            let productTotal = data["total"] as! Int
                            self.totalProducts.text = "\(productTotal) products"
                        }
                    }
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    func getbestSellersData(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_Catproduct + "\(productpageindex)", params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String: Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                if let data = json["data"] as? [String:Any]{
                                    if self.productpageindex == 1 {
                                        let productLastpage = data["last_page"] as! Int
                                        self.productlastindex = productLastpage
                                        self.arrProducts.removeAll()
                                    }
                                    if let products = data["data"] as? [[String:Any]]{
                                        self.arrProducts.append(contentsOf: KidsToys.init(products).productdata)
                                        self.CVProducts.reloadData()
                                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                            self.heightProducts.constant = self.CVProducts.contentSize.height
                                        }
                                    }
                                    let productTotal = data["total"] as! Int
                                    self.totalProducts.text = "\(productTotal) products"
                                }
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let msg = json["data"] as! [String:Any]
                                let massage = msg["message"] as! String
                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
                        
                    }
                case let .failure(error):
                    print(error.localizedDescription)
                    HIDE_CUSTOM_LOADER()
                    break
                }
            }
        }
    }
    func getProductsGuestData(_ param: [String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_CategoriesGuestProduct + "\(productpageindex)", params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [String:Any]{
                                if self.productpageindex == 1 {
                                    let lastpage = data["last_page"] as! Int
                                    self.productlastindex = lastpage
                                    self.arrProducts.removeAll()
                                }
                                if let catData = data["data"] as? [[String:Any]]{
                                    self.arrProducts.append(contentsOf: KidsToys.init(catData).productdata)
                                    self.CVProducts.reloadData()
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                        self.heightProducts.constant = self.CVProducts.contentSize.height
                                    }
                                }
                                let productTotal = data["total"] as! Int
                                self.totalProducts.text = "\(productTotal) products"
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
                    
                    //                    print(json)
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
    }
    func addTocart(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            //            print(getID())
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_Addtocart, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                if let stats = json["data"] as? [String:Any]{
                                    let massage = stats["message"] as? String
                                    let status = json["status"] as? Int
                                    let count = stats["count"] as? Int
                                    UserDefaults.standard.set(count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                                    if status == 1 {
                                        let alert = UIAlertController(title: nil, message: massage, preferredStyle: .actionSheet)
                                        let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
                                            self.dismiss(animated: true)
                                        }
                                        
                                        let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
                                            let vc = self.storyboard?.instantiateViewController(identifier: "ShoppingCartVC") as! ShoppingCartVC
                                            self.navigationController?.pushViewController(vc, animated: true)
                                        }
                                        
                                        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                                        alert.addAction(photoLibraryAction)
                                        alert.addAction(cameraAction)
                                        alert.addAction(cancelAction)
                                        self.present(alert, animated: true, completion: nil)
                                    }else if status == 0 {
                                        if massage == "Product has out of stock."{
                                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage!)
                                        }
                                    }
                                    self.lbl_cartCount.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
                                }
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let alertVC = UIAlertController(title: Bundle.main.displayName!, message: "Product already in the cart! Do you want to add it again?", preferredStyle: .alert)
                                let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
                                    let param: [String:Any] = ["user_id": getID(),
                                                               "product_id": self.productid,
                                                               "variant_id": self.varientId,
                                                               "quantity_type": "increase",
                                                               "theme_id": APP_THEMEID]
                                    self.getQtyOfProduct(param)
                                }
                                let noAction = UIAlertAction(title: "No", style: .destructive)
                                alertVC.addAction(noAction)
                                alertVC.addAction(yesAction)
                                self.present(alertVC,animated: true,completion: nil)
                            }
                        }
                        print(json)
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    func getQtyOfProduct(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_cartQty, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                let count = json["count"] as? Int
                                UserDefaults.standard.set(count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                            }else if status == 9{
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let msg = json["data"] as! [String:Any]
                                let massage = msg["message"] as! String
                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
                    }
                    
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    func getWishList(_ param: [String:Any]){
        if UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_APP_TOKEN) != nil {
            if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
                let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                            "Accept": "application/json"]
                AIServiceManager.sharedManager.callPostApi(URL_Wishlist, params: param, headers) { response in
                    switch response.result{
                    case let .success(result):
                        HIDE_CUSTOM_LOADER()
                        if let json = result as? [String: Any]{
                            if let status = json["status"] as? Int{
                                if status == 1{
                                    if let data = json["data"] as? [String:Any]{
                                        let massage = data["message"] as? String
                                        UserDefaults.standard.set(massage, forKey: userDefaultsKeys.KEY_SAVED_CART)
                                        self.CVProducts.reloadData()
                                    }
                                }else if status == 9 {
                                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                    let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                    nav.navigationBar.isHidden = true
                                    keyWindow?.rootViewController = nav
                                }else{
                                    let msg = json["data"] as! [String:Any]
                                    let massage = msg["message"] as! String
                                    showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                                }
                            }
                        }
                    case let .failure(error):
                        print(error.localizedDescription)
                        HIDE_CUSTOM_LOADER()
                        break
                    }
                }
            }
        }
    }
    // MARK: - product banner api calling
    func getProductBanner(_ param: [String:Any]){
            AIServiceManager.sharedManager.callPostApi(URL_ProductBanner, params: param, nil) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                if let data = json["data"] as? [String:Any]{
                                    if let themeJson = data["them_json"] as? [String:Any]{
                                        if let header = themeJson["products-header-banner"] as? [String:Any]{
                                            let endPoint = header["products-header-banner"] as? String
                                            let original = getImageFullURL("\(endPoint!)")
                                            if let encoded = original.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
                                               let url = URL(string: encoded) {
                                                self.img_bannerView.sd_setImage(with: url)
                                            }
                                        }
                                    }
                                }
                            }
                            else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }
                            else {
                                let msg = json["data"] as! [String:Any]
                                let massage = msg["message"] as! String
                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
            }
        }
    }
}

extension CategoriesDetailVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == CVCategories{
            return arrCategories.count + 1
        }else{
            return arrProducts.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == CVCategories{
            if indexPath.item == 0 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CatgoriesDummycell", for: indexPath) as! CatgoriesDummycell
                if catid == 0 {
                    cell.DumybgView.backgroundColor = hexStringToUIColor(hex: "#FBD9B3")
                }else{
                    cell.DumybgView.backgroundColor = .clear
                }
                return cell
            }
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoriesCell", for: indexPath) as! CategoriesCell
            if catid == arrCategories[indexPath.item - 1].id{
                cell.bgView.backgroundColor = hexStringToUIColor(hex: "#FBD9B3")
            }
            cell.configureCell(arrCategories[indexPath.item - 1])
            cell.gotoCategoryButton.isHidden = true
            cell.categoryHeight.constant = 0
            cell.categoryBottem.constant = 0
            return cell
        }else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ToysCell", for: indexPath) as! ToysCell
            cell.configreCell(arrProducts[indexPath.row])
            cell.onclickAddCartClosure = {
                if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == "" {
                    let data = self.arrProducts[indexPath.row]
                    if UserDefaults.standard.value(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) != nil{
                        var Gest_array = UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) as! [[String:Any]]
                        var iscart = false
                        for i in 0..<Gest_array.count{
                            if Gest_array[i]["product_id"] as? Int == data.id && Gest_array[i]["variant_id"] as? String == data.varientID{
                                iscart = true
                            }
                        }
                        if iscart == false{
                            let imagepath = getImageFullURL("\(data.coverimagepath!)")
                            let cartobj = ["product_id": data.id!,
                                           "image": imagepath,
                                           "name": data.name!,
                                           "orignal_price": data.orignalPrice!,
                                           "discount_price": data.discountPrice!,
                                           "final_price": data.finalPrice!,
                                           "qty": 1,
                                           "variant_id": data.defaultVariantID!,
                                           "variant_name": data.varientName!] as [String : Any]
                            Gest_array.append(cartobj)
                            UserDefaults.standard.set(Gest_array, forKey: userDefaultsKeys.KEY_GESTUSEROBJ)
                            UserDefaults.standard.set(Gest_array.count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                            
                            let alert = UIAlertController(title: nil, message: "\(data.name!) add successfully", preferredStyle: .actionSheet)
                            let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
                                self.dismiss(animated: true)
                            }
                            
                            let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
                                let vc = self.storyboard?.instantiateViewController(identifier: "ShoppingCartVC") as! ShoppingCartVC
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                            
                            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                            alert.addAction(photoLibraryAction)
                            alert.addAction(cameraAction)
                            alert.addAction(cancelAction)
                            self.present(alert, animated: true, completion: nil)
                        }else{
                            let alert = UIAlertController(title: "disapalynAMe", message: "\(data.name!) add successfully", preferredStyle: .actionSheet)
                            let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
                                self.dismiss(animated: true)
                            }
                            
                            let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
                                let vc = self.storyboard?.instantiateViewController(identifier: "ShoppingCartVC") as! ShoppingCartVC
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                            
                            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                            alert.addAction(photoLibraryAction)
                            alert.addAction(cameraAction)
                            alert.addAction(cancelAction)
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                        self.lbl_cartCount.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
                    }
                }else{
                    self.productid = self.arrProducts[indexPath.row].id!
                    self.varientId = self.arrProducts[indexPath.row].defaultVariantID!
                    let param: [String:Any] = ["user_id": getID(),
                                               "product_id": self.arrProducts[indexPath.row].id!,
                                               "variant_id": self.arrProducts[indexPath.row].defaultVariantID!,
                                               "qty": 1,
                                               "theme_id": APP_THEMEID]
                    self.addTocart(param)
                }
            }
            if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
                cell.wishlistView.isHidden = true
                cell.btn_fev.isHidden = true
            }else{
                cell.wishlistView.isHidden = false
                cell.btn_fev.isHidden = false
            }
            if arrProducts[indexPath.row].isinwishlist == false{
                cell.btn_fev.setImage(UIImage(named: "ic_heart"), for: .normal)
            }else{
                cell.btn_fev.setImage(UIImage(named: "ic_heartfil"), for: .normal)
            }
            cell.btn_fev.tag = indexPath.row
            cell.btn_fev.addTarget(self, action: #selector(onclickFevBtn), for: .touchUpInside)
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == CVCategories{
            let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
            let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
            let size:CGFloat = (CVCategories.frame.size.width - space) / 2.0
            return CGSize(width: size, height: 100)
        }else {
            return CGSize(width: collectionView.frame.width / 2, height: 310)
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == CVCategories{
            
            if indexPath.item == 0 {
                catid = 0
            }else{
                catid = arrCategories[indexPath.item - 1].id
            }
            self.productpageindex = 1
            self.productlastindex = 0
            let param: [String:Any] = ["maincategory_id": catid!,
                                       "theme_id": APP_THEMEID]
            UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == "" ? getProductsGuestData(param) :  getbestSellersData(param)
            CVCategories.reloadData()
        }else{
            let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "ToyDesctiptionVC") as! ToyDesctiptionVC
            vc.idpassed = arrProducts[indexPath.row].id
            vc.varienrid = arrProducts[indexPath.row].defaultVariantID
            navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""
        {
            if collectionView == self.CVCategories
            {
                if indexPath.item == self.arrCategories.count - 1 {
                    if self.pageindex != self.lastindex{
                        self.pageindex += 1
                        if arrCategories.count != 0 {
                            let param: [String: Any] = ["theme_id": APP_THEMEID]
                            getCategoriesData(param)
                        }
                    }
                }
            }
        }else{
            if collectionView == self.CVCategories
            {
                if indexPath.item == self.arrCategories.count - 1 {
                    if self.pageindex != self.lastindex {
                        self.pageindex += 1
                        if self.arrCategories.count != 0 {
                            let proParam: [String:Any] = ["theme_id": APP_THEMEID]
                            getCategoriesData(proParam)
                        }
                    }
                }
            }
        }
    }
    @objc func onclickFevBtn(sender:UIButton) {
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == "" {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            keyWindow?.rootViewController = nav
        }else{
            let data = self.arrProducts[sender.tag]
            if data.isinwishlist == false{
                let param: [String:Any] = ["user_id": getID(),
                                           "product_id": data.id!,
                                           "wishlist_type": "add",
                                           "theme_id": APP_THEMEID]
                data.isinwishlist = true
                self.getWishList(param)
            }else{
                let param: [String:Any] = ["user_id": getID(),
                                           "product_id": data.id!,
                                           "wishlist_type": "remove",
                                           "theme_id": APP_THEMEID]
                data.isinwishlist = false
                self.getWishList(param)
            }
        }
    }
}
