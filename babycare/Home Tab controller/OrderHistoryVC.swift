//
//  OrderHistoryVC.swift
//  kiddos
//
//  Created by mac on 20/04/22.
//

import UIKit
import Alamofire

class OrderHistoryVC: UIViewController {
    
    @IBOutlet weak var tableOrderhistory: UITableView!
    @IBOutlet weak var tableViewheight: NSLayoutConstraint!
    @IBOutlet weak var scroll_view: UIScrollView!
    
    @IBOutlet weak var lbl_nodataFond: UILabel!
    @IBOutlet weak var viewempty: UIView!
    
    var pageindex = 1
    var lastindex = 0
    var orderHistoryArray: [OrderList] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        lbl_nodataFond.isHidden = true
        viewempty.isHidden = true
        scroll_view.delegate = self
        tableOrderhistory.register(UINib.init(nibName: "OrderCell", bundle: nil), forCellReuseIdentifier: "OrderCell")
        tableOrderhistory.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.pageindex = 1
        self.lastindex = 0
        let param: [String:Any] = ["user_id": getID(),
                                   "theme_id": APP_THEMEID]
        getOrderLists(param)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (Int(self.scroll_view.contentOffset.y) >=  Int(self.scroll_view.contentSize.height - self.scroll_view.frame.size.height)) {
            if self.pageindex != self.lastindex {
                self.pageindex = self.pageindex + 1
                if self.orderHistoryArray.count != 0 {
                    let param: [String:Any] = ["user_id": getID(),
                                               "theme_id": APP_THEMEID]
                    getOrderLists(param)
                }
            }
        }
    }
    
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    func getOrderLists(_ param: [String:Any]) {
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
        AIServiceManager.sharedManager.callPostApi(URL_OrderHistory + "\(pageindex)", params: param, headers) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [String:Any]{
                                if self.pageindex == 1 {
                                    let lastpage = data["last_page"] as! Int
                                    self.lastindex = lastpage
                                    self.orderHistoryArray.removeAll()
                                }
                                if let orderdata = data["data"] as? [[String:Any]]{
                                    self.orderHistoryArray.append(contentsOf: Order.init(orderdata).orderList)
                                    self.tableOrderhistory.reloadData()
                                    self.tableViewheight.constant = CGFloat(80 * self.orderHistoryArray.count)
                                }
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                           let massage = msg["message"] as! String
                           showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
//                    print(json)
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
      }
    }
}

extension OrderHistoryVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.orderHistoryArray.count == 0 {
            self.viewempty.isHidden = false
            self.lbl_nodataFond.isHidden = false
        }else{
            self.viewempty.isHidden = true
            self.lbl_nodataFond.isHidden = true
            return orderHistoryArray.count
        }
        return 0 
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderCell") as! OrderCell
        let orderDetails = orderHistoryArray[indexPath.row]
        cell.lbl_orderId.text = orderDetails.order_id_string
        cell.lbl_orderAmount.text = "\(orderDetails.amount!)"
        
        let dates = DateFormater.getFullDateStringFromString(givenDate: orderDetails.date!)
        cell.lbl_orderdate.text = "Date:\(dates)"
        
        cell.lblStatus.text = orderDetails.delivered_status_string
        cell.lbl_currency.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY)
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "OrderDetailVC") as! OrderDetailVC
        vc.order_id = orderHistoryArray[indexPath.row].id!
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
