//
//  BestsellersVC.swift
//  kiddos
//
//  Created by mac on 13/04/22.
//

import UIKit
import Alamofire
import SDWebImage

class BestsellersVC: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var homeScreenbg: UIImageView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var detailsHeader: UILabel!
    @IBOutlet weak var gotocategories: UIButton!
    @IBOutlet weak var CVCategories: UICollectionView!
    @IBOutlet weak var CVToys: UICollectionView!
    @IBOutlet weak var CVBestsellers: UICollectionView!
    @IBOutlet weak var promotionsDiscount: UIImageView!
    @IBOutlet weak var promotionReturn: UIImageView!
    @IBOutlet weak var productHeader: UILabel!
    @IBOutlet weak var bestsellersHeader: UILabel!
    @IBOutlet weak var categoriesHeader: UILabel!
    @IBOutlet weak var loyalityButton: UIButton!
    @IBOutlet weak var loyalitySubtext: UILabel!
    @IBOutlet weak var loyalitytitle: UILabel!
    @IBOutlet weak var loyalityBGImage: UIImageView!
    @IBOutlet weak var products: UITableView!
    @IBOutlet weak var producsCollection_height: NSLayoutConstraint!
    @IBOutlet weak var lbl_cartindex: UILabel!
    
    //MARK: - Variables
    var arrCategories: [CategoryData] = []
    var arrProducts: [Products] = []
    var arrProduts1: [Products] = []
    var cartindex: Int = 0
    var pageIndex = 1
    var lastIndex = 0
    var productpageindex = 1
    var productlastindex = 0
    var trendingPageindex = 1
    var trendinglastindex = 0
    var selectedindex = 0
    var addwishlist = String()
    var removeWishlist = String()
    var productid = Int()
    var varientId = Int()
    
    //MARK: - ViewLifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CVCategories.register(UINib(nibName: "CategoriesCell", bundle: nil), forCellWithReuseIdentifier: "CategoriesCell")
        CVToys.register(UINib.init(nibName: "ToysCell", bundle: nil), forCellWithReuseIdentifier: "ToysCell")
        CVBestsellers.register(UINib.init(nibName: "ToysCell", bundle: nil), forCellWithReuseIdentifier: "ToysCell")
        products.register(UINib(nibName: "HomeCategoriesCell", bundle: nil), forCellReuseIdentifier: "HomeCategoriesCell")
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lbl_cartindex.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
        let baseParam: [String:Any] = ["theme_id": APP_THEMEID]
        MakeAPICallForBaseURL(baseParam)
    }
    //MARK: - IBActions
    @IBAction func btn_Search(_ sender: UIButton) {
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "BabygiftsVC") as! BabygiftsVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickMenu(_ sender: Any){
        let vc = MenuViewController(nibName: "MenuViewController", bundle: nil)
        vc.modalPresentationStyle = .overCurrentContext
        vc.parentVC = self
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    @IBAction func onClickCart(_ sender: Any){
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "ShoppingCartVC") as! ShoppingCartVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onclickCategories(_ sender: UIButton) {
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "CategoriesVC") as! CategoriesVC
        vc.ishome = "yes"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func showmoreAboutProgam(_ sender: UIButton) {
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "LoyaltyProgramVC") as! LoyaltyProgramVC
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onclickProduct(_ sender: UIButton) {
        if sender.tag == 125 {
            let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "CategoriesDetailVC") as! CategoriesDetailVC
            vc.trending = "Trending"
            vc.isMenu = "Menu"
            navigationController?.pushViewController(vc, animated: true)
        }else if sender.tag == 126{
            let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "CategoriesDetailVC") as! CategoriesDetailVC
            vc.bestseller = "BestSeller"
            vc.isMenu = "Menu"
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    //MARK: - CustomFuctions
    
    
    //MARK: - APIfunctions
    
    func MakeAPICallForBaseURL(_ param: [String:Any]) {
        AIServiceManager.sharedManager.callPostApi(URL_BASE, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let data = json["data"] as? [String:Any]{
                        if let baseURL = data["base_url"] as? String{
                            URL_BASE = baseURL
                        }
                        if let imageURL = data["image_url"] as? String{
                            URL_BASEIMAGE = imageURL
                        }
                        if let paymenturl = data["payment_url"] as? String{
                            URL_PaymentImage = paymenturl
                        }
                        if UserDefaults.standard.value(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) != nil{
                            let guestArray = UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) as! [[String:Any]]
                            UserDefaults.standard.set(guestArray.count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                            self.lbl_cartindex.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
                        }
                        let param: [String: Any] = ["theme_id": APP_THEMEID]
                        self.getHeaderContentData(param)
                    }
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    
    func getCurrentcy(_ param: [String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_Currancy, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1 {
                            if let data = json["data"] as? [String:Any]{
                                let currency = data["currency"] as? String
                                UserDefaults.standard.set(currency, forKey: userDefaultsKeys.KEY_CURRENCY)
                                let currencyName = data["currency_name"] as? String
                                UserDefaults.standard.set(currencyName, forKey: userDefaultsKeys.KEY_CURRENCYNAME)
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
    }
    func getHeaderContentData(_ param: [String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_HeaderContent, params: param, nil) {  response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1 {
                            if let data = json["data"] as? [String:Any]{
                                if let themjson = data["them_json"] as? [String:Any]{
                                    if let homepageHeader = themjson["homepage-header"] as? [String:Any]{
                                        let homepageTitle = homepageHeader["homepage-header-title"] as! String
                                        let homepageSubtitle = homepageHeader["homepage-header-sub-text"] as! String
                                        let homepageImage = homepageHeader["homepage-header-image"] as! String
                                        let homeheaderButton = homepageHeader["homepage-header-button"] as! String
                                        self.headerLabel.text = "\(homepageTitle)"
                                        self.detailsHeader.text = "\(homepageSubtitle)"
                                        self.gotocategories.titleLabel?.text = "\(homeheaderButton)"
                                        let imgurl = getImageFullURL("\(homepageImage)")
                                        self.homeScreenbg.sd_setImage(with: URL(string: imgurl)) { image, error, type, url in
                                            self.homeScreenbg.image = image
                                        }
                                    }
                                    if let homePromotions = themjson["homepage-promotions"] as? [String:Any]{
                                        if let homeProIcons = homePromotions["homepage-promotions-icon-image"] as? [String]{
                                            let imgPromotionURL = getImageFullURL("\(homeProIcons[0])")
                                            self.promotionReturn.sd_setImage(with: URL(string: imgPromotionURL)) { image, error, type, url in
                                                self.promotionReturn.image = image
                                            }
                                            let imgProURL = getImageFullURL("\(homeProIcons[1])")
                                            self.promotionsDiscount.sd_setImage(with: URL(string: imgProURL)) { image, error, type, url in
                                                self.promotionsDiscount.image = image
                                            }
                                        }
                                    }
                                    if let homepageProducts = themjson["homepage-products"] as? [String:Any]{
                                        let homeProductsTitle = homepageProducts["homepage-products-heading"] as! String
                                        self.productHeader.text = "\(homeProductsTitle)"
                                    }
                                    if let homeCatgories = themjson["homepage-categories"] as? [String:Any]{
                                        let homeCategories = homeCatgories["homepage-categories-heading"] as! String
                                        self.categoriesHeader.text = "\(homeCategories)"
                                    }
                                    if let homepageBestsellers = themjson["homepage-bestsellers"] as? [String:Any]{
                                        let homeBestSellerTitle = homepageBestsellers["homepage-bestsellers-heading"] as! String
                                        self.bestsellersHeader.text = "\(homeBestSellerTitle)"
                                    }
                                    if let homepageLoyality = themjson["homepage-loyalty-program"] as? [String:Any]{
                                        let homeLoyalityTitle = homepageLoyality["homepage-loyalty-program-title"] as! String
                                        let homeLoyalitySubTitle = homepageLoyality["homepage-loyalty-program-sub-text"] as! String
                                        let homeLoyalitybutton = homepageLoyality["homepage-loyalty-program-button"] as! String
                                        self.loyalitytitle.text = "\(homeLoyalityTitle)"
                                        self.loyalitySubtext.text = homeLoyalitySubTitle
                                        self.loyalityButton.titleLabel?.text = homeLoyalitybutton
                                        let homeLoyalityImage = homepageLoyality["homepage-loyalty-program-bg-image"] as! String
                                        let imgurl = getImageFullURL("\(homeLoyalityImage)")
                                        self.loyalityBGImage.sd_setImage(with: URL(string: imgurl)) { image, error, type, url in
                                            self.loyalityBGImage.image = image
                                        }
                                    }
                                    let param: [String:Any] = ["theme_id": APP_THEMEID]
                                    self.getCategoriesData(param)
                                    let currencyParam: [String:Any] = ["theme_id": APP_THEMEID]
                                    self.getCurrentcy(currencyParam)
                                    self.trendingPageindex = 1
                                    self.trendinglastindex = 0
                                    if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""
                                    {
                                        self.pageIndex = 1
                                        self.lastIndex = 0
                                        let param: [String:Any] = ["theme_id": APP_THEMEID]
                                        self.getProductsGuestData(param)
                                        self.MakeAPICallforTrendingProductsGuest(["theme_id":APP_THEMEID])
                                    }else{
                                        self.productpageindex = 1
                                        self.productlastindex = 0
                                        let proParam: [String:Any] = ["theme_id": APP_THEMEID]
                                        self.getProductsData(proParam)
                                        self.MakeAPICallforTrending(["theme_id":APP_THEMEID])
                                    }
                                    
                                }
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
    }
    func getCategoriesData(_ param: [String:Any]){
        //        let headers: HTTPHeaders = ["Accept": "application/json"]
        AIServiceManager.sharedManager.callPostApi(URL_HomeCategories, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [String:Any]{
                                if let categories = data["data"] as? [[String:Any]]{
                                    self.arrCategories = HomeCategroies.init(categories).categoriesData
                                    self.CVCategories.reloadData()
                                    self.products.reloadData()
                                }
                                let param: [String:Any] = ["theme_id": APP_THEMEID]
                                self.getextraURL(param)
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
    }
    func getProductsData(_ param: [String:Any]){
        if UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_APP_TOKEN) != nil {
            if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
                let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                            "Accept": "application/json"]
                AIServiceManager.sharedManager.callPostApi(URL_BestSellers + "\(productpageindex)", params: param, headers) { response in
                    switch response.result{
                    case let .success(result):
                        HIDE_CUSTOM_LOADER()
                        if let json = result as? [String: Any]{
                            if let status = json["status"] as? Int{
                                if status == 1{
                                    if let data = json["data"] as? [String:Any]{
                                        if self.productpageindex == 1 {
                                            let lastpage = data["last_page"] as! Int
                                            self.productlastindex = lastpage
                                            self.arrProducts.removeAll()
                                        }
                                        if let catData = data["data"] as? [[String:Any]]{
                                            self.arrProducts.append(contentsOf: KidsToys.init(catData).productdata)
                                            self.CVToys.reloadData()
                                            self.CVBestsellers.reloadData()
                                        }
                                    }
                                    let count = json["count"] as? Int
                                    UserDefaults.standard.set(count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                                    self.lbl_cartindex.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
                                }else if status == 9 {
                                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                    let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                    nav.navigationBar.isHidden = true
                                    keyWindow?.rootViewController = nav
                                }else{
                                    let msg = json["data"] as! [String:Any]
                                    let massage = msg["message"] as! String
                                    showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                                }
                            }
                        }
                    case let .failure(error):
                        print(error.localizedDescription)
                        HIDE_CUSTOM_LOADER()
                        break
                    }
                }
            }
        }
    }
    func addTocart(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            //            print(getID())
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_Addtocart, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                if let stats = json["data"] as? [String:Any]{
                                    let massage = stats["message"] as? String
                                    let status = json["status"] as? Int
                                    let count = stats["count"] as? Int
                                    UserDefaults.standard.set(count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                                    if status == 1 {
                                        let alert = UIAlertController(title: nil, message: massage, preferredStyle: .actionSheet)
                                        let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
                                            self.dismiss(animated: true)
                                        }
                                        
                                        let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
                                            let vc = self.storyboard?.instantiateViewController(identifier: "ShoppingCartVC") as! ShoppingCartVC
                                            self.navigationController?.pushViewController(vc, animated: true)
                                        }
                                        
                                        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                                        alert.addAction(photoLibraryAction)
                                        alert.addAction(cameraAction)
                                        alert.addAction(cancelAction)
                                        self.present(alert, animated: true, completion: nil)
                                    }else if status == 0 {
                                        if massage == "Product has out of stock."{
                                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage!)
                                        }
                                    }
                                    self.lbl_cartindex.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
                                }
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let alertVC = UIAlertController(title: Bundle.main.displayName!, message: "Product already in the cart! Do you want to add it again?", preferredStyle: .alert)
                                let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
                                    let param: [String:Any] = ["user_id": getID(),
                                                               "product_id": self.productid,
                                                               "variant_id": self.varientId,
                                                               "quantity_type": "increase",
                                                               "theme_id": APP_THEMEID]
                                    self.getQtyOfProduct(param)
                                }
                                let noAction = UIAlertAction(title: "No", style: .destructive)
                                alertVC.addAction(noAction)
                                alertVC.addAction(yesAction)
                                self.present(alertVC,animated: true,completion: nil)
                            }
                        }
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    func MakeAPICallforTrending(_ param:[String:Any]){
        let headers: HTTPHeaders = ["Authorization": "Bearer \(getToken())"]
        AIServiceManager.sharedManager.callPostApi(URL_TrendingProducts + "\(trendingPageindex)", params: param, headers) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let data = json["data"] as? [String:Any]{
                        if let msg = data["message"] as? String{
                            self.view.makeToast(msg)
                        }else if let aData = data["data"] as? [[String:Any]]{
                            if self.trendingPageindex == 1 {
                                let lastpage = data["last_page"] as! Int
                                self.trendinglastindex = lastpage
                                self.arrProduts1.removeAll()
                            }
                            self.arrProduts1.append(contentsOf: KidsToys.init(aData).productdata)
                            self.CVToys.reloadData()
                        }
                    }
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    func getWishList(_ param: [String:Any]){
        if UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_APP_TOKEN) != nil {
            if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
                let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                            "Accept": "application/json"]
                AIServiceManager.sharedManager.callPostApi(URL_Wishlist, params: param, headers) { response in
                    switch response.result{
                    case let .success(result):
                        HIDE_CUSTOM_LOADER()
                        if let json = result as? [String: Any]{
                            if let status = json["status"] as? Int{
                                if status == 1{
                                    if let data = json["data"] as? [String:Any]{
                                        let massage = data["message"] as? String
                                        UserDefaults.standard.set(massage, forKey: userDefaultsKeys.KEY_SAVED_CART)
                                        let param: [String:Any] = ["theme_id": APP_THEMEID]
                                        self.getProductsData(param)
                                        self.MakeAPICallforTrending(param)
                                        self.CVToys.reloadData()
                                        self.CVBestsellers.reloadData()
                                    }
                                }else if status == 9 {
                                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                    let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                    nav.navigationBar.isHidden = true
                                    keyWindow?.rootViewController = nav
                                }else{
                                    let msg = json["data"] as! [String:Any]
                                    let massage = msg["message"] as! String
                                    showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                                }
                            }
                            print(json)
                        }
                    case let .failure(error):
                        print(error.localizedDescription)
                        HIDE_CUSTOM_LOADER()
                        break
                    }
                }
            }
        }
    }
    func getextraURL(_ param: [String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_Extra, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [String:Any]{
                                let terms = data["terms"] as? String
                                UserDefaults.standard.set(terms, forKey: userDefaultsKeys.KEY_TERMS)
                                let contectus = data["contact_us"] as? String
                                UserDefaults.standard.set(contectus, forKey: userDefaultsKeys.KEY_CONTECTUS)
                                let returnpolicy = data["return_policy"] as? String
                                UserDefaults.standard.set(returnpolicy, forKey: userDefaultsKeys.KEY_RETURNPOLICY)
                                let insta = data["insta"] as? String
                                UserDefaults.standard.set(insta, forKey: userDefaultsKeys.KEY_INSTA)
                                let youtube = data["youtube"] as? String
                                UserDefaults.standard.set(youtube, forKey: userDefaultsKeys.KEY_YOUTUVBE)
                                let massanger = data["messanger"] as? String
                                UserDefaults.standard.set(massanger, forKey: userDefaultsKeys.KEY_MASSANGER)
                                let twitter = data["twitter"] as? String
                                UserDefaults.standard.set(twitter, forKey: userDefaultsKeys.KEY_TWITTER)
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
                    
                    //                    print(json)
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
    }
    func getProductsGuestData(_ param: [String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_Guest + "\(pageIndex)", params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [String:Any]{
                                if let categories = data["data"] as? [[String:Any]]{
                                    if self.pageIndex == 1 {
                                        let lastpage = data["last_page"] as! Int
                                        self.lastIndex = lastpage
                                        self.arrProducts.removeAll()
                                    }
                                    if let catData = data["data"] as? [[String:Any]]{
                                        self.arrProducts.append(contentsOf: KidsToys.init(catData).productdata)
                                        self.CVToys.reloadData()
                                        self.CVBestsellers.reloadData()
                                    }
                                }
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
    }
    func MakeAPICallforTrendingProductsGuest(_ param:[String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_TrendingProductsGuest + "\(trendingPageindex)", params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let data = json["data"] as? [String:Any]{
                        if let msg = data["message"] as? String{
                            self.view.makeToast(msg)
                        }else if let aData = data["data"] as? [[String:Any]]{
                            if self.trendingPageindex == 1 {
                                let lastpage = data["last_page"] as! Int
                                self.trendinglastindex = lastpage
                                self.arrProduts1.removeAll()
                            }
                            self.arrProduts1.append(contentsOf: KidsToys.init(aData).productdata)
                            self.CVToys.reloadData()
                        }
                    }
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    func getQtyOfProduct(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_cartQty, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                let count = json["count"] as? Int
                                UserDefaults.standard.set(count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let msg = json["data"] as! [String:Any]
                                let massage = msg["message"] as! String
                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
                    }
                    
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    
}

extension BestsellersVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == CVCategories{
            return arrCategories.count
        }else if collectionView == CVToys{
            return arrProduts1.count
        }else{
            return arrProducts.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == CVCategories{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoriesCell", for: indexPath) as! CategoriesCell
            cell.configureCell(arrCategories[indexPath.row])
            return cell
        }else if collectionView == CVToys{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ToysCell", for: indexPath) as! ToysCell
            cell.configreCell(arrProduts1[indexPath.row])
            cell.onclickAddCartClosure = {
                if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == "" {
                    
                    let data = self.arrProduts1[indexPath.row]
                    
                    if UserDefaults.standard.value(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) != nil{
                        
                        var Gest_array = UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) as! [[String:Any]]
                        var iscart = false
                        var cartindex = Int()
                        for i in 0..<Gest_array.count{
                            if Gest_array[i]["product_id"]! as! Int == data.id! && Gest_array[i]["variant_id"]! as! Int == data.defaultVariantID!{
                                iscart = true
                                cartindex = i
                            }
                        }
                        if iscart == false{
                            let image = getImageFullURL("\(data.coverimagepath!)")
                            let cartobj = ["product_id": data.id!,
                                           "image": image,
                                           "name": data.name!,
                                           "orignal_price": data.orignalPrice!,
                                           "discount_price": data.discountPrice!,
                                           "final_price": data.finalPrice!,
                                           "qty": 1,
                                           "variant_id": data.defaultVariantID!,
                                           "variant_name": data.varientName!] as [String : Any]
                            Gest_array.append(cartobj)
                            UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_USERID)
                            UserDefaults.standard.set(Gest_array, forKey: userDefaultsKeys.KEY_GESTUSEROBJ)
                            UserDefaults.standard.set(Gest_array.count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                            
                            let alert = UIAlertController(title: nil, message: "\(data.name!) add successfully", preferredStyle: .actionSheet)
                            let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
                                self.dismiss(animated: true)
                            }
                            
                            let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
                                let vc = self.storyboard?.instantiateViewController(identifier: "ShoppingCartVC") as! ShoppingCartVC
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                            
                            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                            alert.addAction(photoLibraryAction)
                            alert.addAction(cameraAction)
                            alert.addAction(cancelAction)
                            self.present(alert, animated: true, completion: nil)
                        }else{
                            let alert = UIAlertController(title: Bundle.main.applicationName, message: "Product already in the cart! Do you want to add it again?", preferredStyle: .alert)
                            let yesaction = UIAlertAction(title: "Yes", style: .default) { (action) in
                                var cartsList = Gest_array[cartindex]
                                cartsList["qty"] = cartsList["qty"] as! Int + 1
                                Gest_array.remove(at: cartindex)
                                Gest_array.insert(cartsList, at: cartindex)
                                
                                UserDefaults.standard.set(Gest_array, forKey: userDefaultsKeys.KEY_GESTUSEROBJ)
                                UserDefaults.standard.set("\(Gest_array.count)", forKey: userDefaultsKeys.KEY_SAVED_CART)
                            }
                            let noaction = UIAlertAction(title: "No", style: .destructive)
                            alert.addAction(yesaction)
                            alert.addAction(noaction)
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                        self.lbl_cartindex.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
                    }
                }else{
                    self.productid = self.arrProduts1[indexPath.row].id!
                    self.varientId = self.arrProduts1[indexPath.row].defaultVariantID!
                    let param: [String:Any] = ["user_id": getID(),
                                               "product_id": self.arrProduts1[indexPath.row].id!,
                                               "variant_id": self.arrProduts1[indexPath.row].defaultVariantID!,
                                               "qty": 1,
                                               "theme_id": APP_THEMEID]
                    self.addTocart(param)
                }
            }
            if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
                cell.wishlistView.isHidden = true
                cell.btn_fev.isHidden = true
            }else{
                cell.wishlistView.isHidden = false
                cell.btn_fev.isHidden = false
            }
            if arrProduts1[indexPath.row].isinwishlist == false{
                cell.btn_fev.setImage(UIImage(named: "ic_heart"), for: .normal)
            }else if arrProduts1[indexPath.row].isinwishlist == true{
                cell.btn_fev.setImage(UIImage(named: "ic_heartfil"), for: .normal)
            }
            cell.btn_fev.tag = indexPath.row
            cell.btn_fev.addTarget(self, action: #selector(onclickFevBtn), for: .touchUpInside)
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ToysCell", for: indexPath) as! ToysCell
            cell.configreCell(arrProducts[indexPath.row])
            cell.onclickAddCartClosure = {
                if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == "" {
                    let data = self.arrProducts[indexPath.row]
                    if UserDefaults.standard.value(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) != nil{
                        var Gest_array = UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) as! [[String:Any]]
                        var iscart = false
                        for i in 0..<Gest_array.count{
                            if Gest_array[i]["product_id"] as? Int == data.id && Gest_array[i]["variant_id"] as? String == data.varientID{
                                iscart = true
                            }
                        }
                        if iscart == false{
                            let image = getImageFullURL("\(data.coverimagepath!)")
                            let cartobj = ["product_id": data.id!,
                                           "image": image,
                                           "name": data.name!,
                                           "orignal_price": data.orignalPrice!,
                                           "discount_price": data.discountPrice!,
                                           "final_price": data.finalPrice!,
                                           "qty": 1,
                                           "variant_id": data.defaultVariantID!,
                                           "variant_name": data.varientName!] as [String : Any]
                            Gest_array.append(cartobj)
                            UserDefaults.standard.set(Gest_array, forKey: userDefaultsKeys.KEY_GESTUSEROBJ)
                            UserDefaults.standard.set(Gest_array.count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                            
                            let alert = UIAlertController(title: nil, message: "\(data.name!) add successfully", preferredStyle: .actionSheet)
                            let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
                                self.dismiss(animated: true)
                            }
                            
                            let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
                                let vc = self.storyboard?.instantiateViewController(identifier: "ShoppingCartVC") as! ShoppingCartVC
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                            
                            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                            alert.addAction(photoLibraryAction)
                            alert.addAction(cameraAction)
                            alert.addAction(cancelAction)
                            self.present(alert, animated: true, completion: nil)
                        }else{
                            let alert = UIAlertController(title: "disapalynAMe", message: "\(data.name!) add successfully", preferredStyle: .actionSheet)
                            let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
                                self.dismiss(animated: true)
                            }
                            
                            let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
                                let vc = self.storyboard?.instantiateViewController(identifier: "ShoppingCartVC") as! ShoppingCartVC
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                            
                            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                            alert.addAction(photoLibraryAction)
                            alert.addAction(cameraAction)
                            alert.addAction(cancelAction)
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                        self.lbl_cartindex.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
                    }
                }else{
                    self.productid = self.arrProducts[indexPath.row].id!
                    self.varientId = self.arrProducts[indexPath.row].defaultVariantID!
                    let param: [String:Any] = ["user_id": getID(),
                                               "product_id": self.arrProducts[indexPath.row].id!,
                                               "variant_id": self.arrProducts[indexPath.row].defaultVariantID!,
                                               "qty": 1,
                                               "theme_id": APP_THEMEID]
                    self.addTocart(param)
                }
            }
            if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
                cell.wishlistView.isHidden = true
                cell.btn_fev.isHidden = true
            }else{
                cell.wishlistView.isHidden = false
                cell.btn_fev.isHidden = false
            }
            if arrProducts[indexPath.row].isinwishlist == false{
                cell.btn_fev.setImage(UIImage(named: "ic_heart"), for: .normal)
            }else{
                cell.btn_fev.setImage(UIImage(named: "ic_heartfil"), for: .normal)
            }
            cell.btn_fev.tag = indexPath.row
            cell.btn_fev.addTarget(self, action: #selector(onclickBtnWishlistFev), for: .touchUpInside)
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == CVCategories{
            let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
            let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
            let size:CGFloat = (CVCategories.frame.size.width - space) / 2.0
            return CGSize(width: size, height: 142)
        }else if collectionView == CVToys{
            return CGSize(width: collectionView.frame.width / 2, height: 310)
        }else{
            return CGSize(width: collectionView.frame.width / 2, height: 310)
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == CVCategories{
            let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "CategoriesDetailVC") as! CategoriesDetailVC
            vc.catid = arrCategories[indexPath.row].id
            vc.isHome = "yes"
            vc.isMenu = "Menu"
            self.navigationController?.pushViewController(vc, animated: true)
        }else if collectionView == CVToys{
            let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "ToyDesctiptionVC") as! ToyDesctiptionVC
            vc.idpassed = arrProduts1[indexPath.row].id
            vc.varienrid = arrProduts1[indexPath.row].defaultVariantID
            self.navigationController?.pushViewController(vc, animated: true)
        }else if collectionView == CVToys{
            if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""
            {
                self.pageIndex = 1
                self.lastIndex = 0
                let param: [String: Any] = ["theme_id": APP_THEMEID]
                getProductsGuestData(param)
            }else{
                
                self.productpageindex = 1
                self.productlastindex = 0
                let proParam: [String:Any] = ["theme_id": APP_THEMEID]
                getProductsData(proParam)
            }
        }else{
            let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "ToyDesctiptionVC") as! ToyDesctiptionVC
            vc.idpassed = arrProducts[indexPath.row].id
            vc.varienrid = arrProducts[indexPath.row].defaultVariantID
            self.navigationController?.pushViewController(vc, animated: true)
        }
        self.selectedindex = indexPath.item
        self.CVToys.reloadData()
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""
        {
            if collectionView == self.CVToys
            {
                if indexPath.item == self.arrProduts1.count - 1 {
                    if self.trendingPageindex != self.trendinglastindex{
                        self.trendingPageindex += 1
                        if arrProduts1.count != 0 {
                            let param: [String: Any] = ["theme_id": APP_THEMEID]
                            MakeAPICallforTrendingProductsGuest(param)
                        }
                    }
                }
            }
            else if collectionView == self.CVBestsellers{
                if indexPath.item == self.arrProducts.count - 1 {
                    if self.pageIndex != self.lastIndex{
                        self.pageIndex += 1
                        if arrProducts.count != 0 {
                            let param: [String: Any] = ["theme_id": APP_THEMEID]
                            getProductsGuestData(param)
                        }
                    }
                }
            }
        }else{
            if collectionView == self.CVToys
            {
                if indexPath.item == self.arrProduts1.count - 1 {
                    if self.trendingPageindex != self.trendinglastindex{
                        self.trendingPageindex += 1
                        if arrProduts1.count != 0 {
                            let param: [String: Any] = ["theme_id": APP_THEMEID]
                            MakeAPICallforTrending(param)
                        }
                    }
                }
            }
            else if collectionView == self.CVBestsellers
            {
                if indexPath.item == self.arrProducts.count - 1 {
                    if self.productpageindex != self.productlastindex {
                        self.productpageindex += 1
                        if self.arrProducts.count != 0 {
                            let proParam: [String:Any] = ["theme_id": APP_THEMEID]
                            getProductsData(proParam)
                        }
                    }
                }
            }
        }
    }
    @objc func onclickFevBtn(sender:UIButton) {
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            keyWindow?.rootViewController = nav
        }else{
            let data = self.arrProduts1[sender.tag]
            if data.isinwishlist == false{
                let param: [String:Any] = ["user_id": getID(),
                                           "product_id": data.id!,
                                           "wishlist_type": "add",
                                           "theme_id": APP_THEMEID]
                data.isinwishlist = true
                self.getWishList(param)
            }else if data.isinwishlist == true{
                let param: [String:Any] = ["user_id": getID(),
                                           "product_id": data.id!,
                                           "wishlist_type": "remove",
                                           "theme_id": APP_THEMEID]
                data.isinwishlist = false
                self.getWishList(param)
            }
        }
    }
    @objc func onclickBtnWishlistFev(sender:UIButton) {
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            keyWindow?.rootViewController = nav
        }else{
            let data = self.arrProducts[sender.tag]
            if data.isinwishlist == false{
                let param: [String:Any] = ["user_id": getID(),
                                           "product_id": data.id!,
                                           "wishlist_type": "add",
                                           "theme_id": APP_THEMEID]
                data.isinwishlist = true
                self.getWishList(param)
            }else{
                let param: [String:Any] = ["user_id": getID(),
                                           "product_id": data.id!,
                                           "wishlist_type": "remove",
                                           "theme_id": APP_THEMEID]
                data.isinwishlist = false
                self.getWishList(param)
            }
        }
    }
}
extension BestsellersVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCategories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCategoriesCell", for: indexPath) as! HomeCategoriesCell
        cell.configureCell(arrCategories[indexPath.row])
        cell.onclickCategoriesClosure = {
            let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "CategoriesDetailVC") as! CategoriesDetailVC
            vc.catid = self.arrCategories[indexPath.row].id
            vc.isHome = "yes"
            vc.isMenu = "Menu"
            self.navigationController?.pushViewController(vc, animated: true)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 230
    }
}
