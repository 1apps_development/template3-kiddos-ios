//
//  ToyDesctiptionVC.swift
//  kiddos
//
//  Created by mac on 14/04/22.
//

import UIKit
import ImageSlideshow
import Alamofire
import SDWebImage
import iOSDropDown

class DropDownColorsCell: UITableViewCell
{
    var onclickButtonClosure: (()->Void)?
    
    @IBOutlet weak var selectColorTitle: UILabel!
    @IBOutlet weak var clorsSlectionView: UIView!
    @IBOutlet weak var selectclors: UILabel!
    @IBOutlet weak var selectColorText: DropDown!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func onClickView(_ sender: DropDown){
        self.onclickButtonClosure?()
    }
}
class SelectSizeCell: UITableViewCell
{
    @IBOutlet weak var selectSize: UILabel!
    @IBOutlet weak var sizes: UICollectionView!
}
class DescriptionCell: UITableViewCell
{
    @IBOutlet weak var lbl_discripttitle: UILabel!
    @IBOutlet weak var lbl_discription: UILabel!
    @IBOutlet weak var btn_discript: UIButton!
    @IBOutlet weak var btn_expand: UIButton!
}
class ToyDesctiptionVC: UIViewController, ImageSlideshowDelegate {
    
    //MARK:  - Outlets
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var productRateing: CosmosView!
    @IBOutlet weak var discountProductPrice: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var productDetails: UICollectionView!
    @IBOutlet weak var CVProduct: UICollectionView!
    @IBOutlet weak var bgimage: ImageSlideshow!
    @IBOutlet weak var tableViewVarientList: UITableView!
    @IBOutlet weak var discriptionsList: UITableView!
    @IBOutlet weak var sizeTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var discriptionTableHeight: NSLayoutConstraint!
    @IBOutlet weak var reviewCollection: UICollectionView!
    @IBOutlet weak var heightRateing: NSLayoutConstraint!
    @IBOutlet weak var reteView: UIView!
    @IBOutlet weak var cartIndexLabel: UILabel!
    @IBOutlet weak var lbl_OutofStock: UILabel!
    @IBOutlet weak var addReview: UIButton!
    @IBOutlet weak var btn_addTocart: UIButton!
    
    //MARK: - Variables
    var idpassed: Int?
    var arrProductsDetails: [Products] = []
    var arrSubProduct: [ProductImages] = []
    var arrProductImages: [SDWebImageSource] = []
    var arrVarients: [Varient] = []
    var disciptions: [OtherDis] = []
    var arrReviews: [Review] = []
    var valueArray: [String] = []
    var varientColor: [String] = []
    var cellHeight = Double()
    var varienrid: Int?
    var SelectVarientArray: [String] = []
    var Selected_Variant_Name = String()
    var stock: Int?
    var itemid: String?
    var productid = Int()
    var varientId = Int()
    var gestUserdata: [String:Any] = [:]
    
    //MARK: - Viewlifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.sizeTableViewHeight.constant = 0.0
        self.lbl_OutofStock.isHidden = true
        self.addReview.isHidden = true
        productDetails.register(UINib(nibName: "ProductDetailsCell", bundle: nil), forCellWithReuseIdentifier: "ProductDetailsCell")
        CVProduct.register(UINib(nibName: "ProductDetailsCell", bundle: nil), forCellWithReuseIdentifier: "ProductDetailsCell")
        reviewCollection.register(UINib(nibName: "TestimetionsCell", bundle: nil), forCellWithReuseIdentifier: "TestimetionsCell")
        
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == "" {
            self.addReview.isHidden = true
            let param: [String:Any] = ["id": idpassed!,
                                       "theme_id": APP_THEMEID]
            getProductInfoGuest(param)
        }else{
            let param: [String:Any] = ["id": idpassed!,
                                       "theme_id": APP_THEMEID]
            getProductDetails(param)
        }
        heightRateing.constant = 0
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.cartIndexLabel.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
    }
    //MARK: - CustomFunctions
    func imageSliderData() {
        
        self.bgimage.slideshowInterval = 3.0
        self.bgimage.pageIndicatorPosition = .init(horizontal: .center, vertical: .customBottom(padding: 10.0))
        self.bgimage.contentScaleMode = UIView.ContentMode.scaleAspectFit
        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = UIColor.white
        pageControl.pageIndicatorTintColor = UIColor.lightGray
        self.bgimage.pageIndicator = pageControl
        self.bgimage.setImageInputs(self.arrProductImages)
        self.bgimage.delegate = self
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTapImage))
        self.bgimage.addGestureRecognizer(recognizer)
    }
    @objc func didTapImage() {
        self.bgimage.presentFullScreenController(from: self)
    }
    //MARK: - APIfuctions
    
    func getProductDetails(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            print(getID())
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_ProductDetails, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                if let prodata = json["data"] as? [String:Any]{
                                    if let proDetailsdata = prodata["product_info"] as? [String:Any]{
                                        self.gestUserdata = proDetailsdata
                                        let proTitle = proDetailsdata["name"] as? String
                                        self.productTitle.text = proTitle
                                        let id = proDetailsdata["id"] as? Int
                                        self.productid = id!
                                        let variid = proDetailsdata["default_variant_id"] as? Int
                                        self.varientId = variid!
                                        if let discription = proDetailsdata["other_description_array"] as? [[String:Any]]{
                                            self.disciptions = Discription.init(discription).discription
                                            self.discriptionsList.reloadData()
                                        }
                                        let discuntProPrice = proDetailsdata["discount_price"] as? String
                                        let attributedText = NSAttributedString(
                                            string: self.discountProductPrice.text!,
                                            attributes: [.strikethroughStyle: NSUnderlineStyle.single.rawValue]
                                        )
                                        self.discountProductPrice.attributedText = attributedText
                                        self.discountProductPrice.text = discuntProPrice
                                        let proprice = proDetailsdata["final_price"] as? String
                                        self.productPrice.text = proprice
                                        
                                        let review = proDetailsdata["is_review"] as? Int
                                        if review == 1 {
                                            self.addReview.isHidden = true
                                        }
                                        else if review == 0{
                                            self.addReview.isHidden = false
                                        }
                                        else{
                                            self.addReview.isHidden = false
                                        }
                                        let avragerate = proDetailsdata["average_rating"] as? Double
                                        self.productRateing.rating = avragerate!
                                        let stock = proDetailsdata["product_stock"] as? Int
                                        self.stock = stock
                                        let defaultvarientid = proDetailsdata["default_variant_id"] as? Int
                                        if defaultvarientid == 0 {
                                            if stock! <= 0 {
                                                self.lbl_OutofStock.isHidden = false
                                                self.btn_addTocart.setTitle("Notify me when available", for: .normal)
                                            }
                                            else {
                                                self.lbl_OutofStock.isHidden = true
                                                self.btn_addTocart.setTitle("Add to cart", for: .normal)
                                            }
                                        }
                                        else {
                                            if self.arrVarients.count != 0 {
                                                self.sizeTableViewHeight.constant = CGFloat(self.arrVarients.count * 100)
                                                for varient in self.arrVarients {
                                                    if varient.value?.count != 0 {
                                                        self.SelectVarientArray.append(varient.value![0])
                                                    }
                                                }
                                                let param: [String:Any] = ["product_id": self.productid,
                                                                           "variant_sku": self.SelectVarientArray.joined(separator: "-"),
                                                                           "theme_id": APP_THEMEID]
                                                self.MakeAPICallforVarientStock(param)
                                            }
                                        }
                                    }
                                    self.arrProductImages.removeAll()
                                    if let varient = prodata["product_image"] as? [[String:Any]]{
                                        self.arrSubProduct = ProductSubImage.init(varient).subproductdata
                                        let arrImage = self.arrSubProduct.map({$0.imagePath})
                                        for producturl in arrImage{
                                            let getproductURL = getImageFullURL("\(producturl!)")
                                            let sdwebimage = SDWebImageSource(url: URL(string: "\(getproductURL)")!, placeholder: UIImage(named: ""))
                                            self.arrProductImages.append(sdwebimage)
                                        }
                                        self.imageSliderData()
                                        self.productDetails.reloadData()
                                        self.CVProduct.reloadData()
                                    }
                                    if let productReview = prodata["product_Review"] as? [[String:Any]] {
                                        self.arrReviews = ProductReview.init(productReview).productreview
                                        self.reviewCollection.reloadData()
                                    }
                                    if let typeVarients = prodata["variant"] as? [[String:Any]] {
                                        self.arrVarients = ProductVarient.init(typeVarients).varients
                                        self.tableViewVarientList.reloadData()
                                        self.sizeTableViewHeight.constant = CGFloat(self.arrVarients.count * 125)
                                    }
                                    if self.arrVarients.count != 0 {
                                        self.sizeTableViewHeight.constant = CGFloat(self.arrVarients.count * 125)
                                        for varient in self.arrVarients {
                                            if varient.value?.count != 0 {
                                                self.SelectVarientArray.append(varient.value![0])
                                            }
                                        }
                                        let param: [String:Any] = ["product_id": self.idpassed!,
                                                                   "variant_sku": self.SelectVarientArray.joined(separator: "-"),
                                                                   "theme_id": APP_THEMEID
                                        ]
                                        self.MakeAPICallforVarientStock(param)
                                    }
                                    if self.arrReviews.count != 0 {
                                        self.reteView.isHidden = false
                                        self.heightRateing.constant = 270.0
                                    }else{
                                        self.reteView.isHidden = true
                                        self.heightRateing.constant = 0.0
                                    }
                                }
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let msg = json["data"] as! [String:Any]
                                let massage = msg["message"] as! String
                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    func getProductInfoGuest(_ param: [String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_ProductDetailsGuest, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let prodata = json["data"] as? [String:Any]{
                                if let proDetailsdata = prodata["product_info"] as? [String:Any]{
                                    self.gestUserdata = proDetailsdata
                                    let proTitle = proDetailsdata["name"] as? String
                                    self.productTitle.text = proTitle
                                    let id = proDetailsdata["id"] as? Int
                                    self.productid = id!
                                    let variid = proDetailsdata["default_variant_id"] as? Int
                                    self.varientId = variid!
                                    if let discription = proDetailsdata["other_description_array"] as? [[String:Any]]{
                                        self.disciptions = Discription.init(discription).discription
                                        self.discriptionsList.reloadData()
                                    }
                                    let discuntProPrice = proDetailsdata["discount_price"] as? String
                                    let attributedText = NSAttributedString(
                                        string: self.discountProductPrice.text!,
                                        attributes: [.strikethroughStyle: NSUnderlineStyle.single.rawValue]
                                    )
                                    self.discountProductPrice.attributedText = attributedText
                                    self.discountProductPrice.text = discuntProPrice
                                    let proprice = proDetailsdata["original_price"] as? String
                                    self.productPrice.text = proprice
                                    
                                    let avragerate = proDetailsdata["average_rating"] as? Double
                                    self.productRateing.rating = avragerate!
                                    let stock = proDetailsdata["product_stock"] as? Int
                                    self.stock = stock
                                    let defaultvarientid = proDetailsdata["default_variant_id"] as? Int
                                    if defaultvarientid == 0 {
                                        if stock! <= 0 {
                                            self.lbl_OutofStock.isHidden = false
                                            self.btn_addTocart.setTitle("Notify me when available", for: .normal)
                                        }else{
                                            self.lbl_OutofStock.isHidden = true
                                            self.btn_addTocart.setTitle("Add to cart", for: .normal)
                                        }
                                    }else{
                                        if self.arrVarients.count != 0 {
                                            self.sizeTableViewHeight.constant = CGFloat(self.arrVarients.count * 100)
                                            for varient in self.arrVarients {
                                                if varient.value?.count != 0 {
                                                    self.SelectVarientArray.append(varient.value![0])
                                                }
                                            }
                                            let param: [String:Any] = ["product_id": self.productid,
                                                                       "variant_sku": self.SelectVarientArray.joined(separator: "-"),
                                                                       "theme_id": APP_THEMEID]
                                            self.MakeAPICallforVarientStock(param)
                                        }
                                    }
                                }
                                self.arrProductImages.removeAll()
                                if let varient = prodata["product_image"] as? [[String:Any]]{
                                    self.arrSubProduct = ProductSubImage.init(varient).subproductdata
                                    let arrImage = self.arrSubProduct.map({$0.imagePath})
                                    for producturl in arrImage{
                                        let getproductURL = getImageFullURL("/\(producturl!)")
                                        let sdwebimage = SDWebImageSource(url: URL(string: "\(getproductURL)")!, placeholder: UIImage(named: ""))
                                        self.arrProductImages.append(sdwebimage)
                                    }
                                    self.imageSliderData()
                                    self.productDetails.reloadData()
                                    self.CVProduct.reloadData()
                                }
                                if let productReview = prodata["product_Review"] as? [[String:Any]]{
                                    self.arrReviews = ProductReview.init(productReview).productreview
                                    self.reviewCollection.reloadData()
                                }
                                if let typeVarients = prodata["variant"] as? [[String:Any]]{
                                    self.arrVarients = ProductVarient.init(typeVarients).varients
                                    self.tableViewVarientList.reloadData()
                                }
                                if self.arrReviews.count != 0 {
                                    self.reteView.isHidden = false
                                    self.heightRateing.constant = 270.0
                                }else{
                                    self.reteView.isHidden = true
                                    self.heightRateing.constant = 0.0
                                }
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
                    
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    func addTocart(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            //            print(getID())
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_Addtocart, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            let massage = json["message"] as? String
                            if status == 1{
                                if let stats = json["data"] as? [String:Any]{
                                    let count = stats["count"] as? Int
                                    UserDefaults.standard.set(count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                                    self.cartIndexLabel.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
                                    let alert = UIAlertController(title: nil, message: massage, preferredStyle: .actionSheet)
                                    let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
                                        self.dismiss(animated: true)
                                    }
                                    
                                    let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
                                        let vc = self.storyboard?.instantiateViewController(identifier: "ShoppingCartVC") as! ShoppingCartVC
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    }
                                    
                                    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                                    alert.addAction(photoLibraryAction)
                                    alert.addAction(cameraAction)
                                    alert.addAction(cancelAction)
                                    self.present(alert, animated: true, completion: nil)
                                }else if status == 0 {
                                    if massage == "Product has out of stock."{
                                        showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage!)
                                    }
                                }
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let alertVC = UIAlertController(title: Bundle.main.displayName!, message: "Product already in the cart! Do you want to add it again?", preferredStyle: .alert)
                                let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
                                    let param: [String:Any] = ["user_id": getID(),
                                                               "product_id": self.productid,
                                                               "variant_id": self.varientId,
                                                               "quantity_type": "increase",
                                                               "theme_id": APP_THEMEID]
                                    self.getQtyOfProduct(param)
                                }
                                let noAction = UIAlertAction(title: "No", style: .destructive)
                                alertVC.addAction(noAction)
                                alertVC.addAction(yesAction)
                                self.present(alertVC,animated: true,completion: nil)
                            }
                        }
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    func getQtyOfProduct(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_cartQty, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                let count = json["count"] as? Int
                                UserDefaults.standard.set(count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let msg = json["data"] as! [String:Any]
                                let massage = msg["message"] as! String
                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
                    }
                    
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    func MakeAPICallforVarientStock(_ param:[String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_VarientStock, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let data = json["data"] as? [String:Any]{
                        if let msg = data["message"] as? String{
                            self.view.makeToast(msg)
                        }
                        let id = data["id"] as! Int
                        self.varienrid = id
                        let varient = data["variant"] as! String
                        self.Selected_Variant_Name = varient
                        let finalPrice = data["final_price"] as! String
                        self.productPrice.text = finalPrice
                        self.gestUserdata["final_price"] = finalPrice
                        let stock = data["stock"] as! Int
                        self.stock = stock
                        if stock <= 0 {
                            self.lbl_OutofStock.isHidden = false
                            self.btn_addTocart.setTitle("Notify me when available", for: .normal)
                        }else{
                            self.lbl_OutofStock.isHidden = true
                            self.btn_addTocart.setTitle("Add to cart", for: .normal)
                        }
                    }
                    printD(json)
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    
    func notifyUser(_ param: [String:Any]) {
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_notifyUser, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let msg = json["data"] as! [String:Any]
                                let massage = msg["message"] as! String
                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
                        print(json)
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    
    func addrateing(_ param: [String:Any]) {
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            //            print(getID())
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_ProductRateing, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                print(json)
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let msg = json["data"] as! [String:Any]
                                let massage = msg["message"] as! String
                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
                        
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    
    //MARK: - Actions
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onclickCartButton(_ sender: UIButton) {
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "ShoppingCartVC") as! ShoppingCartVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func addreView(_ sender: UIButton) {
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "AddRateingVC") as! AddRateingVC
        vc.product_id = idpassed!
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onclickreturn(_ sender: UIButton) {
        guard let url = URL(string: UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_RETURNPOLICY)!) else {
            return
        }
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    @IBAction func onclickAddtocart(_ sender: UIButton) {
        if self.stock == 0{
            if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                nav.navigationBar.isHidden = true
                keyWindow?.rootViewController = nav
            }else{
                let param: [String: Any] = ["product_id": idpassed!,
                                            "user_id": getID(),
                                            "theme_id": APP_THEMEID]
                self.notifyUser(param)
            }
        }else{
            if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
                if UserDefaults.standard.value(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) != nil{
                    
                    var Gest_array = UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) as! [[String:Any]]
                    var iscart = false
                    var cartindex = Int()
                    for i in 0..<Gest_array.count{
                        if Gest_array[i]["product_id"]! as! Int == gestUserdata["id"] as! Int && Gest_array[i]["variant_id"]! as! Int == self.varienrid!{
                            iscart = true
                            cartindex = i
                        }
                    }
                    if iscart == false{
                        let cartobj = ["product_id": gestUserdata["id"] as! Int,
                                       "image": gestUserdata["cover_image_url"] as! String,
                                       "name": gestUserdata["name"] as! String,
                                       "orignal_price": gestUserdata["original_price"] as! String,
                                       "discount_price": gestUserdata["discount_price"] as! String,
                                       "final_price": gestUserdata["final_price"] as! String,
                                       "qty": 1,
                                       "variant_id": varienrid!,
                                       "variant_name":Selected_Variant_Name] as [String : Any]
                        Gest_array.append(cartobj)
                        UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_USERID)
                        UserDefaults.standard.set(Gest_array, forKey: userDefaultsKeys.KEY_GESTUSEROBJ)
                        UserDefaults.standard.set(Gest_array.count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                        
                        let alert = UIAlertController(title: nil, message: "\(gestUserdata["name"] as! String) add successfully", preferredStyle: .actionSheet)
                        let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
                            self.dismiss(animated: true)
                        }
                        
                        let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
                            let vc = self.storyboard?.instantiateViewController(identifier: "ShoppingCartVC") as! ShoppingCartVC
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                        
                        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                        alert.addAction(photoLibraryAction)
                        alert.addAction(cameraAction)
                        alert.addAction(cancelAction)
                        self.present(alert, animated: true, completion: nil)
                    }else{
                        let alert = UIAlertController(title: Bundle.main.applicationName, message: "Product already in the cart! Do you want to add it again?", preferredStyle: .alert)
                        let yesaction = UIAlertAction(title: "Yes", style: .default) { (action) in
                            var cartsList = Gest_array[cartindex]
                            cartsList["qty"] = cartsList["qty"] as! Int + 1
                            Gest_array.remove(at: cartindex)
                            Gest_array.insert(cartsList, at: cartindex)
                            
                            UserDefaults.standard.set(Gest_array, forKey: userDefaultsKeys.KEY_GESTUSEROBJ)
                            UserDefaults.standard.set("\(Gest_array.count)", forKey: userDefaultsKeys.KEY_SAVED_CART)
                        }
                        let noaction = UIAlertAction(title: "No", style: .destructive)
                        alert.addAction(yesaction)
                        alert.addAction(noaction)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                self.cartIndexLabel.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
            }else{
                let param: [String:Any] = ["user_id": getID(),
                                           "product_id": idpassed!,
                                           "variant_id": varienrid!,
                                           "qty": 1,
                                           "theme_id": APP_THEMEID]
                addTocart(param)
            }
        }
    }
    
}
extension ToyDesctiptionVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == productDetails{
            return arrSubProduct.count
        }else if collectionView == CVProduct{
            return arrSubProduct.count
        }else if collectionView == reviewCollection{
            return arrReviews.count
        }
        else{
            return valueArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == productDetails{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductDetailsCell", for: indexPath) as! ProductDetailsCell
            cell.configureCell(arrSubProduct[indexPath.row])
            return cell
        }else if collectionView == CVProduct{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductDetailsCell", for: indexPath) as! ProductDetailsCell
            cell.configureCell(arrSubProduct[indexPath.row])
            return cell
        }else if collectionView == reviewCollection{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TestimetionsCell", for: indexPath) as! TestimetionsCell
            cell.reviewDescription.text = arrReviews[indexPath.row].reviewProduct
            cell.rateingView.rating = arrReviews[indexPath.row].reteing!
            cell.lbl_rates.text = "\(arrReviews[indexPath.row].reteing!)"
            return cell
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as! CollectionViewCell
            let data = self.valueArray[indexPath.item]
            cell.lbl_size.text = "\(data)"
            
            if data == self.SelectVarientArray[collectionView.tag]
            {
                cell.lbl_View.backgroundColor = hexStringToUIColor(hex: "#0D1E1C")
                cell.lbl_View.applyCornerRadius(8)
                cell.lbl_size.textColor = UIColor.white
            }
            else{
                cell.lbl_View.backgroundColor = .clear
                cell.lbl_View.applyBorder(.black, width: 1)
                cell.lbl_View.applyCornerRadius(8)
                cell.lbl_size.textColor = UIColor.black
            }
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == productDetails{
            let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
            let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
            let size:CGFloat = (productDetails.frame.size.width - space) / 3
            return CGSize(width: size, height: 70)
        }else if collectionView == CVProduct{
            let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
            let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
            let size:CGFloat = (CVProduct.frame.size.width - space) / 2
            return CGSize(width: size, height: 138)
        }else if collectionView == reviewCollection{
            let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
            let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
            let size:CGFloat = (CVProduct.frame.size.width - space)
            return CGSize(width: size, height: 169)
        }
        else{
            return CGSize(width: 55, height: 55)
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == productDetails {
            self.bgimage.setCurrentPage(indexPath.row, animated: true)
        }else if collectionView == reviewCollection{
            
        }else if collectionView == CVProduct{
            
        }else{
            let data = arrVarients[collectionView.tag].value!
            self.SelectVarientArray.remove(at: collectionView.tag)
            self.SelectVarientArray.insert(data[indexPath.item], at: collectionView.tag)
            self.tableViewVarientList.reloadData()
            let param: [String:Any] = ["product_id": self.idpassed!,
                                       "variant_sku": self.SelectVarientArray.joined(separator: "-"),
                                       "theme_id": APP_THEMEID
            ]
            self.MakeAPICallforVarientStock(param)
        }
        
    }
}
extension ToyDesctiptionVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableViewVarientList{
            return arrVarients.count
        }else {
            return disciptions.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tableViewVarientList{
            let dataVarient = self.arrVarients[indexPath.row].type
            if dataVarient == "dropdown"{
                let cell = tableViewVarientList.dequeueReusableCell(withIdentifier: "DropDownColorsCell", for: indexPath) as! DropDownColorsCell
                cell.selectColorTitle.text = "Select \(arrVarients[indexPath.row].name!)"
                cell.selectclors.text = arrVarients[indexPath.row].name
                cell.selectclors.text = self.SelectVarientArray[indexPath.row]
                self.varientColor = arrVarients[indexPath.row].value!
                cell.onclickButtonClosure = {
                    cell.selectColorText.textColor = .clear
                    cell.selectColorText.checkMarkEnabled = false
                    cell.selectColorText.optionArray = self.arrVarients[indexPath.row].value!
                    cell.selectColorText.selectedRowColor = hexStringToUIColor(hex: "#B7D4C5")
                    cell.selectColorText.rowBackgroundColor  = hexStringToUIColor(hex: "#B7D4C5")
                    cell.selectColorText.didSelect(completion: { selected, index, id in
                        cell.selectclors.text = selected
                        self.SelectVarientArray.remove(at: cell.selectColorText.tag)
                        self.SelectVarientArray.insert(selected, at: cell.selectColorText.tag)
                        self.tableViewVarientList.reloadData()
                        let param: [String:Any] = ["product_id": self.idpassed!,
                                                   "variant_sku": self.SelectVarientArray.joined(separator: "-"),
                                                   "theme_id": APP_THEMEID
                        ]
                        self.MakeAPICallforVarientStock(param)
                    })
                }
                return cell
            }else{
                let cell = tableViewVarientList.dequeueReusableCell(withIdentifier: "SelectSizeCell", for: indexPath) as! SelectSizeCell
                cell.selectSize.text = arrVarients[indexPath.row].name
                self.valueArray = arrVarients[indexPath.row].value!
                cell.sizes.tag = indexPath.row
                cell.sizes.delegate = self
                cell.sizes.dataSource = self
                cell.sizes.reloadData()
                return cell
            }
            
        }else{
            let cell = discriptionsList.dequeueReusableCell(withIdentifier: "DescriptionCell", for: indexPath) as! DescriptionCell
            cell.lbl_discripttitle.text = disciptions[indexPath.row].title
            cell.lbl_discription.text = disciptions[indexPath.row].descrips
            if disciptions[indexPath.row].isSelected == false{
                cell.btn_expand.setTitle("+", for: .normal)
            }else{
                cell.btn_expand.setTitle("-", for: .normal)
            }
            cell.btn_expand.tag = indexPath.row
            cell.btn_expand.addTarget(self, action: #selector(btn_tapExpand), for: .touchUpInside)
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tableViewVarientList {
            return 120
        }
        else {
            if self.disciptions[indexPath.row].isSelected == false{
                return 45
            }
            else {
                let height = ((disciptions[indexPath.row].descrips?.heightWithWidthAndFont(UIScreen.main.bounds.width - 36, font: UIFont(name: "Outfit-Medium", size: 14)!))!) + 16.0 + 45.0
                return height
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    @objc func btn_tapExpand(sender:UIButton){
        let data = disciptions[sender.tag]
        data.isSelected = data.isSelected == false ? true : false
        self.disciptions.remove(at: sender.tag)
        self.disciptions.insert(data, at: sender.tag)
        cellHeight = 0.0
        for maindata in disciptions{
            if maindata.isSelected == false{
                cellHeight = cellHeight + 45.0
            }
            else {
                let height = (maindata.descrips?.heightWithWidthAndFont(UIScreen.main.bounds.width - 36, font: UIFont(name: "Outfit-Medium", size: 14)!))! + 16.0 + 45.0
                cellHeight = cellHeight + height
            }
        }
        self.discriptionsList.reloadData()
        self.discriptionTableHeight.constant = cellHeight
    }
    
}
extension ToyDesctiptionVC : FeedbackDelegate
{
    func refreshData(id: Int, rating_no: String, title: String, description: String) {
        let param: [String:Any] = ["id": id,
                                   "user_id": getID(),
                                   "rating_no": rating_no,
                                   "title": title,
                                   "description": description,
                                   "theme_id": APP_THEMEID]
        self.addrateing(param)
    }
    
}
