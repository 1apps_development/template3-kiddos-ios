//
//  CategoriesVC.swift
//  Ticket-GO
//
//  Created by mac on 31/03/22.
//

import UIKit
import SDWebImage

class CategoriesVC: UIViewController, UIScrollViewDelegate{
    
    //MARK: - Outlets
    @IBOutlet weak var bottomCircleMenu: UIView!
    @IBOutlet weak var CVCategories: UICollectionView!
    @IBOutlet weak var lbl_count: UILabel!
    @IBOutlet weak var btn_back: UIView!
    @IBOutlet weak var scroll_View: UIScrollView!
    @IBOutlet weak var categoryCollectionHeight: NSLayoutConstraint!
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view4: UIView!
    
    // View 1
    @IBOutlet weak var cat_title_View_1: UILabel!
    @IBOutlet weak var btn_cat_View_1: UIButton!
    @IBOutlet weak var cat_Image_view_1: UIImageView!
    
    //View 2
    
    @IBOutlet weak var cat_title_1_View_2: UILabel!
    @IBOutlet weak var btn_cat_1_View_2: UIButton!
    @IBOutlet weak var cat_Image_1_view_2: UIImageView!
    
    @IBOutlet weak var cat_title_2_View_2: UILabel!
    @IBOutlet weak var btn_cat_2_View_2: UIButton!
    @IBOutlet weak var cat_Image_2_view_2: UIImageView!
    
    //View 3
    @IBOutlet weak var cat_title_1_View_3: UILabel!
    @IBOutlet weak var btn_cat_1_View_3: UIButton!
    @IBOutlet weak var cat_Image_1_view_3: UIImageView!
    
    @IBOutlet weak var cat_title_2_View_3: UILabel!
    @IBOutlet weak var btn_cat_2_View_3: UIButton!
    @IBOutlet weak var cat_Image_2_view_3: UIImageView!
    
    @IBOutlet weak var cat_title_3_View_3: UILabel!
    @IBOutlet weak var btn_cat_3_View_3: UIButton!
    @IBOutlet weak var cat_Image_3_view_3: UIImageView!
    
    //View 4
    
    @IBOutlet weak var cat_title_1_View_4: UILabel!
    @IBOutlet weak var btn_cat_1_View_4: UIButton!
    @IBOutlet weak var cat_Image_1_view_4: UIImageView!
    
    @IBOutlet weak var cat_title_2_View_4: UILabel!
    @IBOutlet weak var btn_cat_2_View_4: UIButton!
    @IBOutlet weak var cat_Image_2_view_4: UIImageView!
    
    @IBOutlet weak var cat_title_3_View_4: UILabel!
    @IBOutlet weak var btn_cat_3_View_4: UIButton!
    @IBOutlet weak var cat_Image_3_view_4: UIImageView!
    
    @IBOutlet weak var cat_title_4_View_4: UILabel!
    @IBOutlet weak var btn_cat_4_View_4: UIButton!
    @IBOutlet weak var cat_Image_4_view_4: UIImageView!
    
    
    //MARK: - Variables
    var arrCategories: [CategoryData] = []
    var isSelectedAgain:Bool = false
    var pageindex = 1
    var lastindex = 0
    var ishome = String()

    //MARK: - Viewlifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view1.isHidden = true
        view2.isHidden = true
        view3.isHidden = true
        view4.isHidden = true
        scroll_View.delegate = self
        CVCategories.register(UINib(nibName: "CategoriesCell", bundle: nil), forCellWithReuseIdentifier: "CategoriesCell")
        if self.ishome == "yes"{
            btn_back.isHidden = false
        }else{
            btn_back.isHidden = true
        }
        let param: [String: Any] = ["theme_id": APP_THEMEID]
        getCategoriesData(param)
        NotificationCenter.default.addObserver(self, selector: #selector(notificationReceived(_:)), name: Notification.Name(rawValue: "NOTIFICATION_CENTER_TAB"), object: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.lbl_count.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
        self.tabBarController?.tabBar.isHidden = false
        self.bottomCircleMenu.isHidden = false
        
    }
    //MARK: - Actions
//    @IBAction func onClickMenu(_ sender: Any){
//        let vc = MenuViewController(nibName: "MenuViewController", bundle: nil)
//        vc.parentVC = self
//        vc.selectedItem = "Invoices"
//        vc.view.backgroundColor = .clear
//        vc.modalPresentationStyle = .overCurrentContext
//        self.navigationController?.present(vc, animated: true)
//    }
    
    @IBAction func onClickCart(_ sender: Any){
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "ShoppingCartVC") as! ShoppingCartVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }


    @IBAction func onclickBottomCircleMenu(_ sender: UIButton) {
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "CategoriesDetailVC") as! CategoriesDetailVC
        
        // View - 1
        if sender.tag == 11 {
            vc.catid = arrCategories[0].id
            vc.selectedIndex = 1
        }
        //View - 2
        else if sender.tag == 21{
            vc.catid = arrCategories[0].id
            vc.selectedIndex = 1
        }else if sender.tag == 22 {
            vc.catid = arrCategories[1].id
            vc.selectedIndex = 2
        }
        
        // View - 3
        
        else if sender.tag == 31{
            vc.catid = arrCategories[0].id
            vc.selectedIndex = 1
        }else if sender.tag == 32 {
            vc.catid = arrCategories[1].id
            vc.selectedIndex = 2
        }else if sender.tag == 33 {
            vc.catid = arrCategories[2].id
            vc.selectedIndex = 3
        }
        
        // View - 4
        
        else if sender.tag == 41{
            vc.catid = arrCategories[0].id
            vc.selectedIndex = 1
        }else if sender.tag == 42 {
            vc.catid = arrCategories[1].id
            vc.selectedIndex = 2
        }else if sender.tag == 43 {
            vc.catid = arrCategories[2].id
            vc.selectedIndex = 3
        }else if sender.tag == 44{
            vc.catid = arrCategories[3].id
            vc.selectedIndex = 4
        }
        vc.isHome = "yes"
        vc.isMenu = "Menu"
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func notificationReceived(_ noti: Notification){
        isSelectedAgain = true
        if isSelectedAgain{
            UIView.animate(withDuration: 0.3) {
                self.bottomCircleMenu.isHidden = false
            }
        }
    }
    
    //MARK: - Custom functions
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.bottomCircleMenu.isHidden = true
    }
    
    //MARK: - API functions
    func getCategoriesData(_ param: [String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_HomeCategories + "\(pageindex)", params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [String:Any]{
                                if self.pageindex == 1 {
                                    let lastpage = data["last_page"] as! Int
                                    self.lastindex = lastpage
                                    self.arrCategories.removeAll()
                                }
                                if let categories = data["data"] as? [[String:Any]]{
                                    self.arrCategories = HomeCategroies.init(categories).categoriesData
                                    //self.categoryCollectionHeight.constant = CGFloat(self.arrCategories.count * 150)
                                    self.view.layoutIfNeeded()
                                    self.CVCategories.reloadData()
                                }
                                let first4 = Array(self.arrCategories.prefix(4))
                                if first4.count == 1 {
                                    self.view1.isHidden = false
                                    self.cat_title_View_1.text = self.arrCategories[0].name
                                    
                                    self.cat_Image_view_1.sd_setImage(with: URL(string: getImageFullURL("\(self.arrCategories[0].iconpath!)")), placeholderImage: UIImage(named: ""))
                                    
                                    self.btn_cat_View_1.layer.cornerRadius = self.btn_cat_View_1.frame.height / 2
                                }
                                if first4.count == 2 {
                                    self.view2.isHidden = false
                                    self.cat_title_1_View_2.text = self.arrCategories[0].name
                                    self.cat_title_2_View_2.text = self.arrCategories[1].name
                                    
                                    self.cat_Image_1_view_2.sd_setImage(with: URL(string: getImageFullURL("\(self.arrCategories[0].iconpath!)")), placeholderImage: UIImage(named: ""))
                                    self.cat_Image_2_view_2.sd_setImage(with: URL(string: getImageFullURL("\(self.arrCategories[1].iconpath!)")), placeholderImage: UIImage(named: ""))
                                    
                                    self.btn_cat_1_View_2.layer.cornerRadius = self.btn_cat_1_View_2.frame.height / 2
                                    self.btn_cat_2_View_2.layer.cornerRadius = self.btn_cat_2_View_2.frame.height / 2
                                }
                                if first4.count == 3 {
                                    self.view3.isHidden = false
                                    self.cat_title_1_View_3.text = self.arrCategories[0].name
                                    self.cat_title_2_View_3.text = self.arrCategories[1].name
                                    self.cat_title_3_View_3.text = self.arrCategories[2].name
                                    
                                    self.cat_Image_1_view_3.sd_setImage(with: URL(string: getImageFullURL("\(self.arrCategories[0].iconpath!)")), placeholderImage: UIImage(named: ""))
                                    self.cat_Image_2_view_3.sd_setImage(with: URL(string: getImageFullURL("\(self.arrCategories[1].iconpath!)")), placeholderImage: UIImage(named: ""))
                                    self.cat_Image_3_view_3.sd_setImage(with: URL(string: getImageFullURL("\(self.arrCategories[2].iconpath!)")), placeholderImage: UIImage(named: ""))
                                    
                                    self.btn_cat_1_View_3.layer.cornerRadius = self.btn_cat_1_View_3.frame.height / 2
                                    self.btn_cat_2_View_3.layer.cornerRadius = self.btn_cat_2_View_3.frame.height / 2
                                    self.btn_cat_3_View_3.layer.cornerRadius = self.btn_cat_3_View_3.frame.height / 2
                                }
                                if first4.count == 4 {
                                    self.view4.isHidden = false
                                    self.cat_title_1_View_4.text = self.arrCategories[0].name!
                                    self.cat_title_2_View_4.text = self.arrCategories[1].name!
                                    self.cat_title_3_View_4.text = self.arrCategories[2].name!
                                    self.cat_title_4_View_4.text = self.arrCategories[3].name!
                                    
                                    
                                    self.cat_Image_1_view_4.sd_setImage(with: URL(string: getImageFullURL("\(self.arrCategories[0].iconpath!)")), placeholderImage: UIImage(named: ""))
                                    self.cat_Image_2_view_4.sd_setImage(with: URL(string: getImageFullURL("\(self.arrCategories[1].iconpath!)")), placeholderImage: UIImage(named: ""))
                                    self.cat_Image_3_view_4.sd_setImage(with: URL(string: getImageFullURL("\(self.arrCategories[2].iconpath!)")), placeholderImage: UIImage(named: ""))
                                    self.cat_Image_4_view_4.sd_setImage(with: URL(string: getImageFullURL("\(self.arrCategories[3].iconpath!)")), placeholderImage: UIImage(named: ""))
                                    
                                    self.btn_cat_1_View_4.layer.cornerRadius = self.btn_cat_1_View_4.frame.height / 2
                                    self.btn_cat_2_View_4.layer.cornerRadius = self.btn_cat_2_View_4.frame.height / 2
                                    self.btn_cat_3_View_4.layer.cornerRadius = self.btn_cat_3_View_4.frame.height / 2
                                    self.btn_cat_4_View_4.layer.cornerRadius = self.btn_cat_4_View_4.frame.height / 2
                                }
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                           let massage = msg["message"] as! String
                           showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
    }
  }
}
extension CategoriesVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        arrCategories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoriesCell", for: indexPath) as! CategoriesCell
        cell.configureCell(arrCategories[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
        let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
        let size:CGFloat = (CVCategories.frame.size.width - space) / 2.0
        return CGSize(width: size, height: 142)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "CategoriesDetailVC") as! CategoriesDetailVC
        vc.catid = arrCategories[indexPath.row].id
        vc.isHome = "yes"
        vc.isMenu = "Menu"
        navigationController?.pushViewController(vc, animated: true)
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView == CVCategories{
            if indexPath.item == self.arrCategories.count - 1 {
                if self.pageindex != self.lastindex{
                    self.pageindex += 1
                    if arrCategories.count != 0 {
                        let param: [String: Any] = ["theme_id": APP_THEMEID]
                        getCategoriesData(param)
                    }
                }
            }
        }
    }
}
