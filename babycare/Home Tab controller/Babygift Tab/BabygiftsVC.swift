//
//  BabygiftsVC.swift
//  kiddos
//
//  Created by mac on 15/04/22.
//

import UIKit
import Alamofire

class BabygiftsVC: UIViewController {
    
    //MARK: - Outlets
    
    @IBOutlet weak var CVToys: UICollectionView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_example: UILabel!

    //MARK: - Variables
    var arrProducts: [Products] = []
    var tag: String?
    var isfileterSelected: String?
    var maxPrice: String?
    var minPrice: String?
    var rateing: String?
    var pageindex = 1
    var lastindex = 0
    var productid = Int()
    var varientId = Int()
    
    //MARK: - ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        CVToys.register(UINib.init(nibName: "ToysCell", bundle: nil), forCellWithReuseIdentifier: "ToysCell")
        lbl_title.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    //MARK: - Actions
    @IBAction func onclickbackbtn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func onclickFilter(_ sender: UIButton) {
        let vc = FiltersVC(nibName: "FiltersVC", bundle: nil)
        vc.modalPresentationStyle = .overCurrentContext
        vc.delegate = self
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func onclicktextSearchTextField(_ sender: UITextField) {
        if self.searchTextField.text == ""{
            self.pageindex = 1
            self.lastindex = 0
            self.arrProducts.removeAll()
            self.CVToys.reloadData()
            self.lbl_title.isHidden = true
        } else {
            self.pageindex = 1
            self.lastindex = 0
            if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
                let parametersGuest: [String:Any] = ["type": "product_search",
                                                "name": searchTextField.text!,
                                                "theme_id": APP_THEMEID]
                getProductsGuestData(parametersGuest)
            }else{
                let parameters: [String:Any] = ["type": "product_search",
                                                "name": searchTextField.text!,
                                                "theme_id": APP_THEMEID]
                getProductsData(parameters)
            }
        }
    }
    
    //MARK: - CustomFunctions
    
    
    
    //MARK: - APIfunctions
    
    func getProductsGuestData(_ param: [String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_productsSearchGuest + "\(pageindex)", params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let prodata = json["data"] as? [String:Any]{
                                if self.pageindex == 1 {
                                    let lastpage = prodata["last_page"] as! Int
                                    self.lastindex = lastpage
                                    self.arrProducts.removeAll()
                                }
                                if let catData = prodata["data"] as? [[String:Any]]{
                                    self.arrProducts.append(contentsOf: KidsToys.init(catData).productdata)
                                    self.CVToys.reloadData()
                                }
                                self.lbl_title.isHidden = false
                                let totalProduct = prodata["total"] as? Int
                                self.lbl_title.text = "\(totalProduct ?? 0) result found"
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
                }
            case let .failure(error):
                printD("error:\(error.localizedDescription)")
                HIDE_CUSTOM_LOADER()
            }
        }
    }
    
    func getProductsData(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_ProductsSearch + "\(pageindex)", params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                if let prodata = json["data"] as? [String:Any]{
                                    if self.pageindex == 1 {
                                        let lastpage = prodata["last_page"] as! Int
                                        self.lastindex = lastpage
                                        self.arrProducts.removeAll()
                                    }
                                    if let catData = prodata["data"] as? [[String:Any]]{
                                        self.arrProducts.append(contentsOf: KidsToys.init(catData).productdata)
                                        self.CVToys.reloadData()
                                    }
                                    self.lbl_title.isHidden = false
                                    let totalProduct = prodata["total"] as? Int
                                    self.lbl_title.text = "\(totalProduct ?? 0) result found"
                                }
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let msg = json["data"] as! [String:Any]
                               let massage = msg["message"] as! String
                               showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    func addTocart(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
//            print(getID())
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_Addtocart, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                if let stats = json["data"] as? [String:Any]{
                                    if let stats = json["data"] as? [String:Any]{
                                        let massage = stats["message"] as? String
                                        let status = json["status"] as? Int
                                        let count = stats["count"] as? Int
                                        UserDefaults.standard.set(count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                                        if status == 1 {
                                            let alert = UIAlertController(title: nil, message: massage, preferredStyle: .actionSheet)
                                            let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
                                                self.dismiss(animated: true)
                                            }
                                            
                                            let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
                                                let vc = self.storyboard?.instantiateViewController(identifier: "ShoppingCartVC") as! ShoppingCartVC
                                                self.navigationController?.pushViewController(vc, animated: true)
                                            }
                                            
                                            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                                            alert.addAction(photoLibraryAction)
                                            alert.addAction(cameraAction)
                                            alert.addAction(cancelAction)
                                            self.present(alert, animated: true, completion: nil)
                                        }else if status == 0 {
                                            if massage == "Product has out of stock."{
                                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage!)
                                            }
                                        }
                                    }
                                }
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let alertVC = UIAlertController(title: Bundle.main.displayName!, message: "Product already in the cart! Do you want to add it again?", preferredStyle: .alert)
                                let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
                                    let param: [String:Any] = ["user_id": getID(),
                                                               "product_id": self.productid,
                                                               "variant_id": self.varientId,
                                                               "quantity_type": "increase",
                                                               "theme_id": APP_THEMEID]
                                    self.getQtyOfProduct(param)
                                }
                                let noAction = UIAlertAction(title: "No", style: .destructive)
                                alertVC.addAction(noAction)
                                alertVC.addAction(yesAction)
                                self.present(alertVC,animated: true,completion: nil)
                            }
                        }
                        print(json)
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    func getQtyOfProduct(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_cartQty, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                let count = json["count"] as? Int
                                UserDefaults.standard.set(count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let msg = json["data"] as! [String:Any]
                                let massage = msg["message"] as! String
                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
                    }
                    
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    func getWishList(_ param: [String:Any]){
        if UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_APP_TOKEN) != nil {
            if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
                let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                            "Accept": "application/json"]
                AIServiceManager.sharedManager.callPostApi(URL_Wishlist, params: param, headers) { response in
                    switch response.result{
                    case let .success(result):
                        HIDE_CUSTOM_LOADER()
                        if let json = result as? [String: Any]{
                            if let status = json["status"] as? Int{
                                if status == 1{
                                    if let data = json["data"] as? [String:Any]{
                                        let massage = data["message"] as? String
                                        UserDefaults.standard.set(massage, forKey: userDefaultsKeys.KEY_SAVED_CART)
                                        self.CVToys.reloadData()
                                    }
                                }else if status == 9 {
                                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                    let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                    nav.navigationBar.isHidden = true
                                    keyWindow?.rootViewController = nav
                                }else{
                                    let msg = json["data"] as! [String:Any]
                                   let massage = msg["message"] as! String
                                   showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                                }
                            }
                            print(json)
                        }
                    case let .failure(error):
                        print(error.localizedDescription)
                        HIDE_CUSTOM_LOADER()
                        break
                    }
                }
            }
        }
    }
}

extension BabygiftsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        collectionView == CVToys ? arrProducts.count : 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == CVToys{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ToysCell", for: indexPath) as! ToysCell
            cell.configreCell(arrProducts[indexPath.row])
            cell.onclickAddCartClosure = {
                if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == "" {
                    let data = self.arrProducts[indexPath.row]
                    
                    if UserDefaults.standard.value(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) != nil{
                        
                        var Gest_array = UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_GESTUSEROBJ) as! [[String:Any]]
                        var iscart = false
                        var cartindex = Int()
                        for i in 0..<Gest_array.count{
                            if Gest_array[i]["product_id"]! as! Int == data.id! && Gest_array[i]["variant_id"]! as! Int == data.defaultVariantID!{
                                iscart = true
                                cartindex = i
                            }
                        }
                        if iscart == false{
                            let cartobj = ["product_id": data.id!,
                                           "image": data.coverimagepath!,
                                           "name": data.name!,
                                           "orignal_price": data.orignalPrice!,
                                           "discount_price": data.discountPrice!,
                                           "final_price": data.finalPrice!,
                                           "qty": 1,
                                           "variant_id": data.defaultVariantID!,
                                           "variant_name": data.varientName!] as [String : Any]
                            Gest_array.append(cartobj)
                            UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_USERID)
                            UserDefaults.standard.set(Gest_array, forKey: userDefaultsKeys.KEY_GESTUSEROBJ)
                            
                            let alert = UIAlertController(title: nil, message: "\(data.name!) add successfully", preferredStyle: .actionSheet)
                            let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
                                self.dismiss(animated: true)
                            }
                            
                            let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
                                let vc = self.storyboard?.instantiateViewController(identifier: "ShoppingCartVC") as! ShoppingCartVC
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                            
                            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                            alert.addAction(photoLibraryAction)
                            alert.addAction(cameraAction)
                            alert.addAction(cancelAction)
                            self.present(alert, animated: true, completion: nil)
                        }else{
                            let alert = UIAlertController(title: Bundle.main.applicationName, message: "Product already in the cart! Do you want to add it again?", preferredStyle: .alert)
                            let yesaction = UIAlertAction(title: "Yes", style: .default) { (action) in
                                var cartsList = Gest_array[cartindex]
                                cartsList["qty"] = cartsList["qty"] as! Int + 1
                                Gest_array.remove(at: cartindex)
                                Gest_array.insert(cartsList, at: cartindex)
                                
                                UserDefaults.standard.set(Gest_array, forKey: userDefaultsKeys.KEY_GESTUSEROBJ)
                                UserDefaults.standard.set("\(Gest_array.count)", forKey: userDefaultsKeys.KEY_SAVED_CART)
                            }
                            let noaction = UIAlertAction(title: "No", style: .destructive)
                            alert.addAction(yesaction)
                            alert.addAction(noaction)
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                }else{
                    let param: [String:Any] = ["user_id": getID(),
                                               "product_id": self.arrProducts[indexPath.row].id!,
                                               "variant_id": self.arrProducts[indexPath.row].defaultVariantID!,
                                               "qty": 1,
                                               "theme_id": APP_THEMEID]
                    self.addTocart(param)
                }
            }
            if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
                cell.wishlistView.isHidden = true
                cell.btn_fev.isHidden = true
            }else{
                cell.wishlistView.isHidden = false
                cell.btn_fev.isHidden = false
            }
            if arrProducts[indexPath.row].isinwishlist == false{
                cell.btn_fev.setImage(UIImage(named: "ic_heart"), for: .normal)
            }else{
                cell.btn_fev.setImage(UIImage(named: "ic_heartfil"), for: .normal)
            }
            cell.btn_fev.tag = indexPath.row
            cell.btn_fev.addTarget(self, action: #selector(onclickFevBtn), for: .touchUpInside)
            return cell
        }else{
            return UICollectionViewCell()
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        collectionView == CVToys ? CGSize(width: collectionView.frame.width / 2, height: 310) : CGSize.zero
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == CVToys{
            let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "ToyDesctiptionVC") as! ToyDesctiptionVC
            vc.idpassed = arrProducts[indexPath.row].id
            vc.varienrid = arrProducts[indexPath.row].defaultVariantID
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView == CVToys{
            if self.isfileterSelected == "1"{
                if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == "" {
                    if indexPath.item == self.arrProducts.count - 1 {
                        if self.pageindex != self.lastindex{
                            self.pageindex += 1
                            if arrProducts.count != 0 {
                                let parameters: [String:Any] = ["type": "product_filter",
                                                                "tag": tag!,
                                                                "min_price": minPrice!,
                                                                "max_price": maxPrice!,
                                                                "rating": rateing!,
                                                                "theme_id": APP_THEMEID]
                                getProductsGuestData(parameters)
                            }
                        }
                    }
                }else{
                    if indexPath.item == self.arrProducts.count - 1 {
                        if self.pageindex != self.lastindex{
                            self.pageindex += 1
                            if arrProducts.count != 0 {
                                let parameters: [String:Any] = ["type": "product_filter",
                                                                "tag": tag!,
                                                                "min_price": minPrice!,
                                                                "max_price": maxPrice!,
                                                                "rating": rateing!,
                                                                "theme_id": APP_THEMEID]
                                getProductsData(parameters)
                            }
                        }
                    }
                }
            }
            else{
                if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == "" {
                    if indexPath.item == self.arrProducts.count - 1 {
                        if self.pageindex != self.lastindex{
                            self.pageindex += 1
                            if arrProducts.count != 0 {
                                let parameters: [String:Any] = ["type": "product_search",
                                                                "name": searchTextField.text!,
                                                                "theme_id": APP_THEMEID]
                                getProductsGuestData(parameters)
                            }
                        }
                    }
                }else{
                    if indexPath.item == self.arrProducts.count - 1 {
                        if self.pageindex != self.lastindex{
                            self.pageindex += 1
                            if arrProducts.count != 0 {
                                let parameters: [String:Any] = ["type": "product_search",
                                                                "name": searchTextField.text!,
                                                                "theme_id": APP_THEMEID]
                                getProductsData(parameters)
                            }
                        }
                    }
                }
            }
        }
    }
    @objc func onclickFevBtn(sender:UIButton) {
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            keyWindow?.rootViewController = nav
        }else{
            let data = self.arrProducts[sender.tag]
            if data.isinwishlist == false{
                let param: [String:Any] = ["user_id": getID(),
                                           "product_id": data.id!,
                                            "wishlist_type": "add",
                                           "theme_id": APP_THEMEID]
                data.isinwishlist = true
                self.getWishList(param)
            }else if data.isinwishlist == true{
                let param: [String:Any] = ["user_id": getID(),
                                           "product_id": data.id!,
                                            "wishlist_type": "remove",
                                           "theme_id": APP_THEMEID]
                data.isinwishlist = false
                self.getWishList(param)
            }
        }
    }
}
extension BabygiftsVC: FilterDelegate{
    func filterData(tag: String, min_price: String, max_price: String, rating: String, isfilter: String) {
        self.isfileterSelected = isfilter
        self.tag = tag
        self.minPrice = min_price
        self.maxPrice = max_price
        self.rateing = rating
        
        self.pageindex = 1
        self.lastindex = 0
        
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == "" {
            let parameters: [String:Any] = ["type": "product_filter",
                                            "tag": tag,
                                            "min_price": minPrice!,
                                            "max_price": maxPrice!,
                                            "rating": rateing!,
                                            "theme_id": APP_THEMEID]
            getProductsGuestData(parameters)
        }
        else{
            let parameters: [String:Any] = ["type": "product_filter",
                                            "tag": tag,
                                            "min_price": minPrice!,
                                            "max_price": maxPrice!,
                                            "rating": rateing!,
                                            "theme_id": APP_THEMEID]
            getProductsData(parameters)
        }
    }
}
