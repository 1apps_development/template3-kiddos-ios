//
//  TrackOrderVCViewController.swift
//  kiddos
//
//  Created by mac on 01/11/22.
//

import UIKit

class TrackOrderVC: UIViewController {
    
    @IBOutlet weak var img_circle_1: UIImageView!
    @IBOutlet weak var lbl_title_1: UILabel!
    @IBOutlet weak var lbl_line_1: UILabel!
    @IBOutlet weak var img_circle_2: UIImageView!
    @IBOutlet weak var lbl_title_2: UILabel!
    @IBOutlet weak var lbl_line_2: UILabel!
    
    var Status = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        if self.Status == "Pending"
        {
            self.img_circle_1.image = UIImage(systemName: "circle.fill")
            self.img_circle_1.tintColor = hexStringToUIColor(hex: "#3E4F46")
            self.lbl_line_1.backgroundColor = hexStringToUIColor(hex: "#3E4F46")
            
            self.img_circle_2.image = UIImage(systemName: "circle")
            self.img_circle_2.tintColor = .white
            self.lbl_line_2.backgroundColor = .white
        }
        else if self.Status == "Delivered" {
            
            self.img_circle_1.image = UIImage(systemName: "circle.fill")
            self.img_circle_1.tintColor = hexStringToUIColor(hex: "#3E4F46")
            self.lbl_line_1.backgroundColor = hexStringToUIColor(hex: "#3E4F46")
            
            self.img_circle_2.image = UIImage(systemName: "circle.fill")
            self.img_circle_2.tintColor = hexStringToUIColor(hex: "#3E4F46")
            self.lbl_line_2.backgroundColor = hexStringToUIColor(hex: "#3E4F46")
        }else{
            print("cancel or return")
        }
    }
    
    
    @IBAction func onClickBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
