//
//  OrderDetailVC.swift
//  kiddos
//
//  Created by mac on 20/04/22.
//

import UIKit
import Alamofire
import SDWebImage

class OrderDetailVC: UIViewController {
    
    @IBOutlet weak var tableOrderDetail: UITableView!
    @IBOutlet weak var heightOrderDetailTableView: NSLayoutConstraint!
    @IBOutlet weak var taxtCollection: UICollectionView!
    @IBOutlet weak var lbl_orderid: UILabel!
    @IBOutlet weak var lbl_billinginfo: UILabel!
    @IBOutlet weak var lbl_delivaryInfo: UILabel!
    @IBOutlet weak var cupenView: UIView!
    
    @IBOutlet weak var lbl_subTotalTop: NSLayoutConstraint!
    @IBOutlet weak var img_delivary: UIImageView!
    @IBOutlet weak var img_payment: UIImageView!
    
    @IBOutlet weak var cupenCodeTitle: UILabel!
    @IBOutlet weak var lbl_subtotal: UILabel!
    @IBOutlet weak var lbl_finaltotal: UILabel!
    @IBOutlet weak var lbl_cupencode: UILabel!
    @IBOutlet weak var lbl_cupenAmount: UILabel!
    @IBOutlet weak var btn_Cancel: UIButton!
    @IBOutlet weak var lbl_disString2: UILabel!
    
    @IBOutlet weak var lbl_massage: UILabel!
    
    @IBOutlet weak var Viewcuppen: UIView!
    @IBOutlet weak var heightCupenView: NSLayoutConstraint!
    
    @IBOutlet weak var viewEmpty: UIView!
    @IBOutlet weak var lblnodataFound: UILabel!
    
    @IBOutlet weak var line1: UIView!
    @IBOutlet weak var btn_TrackOrder: UIButton!
    
    var cartListArray: [CartsList] = []
    var textinfoArray: [TextdataList] = []
    var orderStaus = Int()
    var orderStausText = String()
    var order_id = Int()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableOrderDetail.register(UINib(nibName: "CartCell", bundle: nil), forCellReuseIdentifier: "CartCell")
        taxtCollection.register(UINib(nibName: "TextDetailsCell", bundle: nil), forCellWithReuseIdentifier: "TextDetailsCell")
        tableOrderDetail.tableFooterView = UIView()
        btn_Cancel.isHidden = false
        btn_TrackOrder.isHidden = false
        viewEmpty.isHidden = true
        lblnodataFound.isHidden = true
        cupenView.isHidden = true
        cupenCodeTitle.isHidden = true
        lbl_subTotalTop.constant = 8
        heightCupenView.constant = 115
        line1.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let param: [String:Any] = ["order_id": order_id,
                                   "theme_id": APP_THEMEID]
        getOrderDetails(param)
    }
    
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func cancelOrder(_ sender: UIButton) {
        if btn_Cancel.titleLabel?.text == "Return Order"{
            let param: [String:Any] = ["order_id": order_id,
                                       "order_status": "return",
                                       "theme_id": APP_THEMEID]
            getOrderStatusChange(param)
        }else if btn_Cancel.titleLabel?.text == "Cancel Order"{
            let param: [String:Any] = ["order_id": order_id,
                                       "order_status": "cancel",
                                       "theme_id": APP_THEMEID]
            getOrderStatusChange(param)
        }
    }
    
    @IBAction func onclickTrackOrder(_ sender: UIButton) {
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "TrackOrderVC") as! TrackOrderVC
        vc.Status = self.orderStausText
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func getOrderDetails(_ param: [String:Any]) {
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
        AIServiceManager.sharedManager.callPostApi(URL_OrderDetails, params: param, headers) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [String:Any]{
                               let orderid = data["order_id"] as? String
                               self.lbl_orderid.text = "Order \(orderid!)"
                               let order = data["order_status_text"] as? String
                               self.orderStausText = order!
                               let orderstatus = data["order_status"] as? Int
                               self.orderStaus = orderstatus!
                               if self.orderStaus == 0{
                                   self.btn_Cancel.isHidden = false
                                   self.lbl_massage.isHidden = true
                                   self.btn_Cancel.setTitle("Cancel Order", for: .normal)
                                   self.btn_TrackOrder.isHidden = false
                               }else if self.orderStaus == 1{
                                   self.btn_Cancel.isHidden = false
                                   self.lbl_massage.isHidden = true
                                   self.btn_Cancel.setTitle("Return Order", for: .normal)
                                   self.btn_TrackOrder.isHidden = false
                               }else if self.orderStaus == 2 {
                                   self.btn_Cancel.isHidden = true
                                   self.lbl_massage.isHidden = false
                                   self.lbl_massage.text = data["order_status_message"] as? String
                                   self.btn_TrackOrder.isHidden = true
                               }else if self.orderStaus == 3 {
                                   self.btn_Cancel.isHidden = true
                                   self.lbl_massage.isHidden = false
                                   self.lbl_massage.text = data["order_status_message"] as? String
                                   self.btn_TrackOrder.isHidden = true
                               }
                               if let text = data["tax"] as? [[String:Any]]{
                                   self.textinfoArray = TextData.init(text).textdatalists
                                   self.taxtCollection.reloadData()
                               }
                               if let product = data["product"] as? [[String:Any]]{
                                   self.cartListArray = ProductsList.init(product).cartsList
                                   self.tableOrderDetail.reloadData()
                                   self.heightOrderDetailTableView.constant = CGFloat(self.cartListArray.count * 89)
                               }
                               if let cupenifo = data["coupon_info"] as? [String:Any] {
                                   self.cupenView.isHidden = false
                                   self.cupenCodeTitle.isHidden = false
                                   self.line1.isHidden = false
                                   self.lbl_subTotalTop.constant = 98
                                   self.heightCupenView.constant = 203
                                   let cupencode = cupenifo["code"] as? String
                                   let disString = cupenifo["discount_string"] as? String
                                   let disString2 = cupenifo["discount_string2"] as? String
                                   self.lbl_cupencode.text = cupencode
                                   self.lbl_cupenAmount.text = disString
                                   self.lbl_disString2.text = disString2
                               }
                               if let billinginfo = data["billing_informations"] as? [String:Any] {
                                   let name  = billinginfo["name"] as? String
                                   let address = billinginfo["address"] as? String
                                   let email = billinginfo["email"] as? String
                                   let phone = billinginfo["phone"] as? String
                                   let country = billinginfo["country"] as? String
                                   let state = billinginfo["state"] as? String
                                   let city = billinginfo["city"] as? String
                                   let postcode = billinginfo["post_code"] as? String
                                   
                                   self.lbl_billinginfo.text = "\(name!) \n \(address!) \(city!) \(state!) \(country!) - \(postcode!) \n Phone: \(phone!) \n Email: \(email!)"
                               }
                               if let delivaryInfo = data["delivery_informations"] as? [String:Any] {
                                   let name  = delivaryInfo["name"] as! String
                                   let email = delivaryInfo["email"] as! String
                                   let phone = delivaryInfo["phone"] as! String
                                   let country = delivaryInfo["country"] as! String
                                   let state = delivaryInfo["state"] as! String
                                   let city = delivaryInfo["city"] as! String
                                   let postcode = delivaryInfo["post_code"] as! String
                                   let address = delivaryInfo["address"] as! String
                                   if address == "" {
                                       self.lbl_delivaryInfo.text = "-"
                                   }else{
                                       self.lbl_delivaryInfo.text = "\(name) \n \(address) \(city) \(state) \(country) - \(postcode) \n Phone: \(phone) \n Email: \(email)"
                                   }
                               }
                               let paymentimage = data["paymnet"] as? String
                               self.img_payment.sd_setImage(with: URL(string: "\(URL_PaymentImage)/\(paymentimage!)"), placeholderImage: UIImage(named: ""))
                               let delivaryImage = data["delivery"] as? String
                               self.img_delivary.sd_setImage(with: URL(string: "\(URL_BASEIMAGE)\(delivaryImage!)"), placeholderImage: UIImage(named: ""))
                               let subtotal = data["sub_total"] as? String
                               self.lbl_subtotal.text = "\(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY)!) \(subtotal!)"
                               let finalTotal = data["final_price"] as? String
                               self.lbl_finaltotal.text = "\(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY)!) \(finalTotal!)"
                           }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                            let massage = msg["message"] as! String
                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
//                    print(json)
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
      }
    }
    
    func getOrderStatusChange(_ param: [String:Any]) {
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
        AIServiceManager.sharedManager.callPostApi(URL_StatusChange, params: param, headers) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String: Any]{
                    if let status = json["status"] as? Int{
                        if status == 1{
                            if let data = json["data"] as? [String:Any]{
                                let massage = data["message"] as? String
                                let param: [String:Any] = ["order_id": self.order_id,
                                                           "theme_id": APP_THEMEID]
                                self.getOrderDetails(param)
                                print(massage!)
                            }
                        }else if status == 9 {
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                            nav.navigationBar.isHidden = true
                            keyWindow?.rootViewController = nav
                        }else{
                            let msg = json["data"] as! [String:Any]
                           let massage = msg["message"] as! String
                           showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                        }
                    }
//                    print(json)
                }
            case let .failure(error):
                print(error.localizedDescription)
                HIDE_CUSTOM_LOADER()
                break
            }
        }
      }
    }
    
    func addrateing(_ param: [String:Any]) {
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
//            print(getID())
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_ProductRateing, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                print(json)
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let msg = json["data"] as! [String:Any]
                               let massage = msg["message"] as! String
                               showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
                       
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    
}
extension OrderDetailVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartListArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CartCell", for: indexPath) as! CartCell
            cell.lbl_productname.text = cartListArray[indexPath.row].name
            let productimgURL = getImageFullURL("\(cartListArray[indexPath.row].image!)")
            cell.img_product.sd_setImage(with: URL(string: productimgURL)) { image, error, type, url in
                cell.img_product.image = image
            }
            let price = Double(cartListArray[indexPath.row].finalprice!)! * Double(cartListArray[indexPath.row].qty!)
            cell.lbl_price.text = cartListArray[indexPath.row].varientName!
            cell.lbl_finalPrice.text = "\(price)"
            cell.lbl_value.text = "\(cartListArray[indexPath.row].qty!)"
            cell.lbl_currency.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCYNAME)
            return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
extension OrderDetailVC: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return textinfoArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.taxtCollection.dequeueReusableCell(withReuseIdentifier: "TextDetailsCell", for: indexPath) as! TextDetailsCell
        cell.lbl_price.text = "\(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY)!)\(textinfoArray[indexPath.row].taxAmoountString!)"
        cell.lbl_textTitle.text = textinfoArray[indexPath.row].taxString
        return cell
    }
}
extension OrderDetailVC : FeedbackDelegate{
    func refreshData(id: Int, rating_no: String, title: String, description: String) {
        let param: [String:Any] = ["id": id,
                                   "user_id": getID(),
                                   "rating_no": rating_no,
                                   "title": title,
                                   "description": description,
                                   "theme_id": APP_THEMEID]
        self.addrateing(param)
    }
    
}
