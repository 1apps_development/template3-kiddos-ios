//
//  MainTabbarVC.swift
//  Ticket-GO
//
//  Created by mac on 23/06/22.
//

import UIKit

class MainTabbarVC: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let selFont = UIFont.init(name: "Outfit-Semibold", size: 10)
        let normalFont = UIFont.init(name: "Outfit-Medium", size: 10)

       
        let appearance = UITabBarItem.appearance()
        let attributes = [NSAttributedString.Key.font:normalFont, .foregroundColor: UIColor.black]
        
        let AttriSelected = [NSAttributedString.Key.font:selFont, .foregroundColor: UIColor.white]
        appearance.setTitleTextAttributes(attributes as [NSAttributedString.Key : Any], for: .normal)
        appearance.setTitleTextAttributes(AttriSelected as [NSAttributedString.Key : Any], for: .selected)
        
        
//        tabBar.backgroundColor = UIColor.white

        // Removing the upper border of the UITabBar.
        //
        // Note: Don't use `tabBar.clipsToBounds = true` if you want
        // to add a custom shadow to the `tabBar`!
        //
        if #available(iOS 13, *) {
            // iOS 13:
            let appearance = tabBar.standardAppearance
            appearance.configureWithOpaqueBackground()
            appearance.shadowImage = nil
            appearance.shadowColor = nil
            tabBar.standardAppearance = appearance
        } else {
            // iOS 12 and below:
            tabBar.shadowImage = UIImage()
            tabBar.backgroundImage = UIImage()
        }
        
        //store every image in a variable
        let tab1NormalImage: UIImage = UIImage(named: "tab_bestseller")!.withRenderingMode(.alwaysTemplate)
        let tab1SelectedImage: UIImage = UIImage(named: "tab_bestseller")!.withRenderingMode(.alwaysOriginal)
        
        let tab2NormalImage: UIImage = UIImage(named: "tab_gift")!.withRenderingMode(.alwaysTemplate)
        let tab2SelectedImage: UIImage = UIImage(named: "tab_gift")!.withRenderingMode(.alwaysOriginal)
        
        let tab3NormalImage: UIImage = UIImage(named: "tab_categories_selected")!.withRenderingMode(.alwaysTemplate)
        let tab3SelectedImage: UIImage = UIImage(named: "tab_categories_selected")!.withRenderingMode(.alwaysTemplate)
        
        let tab4NormalImage: UIImage = UIImage(named: "tab_promo")!.withRenderingMode(.alwaysTemplate)
        let tab4SelectedImage: UIImage = UIImage(named: "tab_promo")!.withRenderingMode(.alwaysOriginal)
        
        let tab5NormalImage: UIImage = UIImage(named: "tab_special")!.withRenderingMode(.alwaysTemplate)
        let tab5SelectedImage: UIImage = UIImage(named: "tab_special")!.withRenderingMode(.alwaysOriginal)


        
        //use that variable for each icon
        tabBar.items![0].image = tab1NormalImage
        tabBar.items![0].selectedImage = tab1SelectedImage
        
        tabBar.items![1].image = tab2NormalImage
        tabBar.items![1].selectedImage = tab2SelectedImage
        
        tabBar.items![2].image = tab3NormalImage
        tabBar.items![2].selectedImage = tab3SelectedImage
        
        tabBar.items![3].image = tab4NormalImage
        tabBar.items![3].selectedImage = tab4SelectedImage
        
        tabBar.items![4].image = tab5NormalImage
        tabBar.items![4].selectedImage = tab5SelectedImage




        // Do any additional setup after loading the view.
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if item == (self.tabBar.items!)[2]{
            NotificationCenter.default.post(name: Notification.Name("NOTIFICATION_CENTER_TAB"), object: nil)
        }
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
