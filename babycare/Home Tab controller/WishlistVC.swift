//
//  WishlistVC.swift
//  kiddos
//
//  Created by mac on 20/04/22.
//

import UIKit
import Alamofire

class WishlistVC: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var wishlistTableheight: NSLayoutConstraint!
    @IBOutlet weak var tableWish: UITableView!
    @IBOutlet weak var lbl_nodataFound: UILabel!
    @IBOutlet weak var viewEmpty: UIView!
    @IBOutlet weak var lbl_count: UILabel!
    @IBOutlet weak var Scroll_View: UIScrollView!
    
    var pageindex = 1
    var lastindex = 0
    var product_id = Int()
    var varient_id = Int()
    var arrWishlist: [WishlistData] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.Scroll_View.delegate = self
        viewEmpty.isHidden = true
        lbl_nodataFound.isHidden = true
        tableWish.register(UINib.init(nibName: "WishlistCell", bundle: nil), forCellReuseIdentifier: "WishlistCell")
        tableWish.tableFooterView = UIView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lbl_count.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
        if UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_USERID) == ""{
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            let nav : UINavigationController = UINavigationController(rootViewController: objVC)
            nav.navigationBar.isHidden = true
            keyWindow?.rootViewController = nav
        }
        else {
            pageindex = 1
            lastindex = 0
            let param: [String:Any] = ["user_id": getID(),
                                       "theme_id": APP_THEMEID]
            getwihslistData(param)
        }
    }
    
    // MARK: - scroll view didScroll
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (Int(self.Scroll_View.contentOffset.y) >=  Int(self.Scroll_View.contentSize.height - self.Scroll_View.frame.size.height)) {
            if self.pageindex != self.lastindex {
                self.pageindex = self.pageindex + 1
                if self.arrWishlist.count != 0 {
                    let param: [String:Any] = ["user_id": getID(),
                                               "theme_id": APP_THEMEID]
                    getwihslistData(param)
                }
            }
        }
    }
    
    @IBAction func onclickCart(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(identifier: "ShoppingCartVC") as! ShoppingCartVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    func addTocart(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            //            print(getID())
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_Addtocart, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                if let stats = json["data"] as? [String:Any]{
                                    let massage = stats["message"] as? String
                                    let status = json["status"] as? Int
                                    let count = stats["count"] as? Int
                                    UserDefaults.standard.set(count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                                    if status == 1 {
                                        let alert = UIAlertController(title: nil, message: massage, preferredStyle: .actionSheet)
                                        let photoLibraryAction = UIAlertAction(title: "Continue shopping", style: .default) { (action) in
                                            self.dismiss(animated: true)
                                        }
                                        
                                        let cameraAction = UIAlertAction(title: "Proceed to check out", style: .default) { (action) in
                                            let vc = self.storyboard?.instantiateViewController(identifier: "ShoppingCartVC") as! ShoppingCartVC
                                            self.navigationController?.pushViewController(vc, animated: true)
                                        }
                                        
                                        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                                        alert.addAction(photoLibraryAction)
                                        alert.addAction(cameraAction)
                                        alert.addAction(cancelAction)
                                        self.present(alert, animated: true, completion: nil)
                                    }else if status == 0 {
                                        if massage == "Product has out of stock."{
                                            showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage!)
                                        }
                                    }
                                    self.lbl_count.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_SAVED_CART)
                                }
                            }else if status == 9 {
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let alertVC = UIAlertController(title: Bundle.main.displayName!, message: "Product already in the cart! Do you want to add it again?", preferredStyle: .alert)
                                let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
                                    let param: [String:Any] = ["user_id": getID(),
                                                               "product_id": self.product_id,
                                                               "variant_id": self.varient_id,
                                                               "quantity_type": "increase",
                                                               "theme_id": APP_THEMEID]
                                    self.getQtyOfProduct(param)
                                }
                                let noAction = UIAlertAction(title: "No", style: .destructive)
                                alertVC.addAction(noAction)
                                alertVC.addAction(yesAction)
                                self.present(alertVC,animated: true,completion: nil)
                            }
                        }
                        
                        print(json)
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    
    func getQtyOfProduct(_ param: [String:Any]){
        if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                        "Accept": "application/json"]
            AIServiceManager.sharedManager.callPostApi(URL_cartQty, params: param, headers) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        if let status = json["status"] as? Int{
                            if status == 1{
                                let count = json["count"] as? Int
                                UserDefaults.standard.set(count, forKey: userDefaultsKeys.KEY_SAVED_CART)
                            }else if status == 9{
                                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                nav.navigationBar.isHidden = true
                                keyWindow?.rootViewController = nav
                            }else{
                                let msg = json["data"] as! [String:Any]
                                let massage = msg["message"] as! String
                                showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                            }
                        }
                    }
                    
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
    
    func getwihslistData(_ param: [String:Any]){
        if UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_APP_TOKEN) != nil {
            if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
                let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                            "Accept": "application/json"]
                AIServiceManager.sharedManager.callPostApi(URL_Wishlistes + "\(pageindex)", params: param, headers) { response in
                    switch response.result{
                    case let .success(result):
                        HIDE_CUSTOM_LOADER()
                        if let json = result as? [String: Any]{
                            if let status = json["status"] as? Int{
                                if status == 1{
                                    if let data = json["data"] as? [String:Any]{
                                        if self.pageindex == 1 {
                                            let lastpage = data["last_page"] as! Int
                                            self.lastindex = lastpage
                                            self.arrWishlist.removeAll()
                                        }
                                        if let orderdata = data["data"] as? [[String:Any]]{
                                            self.arrWishlist.append(contentsOf: WishList.init(orderdata).wishlists)
                                            self.tableWish.reloadData()
                                            self.tableWish.delegate = self
                                            self.tableWish.dataSource = self
                                            self.wishlistTableheight.constant = CGFloat(80 * self.arrWishlist.count)
                                        }
                                    }
                                }
                                else if status == 9 {
                                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                    let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                    nav.navigationBar.isHidden = true
                                    keyWindow?.rootViewController = nav
                                }
                                else {
                                    let msg = json["data"] as! [String:Any]
                                    let massage = msg["message"] as! String
                                    showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                                }
                            }
                            print(json)
                        }
                    case let .failure(error):
                        print(error.localizedDescription)
                        HIDE_CUSTOM_LOADER()
                        break
                    }
                }
            }
        }
    }
    func getWishList(_ param: [String:Any]){
        if UserDefaults.standard.object(forKey: userDefaultsKeys.KEY_APP_TOKEN) != nil {
            if let token = readValueFromUserDefaults(userDefaultsKeys.KEY_APP_TOKEN) as? String{
                let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",
                                            "Accept": "application/json"]
                AIServiceManager.sharedManager.callPostApi(URL_Wishlist, params: param, headers) { response in
                    switch response.result{
                    case let .success(result):
                        HIDE_CUSTOM_LOADER()
                        if let json = result as? [String: Any]{
                            if let status = json["status"] as? Int{
                                if status == 1{
                                    if let data = json["data"] as? [String:Any]{
                                        let massage = data["message"] as? String
                                        UserDefaults.standard.set(massage, forKey: userDefaultsKeys.KEY_SAVED_CART)
                                        let param: [String:Any] = ["user_id": getID(),
                                                                   "theme_id": APP_THEMEID]
                                        self.getwihslistData(param)
                                    }
                                }else if status == 9 {
                                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                                    let objVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                                    let nav : UINavigationController = UINavigationController(rootViewController: objVC)
                                    nav.navigationBar.isHidden = true
                                    keyWindow?.rootViewController = nav
                                }else{
                                    let msg = json["data"] as! [String:Any]
                                    let massage = msg["message"] as! String
                                    showAlertMessage(titleStr: Bundle.main.displayName!, messageStr: massage)
                                }
                            }
                            print(json)
                        }
                    case let .failure(error):
                        print(error.localizedDescription)
                        HIDE_CUSTOM_LOADER()
                        break
                    }
                }
            }
        }
    }
}

extension WishlistVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrWishlist.count == 0 {
            self.viewEmpty.isHidden = false
            self.lbl_nodataFound.isHidden = false
        }else{
            self.viewEmpty.isHidden = true
            self.lbl_nodataFound.isHidden = true
            return arrWishlist.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WishlistCell") as! WishlistCell
        cell.configCell(arrWishlist[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "ToyDesctiptionVC") as! ToyDesctiptionVC
        vc.idpassed = arrWishlist[indexPath.row].productid
        vc.varienrid = arrWishlist[indexPath.row].varientid
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let cartaction = UIContextualAction(style: .normal, title: nil) { (_, _, success: (Bool) -> Void) in
            success(true)
            let data = self.arrWishlist[indexPath.row]
            self.product_id = data.productid!
            self.varient_id = data.varientid!
            
            let param: [String:Any] = ["user_id": getID(),
                                       "product_id": data.productid!,
                                       "variant_id": data.varientid!,
                                       "qty": 1,
                                       "theme_id": APP_THEMEID]
            self.addTocart(param)
        }
        cartaction.image = UISwipeActionsConfiguration.makeTitledImage(
            image: UIImage(named: "ic_cartwish"),
            title: "", textColor: UIColor(red: 183/255, green: 212/255, blue: 197/255))
        cartaction.backgroundColor = UIColor.white
        
        return UISwipeActionsConfiguration(actions: [cartaction])
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteaction = UIContextualAction(style: .normal, title: nil) { (_, _, success: (Bool) -> Void) in
            success(true)
            let data = self.arrWishlist[indexPath.row]
            self.product_id = data.productid!
            self.varient_id = data.varientid!
            
            let param: [String:Any] = ["user_id": getID(),
                                       "product_id": data.productid!,
                                       "wishlist_type": "remove",
                                       "theme_id": APP_THEMEID]
            self.getWishList(param)
        }
        deleteaction.image = UISwipeActionsConfiguration.makeTitledImage(
            image: UIImage(named: "ic_trash"),
            title: "", textColor: UIColor(red: 183/255, green: 212/255, blue: 197/255))
        deleteaction.backgroundColor = hexStringToUIColor(hex: "#FBD9B3")
        
        return UISwipeActionsConfiguration(actions: [deleteaction])
    }
}
