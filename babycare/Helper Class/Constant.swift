//
//  Constant.swift
//  EsportGamingLogo
//
//  Created by Vrushik on 29/04/21.
//

import Foundation
import UIKit

var isAds = false
var isfbAds = false
var alternativeFB = true
var showAds : Bool = true

///test
var nativeAdsKey = "ca-app-pub-3940256099942544/3986624511"
var bannerKey = "ca-app-pub-3940256099942544/2934735716"
var interstatllKey = "ca-app-pub-3940256099942544/4411468910"
var rewardAdsKey = "ca-app-pub-3940256099942544/1712485313"

///Live
//var nativeAdsKey = "ca-app-pub-8076463371698113/5373301282"
//var bannerKey = "ca-app-pub-8076463371698113/7508510038"
//var interstatllKey = "ca-app-pub-8076463371698113/4990157900"
//var rewardAdsKey = ""


//test
var nativeAdsKeyFB = "VID_HD_9_16_39S_LINK#YOUR_PLACEMENT_ID"
var bannerKeyFB = "IMG_16_9_APP_INSTALL#YOUR_PLACEMENT_ID"
var interstatllKeyFB = "IMG_16_9_LINK#YOUR_PLACEMENT_ID"
var rewardAdsKeyFB = "VID_HD_9_16_39S_LINK#YOUR_PLACEMENT_ID"

///Live
//var nativeAdsKeyFB = "897242054176448_897252197508767"
//var bannerKeyFB = "897242054176448_897242297509757"
//var interstatllKeyFB = "897242054176448_897253010842019"
//var rewardAdsKeyFB = "VID_HD_9_16_39S_LINK#YOUR_PLACEMENT_ID"

/** App Setting  **/
let appID = ""
let App_Link = "https://apps.apple.com/us/app/id\(appID)"
let App_Link_rate = "https://apps.apple.com/us/app/id\(appID)?action=write-review"
let App_Link_Appstore = "https://apps.apple.com/sl/developer/"
var privacyPolicy = "https://sites.google.com/view//home"
var termcondition = "https://sites.google.com/view//home"

let image_Base_Url = "https://logomaker.cloudustechnologies.com/"
let Base_Url = "https://logomaker.cloudustechnologies.com"
let BackGroundApi = Base_Url + "/API/V1/background"
let StickerApi = Base_Url + "/API/V1/StikerData"
let ReadyMadeApi = Base_Url + "/API/V1/poster/swiperCat"
let poster = Base_Url + "/API/V1/poster"
let fontsPath = Base_Url + "/upload/font/"

var AppName = "Gaming LogoMaker"

var authkey = "gfaZjrzze5kQiyuSh9f1u7yEUeA="
var hueOpacitySlider : CGFloat  = 0.4
var hueOpacityColor : UIColor  = UIColor.clear
// text move in ichae perclick
let movePostion : CGFloat = 1.5
 //ListFOnt Glonal
var font_List : [String] = []


//template PrimamCOunt
var primimCount = 9
var primimCountIpad = 5
var primimStickerCount = 10
var ipadtemapleteHeight : CGFloat = 350
var iphonetemapleteHeight : CGFloat = 160
var ipadAdsHeight : CGFloat = 500
var iphoneAdsHeight  : CGFloat = 300
var adsBetween = 8
var adsBetweenBackground = 9
var adsStickerBetween = 9
var adsIpadBetween = 12
var selectedRation = "1:1"

let fontList = ["none","1.ttf",
                "2.otf",
                "3.ttf",
                "4.ttf",
                "5.otf",
                "6.TTF",
                "7.otf",
                "8.otf",
                "9.TTF",
                "10.TTF",
                "11.ttf",
                "12.ttf",
                "13.ttf",
                "14.ttf",
                "15.TTF",
                "16.TTF",
                "17.ttf",
                "18.OTF",
                "19.TTF",
                "20.otf",
                "Acme-Regular.ttf",
                "Aleo-Bold.ttf",
                "Aleo-BoldItalic.ttf",
                "Aleo-Italic.ttf",
                "Aleo-Light.otf",
                "Aleo-Light.ttf",
                "Aleo-LightItalic.ttf",
                "Aleo-Regular.otf",
                "Aleo-Regular.ttf",
                "ALICE-REGULAR.TTF",
                "ALLER_LT.TTF",
                "aller.bold.ttf",
                "ALLER.TTF",
                "ALLERDISPLAY.TTF",
                "amicale.otf",
                "aparaj.ttf",
                "aparajb_0.ttf",
                "arabolical.TTF",
                "Arial regular.ttf",
                "ARIAL.TTF",
                "BAUHS93.TTF",
                "BEBAS___.ttf",
                "Bebas-Regular.otf",
                "Bebas-Regular.ttf",
                "BEBAS.TTF",
                "BebasKai-Regular.otf",
                "BebasNeue Bold.ttf",
                "BebasNeue Book.ttf",
                "BebasNeue Light.ttf",
                "BebasNeue Regular.ttf",
                "BIRA_PERSONAL_USE_ONLY.TTF",
                "BLAIRMDITC TT MEDIUM.TTF",
                "brahmi.ttf",
                "BrushScriptStd_0.otf",
                "Butler_Medium.otf",
                "canter bold 3d.otf",
                "cerbon.TTF",
                "Channel.ttf",
                "charpentier renaissance pro.OTF",
                "Chasing Hearts - OTF.otf",
                "Chunkfive.otf",
                "Cookie-Regular.ttf",
                "COOLVETICA RG.TTF",
                "CurlzMT regular.ttf",
                "Default.ttf",
                "edo.ttf",
                "elmessiri-bold.otf",
                "ENGLEBERT-REGULAR.TTF",
                "EXO-BOLD.TTF",
                "EXO-MEDIUM.TTF",
                "FontsFree-Net-arial-bold.ttf",
                "FredokaOne-Regular.ttf",
                "FunnyKid.ttf",
                "GOOD TIMES RG.TTF",
                "GREATVIBES-REGULAR.TTF",
                "Harabara Mais Black.otf",
                "Harabara Mais Bold.otf",
                "Harabara Mais Light.otf",
                "Harabara Mais.otf",
                "HARABARA.TTF",
                "Lato-Black.ttf",
                "Lato-BlackItalic.ttf",
                "Lato-Bold.ttf",
                "Lato-BoldItalic.ttf",
                "Lato-Italic.ttf",
                "Lato-Light.ttf",
                "Lato-LightItalic.ttf",
                "Lato-Regular.ttf",
                "Lato-Thin.ttf",
                "Lato-ThinItalic.ttf",
                "LemonMilk.otf",
                "LemonMilkbold.otf",
                "MATURASC.TTF",
                "Merriweather-BoldItalic.ttf",
                "MISTRAL.TTF",
                "Montserrat-Black.ttf",
                "Montserrat-Bold.ttf",
                "Montserrat-BoldItalic.otf",
                "Montserrat-ExtraBold.ttf",
                "Montserrat-Light.ttf",
                "Montserrat-Medium.ttf",
                "Montserrat-Regular.ttf",
                "Montserrat-SemiBold.ttf",
                "MONTSERRATSUBRAYADA-BOLD.TTF",
                "MONTSERRATSUBRAYADA-REGULAR.TTF",
                "MYRIADPRO-REGULAR.OTF",
                "NEUROPOL.ttf",
                "Nexa Bold.otf",
                "Nexa Light.otf",
                "NexaRustSans-Black.otf",
                "NexaRustScriptL-0.otf",
                "NORICAN-REGULAR.TTF",
                "OpenSans-Bold.ttf",
                "OpenSans-BoldItalic.ttf",
                "OpenSans-ExtraBold.ttf",
                "OpenSans-ExtraBoldItalic.ttf",
                "OpenSans-Italic.ttf",
                "OpenSans-Light.ttf",
                "OpenSans-LightItalic.ttf",
                "OpenSans-Regular.ttf",
                "OpenSans-SemiBold.ttf",
                "OpenSans-SemiBoldItalic.ttf",
                "Oswald-Bold_0.ttf",
                "OSWALD-BOLD.TTF",
                "Oswald-DemiBold.ttf",
                "Oswald-ExtraLight.ttf",
                "Oswald-Light_0.ttf",
                "Oswald-Medium.ttf",
                "Oswald-Regular_0.ttf",
                "Oswald-Regular.ttf",
                "Oswald-SemiBold.ttf",
                "paola.TTF",
                "PatuaOne-Regular.ttf",
                "perpetua.TTF",
                "quicksand dash.otf",
                "Raleway-Bold.ttf",
                "Raleway-ExtraBold.ttf",
                "Raleway-ExtraLight.ttf",
                "Raleway-Medium.ttf",
                "Raleway-Regular.ttf",
                "Raleway-SemiBold.ttf",
                "reona reguler.otf",
                "RifficFree-Bold.ttf",
                "Roboto-Black.ttf",
                "Roboto-Bold.ttf",
                "Roboto-BoldItalic.ttf",
                "Roboto-Condensed.ttf",
                "Roboto-Italic.ttf",
                "Roboto-Light.ttf",
                "Roboto-Medium.ttf",
                "Roboto-MediumItalic.ttf",
                "Roboto-Regular.ttf",
                "Rochester-Regular.ttf",
                "ROUND BOLD.TTF",
                "Sail-Regular.ttf",
                "sail.otf",
                "Sancreek-Regular.ttf",
                "SansitaOne.ttf",
                "segoesc_0.ttf",
                "Sofia-Regular.otf",
                "sourcesanspro_regular.otf",
                "SourceSansPro-Black.otf",
                "SourceSansPro-Black.ttf",
                "SourceSansPro-BlackIt.otf",
                "SourceSansPro-BlackItalic.ttf",
                "SourceSansPro-Bold.otf",
                "SourceSansPro-Bold.ttf",
                "SourceSansPro-BoldIt.otf",
                "SourceSansPro-BoldItalic.ttf",
                "SourceSansPro-ExtraLight.otf",
                "SourceSansPro-ExtraLight.ttf",
                "SourceSansPro-ExtraLightIt.otf",
                "SourceSansPro-ExtraLightItalic.ttf",
                "SourceSansPro-It.otf",
                "SourceSansPro-Italic.ttf",
                "SourceSansPro-Light.otf",
                "SourceSansPro-Light.ttf",
                "SourceSansPro-LightIt.otf",
                "SourceSansPro-LightItalic.ttf",
                "SOURCESANSPRO-REGULAR.OTF",
                "SourceSansPro-Regular.ttf",
                "SourceSansPro-Semibold.ttf",
                "spyagencyv3.ttf",
                "steelfish rg.ttf",
                "times_0.ttf",
                "times-new-roman-bold.ttf",
                "timesbd_0.ttf"]


struct MerchantsDetails {
    let name : String
    let logo : String
    let color : UIColor
}

extension MerchantsDetails {
    
    static func getDefaultData() -> MerchantsDetails {
        let details = MerchantsDetails(name: "Taskly",
                                       logo: "https://cdn.Taskly.store/wp-content/uploads/2021/01/frimline_logo.jpg",
                                       color: .red)
        return details
    }
}


