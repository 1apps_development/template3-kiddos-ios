//
//  Board.swift
//  Taskly
//
//  Created by mac on 09/03/22.
//

import Foundation

class Board: Codable {
    
    var title: String
    var items: [String]
    
    init(title: String, items: [String]) {
        self.title = title
        self.items = items
    }
}
