//
//  AddressCell.swift
//  kiddos
//
//  Created by mac on 12/10/22.
//

import UIKit

class AddressCell: UITableViewCell {

    @IBOutlet weak var lbl_saveaddressas: UILabel!
    @IBOutlet weak var cellView: ShadowView!
    @IBOutlet weak var lbl_fulladdress: UILabel!
    @IBOutlet weak var img_selected: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configCell(_ address: AddressLists) {
        lbl_saveaddressas.text = address.title
        lbl_fulladdress.text = "\(address.address!), \(address.cityname!), \(address.statename!), \(address.countryname!) -\(address.postcode!)"
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
