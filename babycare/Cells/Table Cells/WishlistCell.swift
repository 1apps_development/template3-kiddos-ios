//
//  WishlistCell.swift
//  kiddos
//
//  Created by mac on 19/04/22.
//

import UIKit
import SDWebImage

class WishlistCell: UITableViewCell {
    
    @IBOutlet weak var img_product: RoundableImageView!
    @IBOutlet weak var lbl_totalPrice: UILabel!
    @IBOutlet weak var lbl_varientname: UILabel!
    @IBOutlet weak var lbl_productname: UILabel!
    @IBOutlet weak var lbl_currency: UILabel!
    
    var isWishlist:Bool = false

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configCell(_ wishlists: WishlistData) {
        lbl_totalPrice.text = wishlists.finalprice
        let productimgURL = getImageFullURL("\(wishlists.productimage!)")
        img_product.sd_setImage(with: URL(string: productimgURL)) { image, error, type, url in
            self.img_product.image = image
        }
        lbl_varientname.text = wishlists.varientname
        lbl_productname.text = wishlists.productname
        lbl_currency.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCYNAME)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
