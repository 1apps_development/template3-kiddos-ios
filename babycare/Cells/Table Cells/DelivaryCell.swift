//
//  DelivaryCell.swift
//  kiddos
//
//  Created by mac on 14/10/22.
//

import UIKit
import SDWebImage

class DelivaryCell: UITableViewCell {
    
    @IBOutlet weak var lbl_Price: UILabel!
    @IBOutlet weak var img_payment: UIImageView!
    @IBOutlet weak var lbl_address: UILabel!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var img_selected: UIImageView!
    @IBOutlet weak var cellView: ShadowView!
    @IBOutlet weak var aditinalprice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configCell(_ delivary: DelivaryList) {
        if delivary.chage_type == "percentage" {
            lbl_Price.text = "\(delivary.amount!) %"
        }else{
            lbl_Price.text = "\(UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY)!) \(delivary.amount!)"
        }
        let imageURL = getImageFullURL("\(delivary.imagepath!)")
        img_payment.sd_setImage(with: URL(string: imageURL)) { image, error, type, url in
            self.img_payment.image = image
        }
        lbl_address.text = delivary.descript
        lbl_title.text = delivary.name
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
