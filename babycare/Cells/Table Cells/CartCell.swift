//
//  CartCell.swift
//  kiddos
//
//  Created by mac on 18/04/22.
//

import UIKit
import SDWebImage

class CartCell: UITableViewCell {
    
    
    @IBOutlet weak var lbl_productname: UILabel!
    @IBOutlet weak var img_product: UIImageView!
    @IBOutlet weak var lbl_price: UILabel!
    @IBOutlet weak var btn_plus: UIButton!
    @IBOutlet weak var lbl_finalPrice: UILabel!
    @IBOutlet weak var lbl_value: UILabel!
    @IBOutlet weak var btn_minus: UIButton!
    @IBOutlet weak var lbl_currency: UILabel!
    @IBOutlet weak var qtyView: UIView!
    //    var qty: Int = 1
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func configCell(_ products: CartsList){
        lbl_productname.text = products.name
        let productimgURL = "\(URL_BASEIMAGE)\(products.image!)"
        img_product.sd_setImage(with: URL(string: productimgURL)) { image, error, type, url in
            self.img_product.image = image
        }
        let price = Double(products.finalprice!)! * Double(products.qty!)
        lbl_price.text = products.varientName
        lbl_finalPrice.text = "\(price)"
        lbl_value.text = "\(products.qty!)"
        lbl_currency.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCYNAME)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
