//
//  HomeCategoriesCell.swift
//  kiddos
//
//  Created by mac on 13/09/22.
//

import UIKit
import SDWebImage

class HomeCategoriesCell: UITableViewCell {

    @IBOutlet weak var goCategoriesButton: UIButton!
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var productBG: UIImageView!
    
    var onclickCategoriesClosure: (()->Void)?
    
    func configureCell(_ categories: CategoryData){
        let imgURL = getImageFullURL("\(categories.imagepath!)")
        productBG.sd_setImage(with: URL(string: imgURL)) { image, error, type, url in
            self.productBG.image = image
        }
        productTitle.text = categories.name
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        productBG.layer.cornerRadius = 10
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onclickGoCategories(_ sender: UIButton) {
        self.onclickCategoriesClosure?()
    }
}
