//
//  SettingsTableViewCell.swift
//  Taskly
//
//  Created by mac on 15/03/22.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {
    @IBOutlet weak var SettingIcon: UIImageView!
    @IBOutlet weak var lblMaintitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCellWith(_ settins: SettingItem){
        lblMaintitle.text = settins.title
        lblSubtitle.text = settins.subtitle
        SettingIcon.image = UIImage.init(named: settins.image)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
