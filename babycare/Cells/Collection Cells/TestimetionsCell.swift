//
//  TestimetionsCell.swift
//  kiddos
//
//  Created by mac on 22/09/22.
//

import UIKit

class TestimetionsCell: UICollectionViewCell {

    @IBOutlet weak var reviewDescription: UILabel!
    @IBOutlet weak var rateingView: CosmosView!
    @IBOutlet weak var lbl_rates: UILabel!
    @IBOutlet weak var clientName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
