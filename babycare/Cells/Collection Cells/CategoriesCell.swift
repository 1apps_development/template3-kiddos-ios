//
//  CategoriesCell.swift
//  kiddos
//
//  Created by mac on 13/09/22.
//

import UIKit
import SDWebImage

class CategoriesCell: UICollectionViewCell {

    @IBOutlet weak var catIcons: UIImageView!
    @IBOutlet weak var catTitle: UILabel!
    @IBOutlet weak var gotoCategoryButton: RoundedCornerButton!
    @IBOutlet weak var bgView: ShadowView!
    @IBOutlet weak var categoryHeight: NSLayoutConstraint!
    @IBOutlet weak var categoryBottem: NSLayoutConstraint!
    
    var catID: Int?
    var onclickGoCategoryClosure: (()->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override var isSelected: Bool {
        didSet {
            self.bgView.backgroundColor = self.isSelected ? hexStringToUIColor(hex: "#FBD9B3") : .white
        }
    }
    func configureCell(_ categorie: CategoryData){
        let iconURL = getImageFullURL("\(categorie.iconpath!)")
        catIcons.sd_setImage(with: URL(string: iconURL)) { image, error, type, url in
            self.catIcons.image = image
        }
        catTitle.text = categorie.name
        catID = categorie.id
//        catID = categorie.id
    }
    
    @IBAction func onclickGoCategory(_ sender: UIButton) {
        self.onclickGoCategoryClosure?()
    }
    
}
