//
//  ToysCell.swift
//  kiddos
//
//  Created by mac on 13/04/22.
//

import UIKit
import SDWebImage

class ToysCell: UICollectionViewCell {
    
    @IBOutlet weak var lbl_currency: UILabel!
    @IBOutlet weak var btn_fev: UIButton!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var btnAddtocart: UIButton!
    @IBOutlet weak var proprice: UILabel!
    @IBOutlet weak var wishlistView: ShadowView!
    
    var onclickAddCartClosure: (()->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func configreCell(_ products: Products){
        let productimgURL = getImageFullURL("\(products.coverimagepath!)")
        imgProduct.sd_setImage(with: URL(string: productimgURL)) { image, error, type, url in
            self.imgProduct.image = image
        }
        lblProductName.text = products.name
        lbl_currency.text = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CURRENCY)
        proprice.text = products.finalPrice
    }
    
    @IBAction func onclickAddtoCart(_ sender: UIButton) {
        self.onclickAddCartClosure?()
    }
    
}
