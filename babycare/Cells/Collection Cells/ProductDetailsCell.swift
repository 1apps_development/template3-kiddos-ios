//
//  ProductDetailsCell.swift
//  kiddos
//
//  Created by mac on 15/09/22.
//

import UIKit

class ProductDetailsCell: UICollectionViewCell {

    @IBOutlet weak var productsDetialsImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func configureCell(_ subproducts: ProductImages){
        let subURL = getImageFullURL("\(subproducts.imagePath!)")
        productsDetialsImg.sd_setImage(with: URL(string: subURL)) { image, error, type, url in
            self.productsDetialsImg.image = image
        }
    }

}
