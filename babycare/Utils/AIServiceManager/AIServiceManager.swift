//
//  AIServiceManager.swift
//  Swift3CodeStucture
//
//  Created by Vrushik on 10/02/21.
//  Copyright © 2016 agilepc-100. All rights reserved.
//

import Alamofire
import UIKit


class AIServiceManager: NSObject {
    
    static let sharedManager : AIServiceManager = {
        let instance = AIServiceManager()
        return instance
    }()
    
    // MARK: - ERROR HANDLING
    
    func handleError(_ errorToHandle : NSError){
        
        if(errorToHandle.domain == CUSTOM_ERROR_DOMAIN)	{
            //let dict = errorToHandle.userInfo as NSDictionary
            displayAlertWithTitle(APP_NAME, andMessage:"Something went wrong.", buttons: ["Dismiss"], completion: nil)
            
        }else if(errorToHandle.code == -1009){
            
            displayAlertWithTitle(APP_NAME, andMessage: "Please check your internet connection and try again.", buttons: ["Dismiss"], completion: nil)
            
        }
        else{
            
            if(errorToHandle.code == -999){
                
                return
                
            }
            
            displayAlertWithTitle(APP_NAME, andMessage:errorToHandle.localizedDescription, buttons: ["Dismiss"], completion:nil)
        }
    }
    
    
    // MARK: - ************* COMMON API METHOD **************
    
    // GET
    
    func callGetApi(_ url : String ,_ headers : HTTPHeaders? = nil, completionHandler : @escaping (AFDataResponse<Any>) -> ())
    {
        if IS_INTERNET_AVAILABLE()
        {
            SHOW_CUSTOM_LOADER()
            AF.request(url, method:.get, parameters: nil,headers:headers).responseJSON(completionHandler: completionHandler)
        }
        else
        {
            SHOW_INTERNET_ALERT()
        }
    }
    
    func callGetApiNoLoader(_ url : String ,_ headers : HTTPHeaders? = nil, completionHandler : @escaping (AFDataResponse<Any>) -> ())
    {
        if IS_INTERNET_AVAILABLE()
        {
            AF.request(url, method:.get, parameters: nil,headers:headers).responseJSON(completionHandler: completionHandler)
        }
        else
        {
            SHOW_INTERNET_ALERT()
        }
    }
    
    
    // Post method with loader
    
    func callPostApi(_ url : String, params : [String : Any]?,_ headers : HTTPHeaders? = nil, completionHandler :@escaping (AFDataResponse<Any>) -> ())
    {
        if IS_INTERNET_AVAILABLE()
        {
            SHOW_CUSTOM_LOADER()
//            SHOW_CUSTOM_LOADER_LOTTIE()
            
            AF.request(url, method: .post, parameters: params,encoding: JSONEncoding.default, headers:headers ).responseJSON(completionHandler: completionHandler)
            
        }
        else
        {
            SHOW_INTERNET_ALERT()
        }
    }
    
    /*
     name    multipart/form-data; charset=utf-8        vrushik Vrushik
     user_name    multipart/form-data; charset=utf-8        vk_pateltest
     school_name    multipart/form-data; charset=utf-8
     qualification    multipart/form-data; charset=utf-8        1
     grade    multipart/form-data; charset=utf-8        1
     country    multipart/form-data; charset=utf-8        1
     */
    /*
    func callPostUpdateProfile(_ urls : String, params : [String : Any]?,_ headers : HTTPHeaders? = nil, completionHandler :@escaping (AFDataResponse<Any>) -> ())
    {
            
        if IS_INTERNET_AVAILABLE()
        {
            SHOW_CUSTOM_LOADER()
//            SHOW_CUSTOM_LOADER_LOTTIE()
            AF.upload(multipartFormData: { (multipartData) in
               
                if let countryid = params?[K_Country] as? Int{
                    let cidStr = String(format: "%d", countryid)
                    multipartData.append(cidStr.data(using: String.Encoding.utf8)!, withName: K_Country)
                }
                if let userType = params?[K_User_Type] as? Int{
                    let uType = String(format:"%d", userType)
                    multipartData.append(uType.data(using: String.Encoding.utf8)!, withName: K_User_Type)
                }
                if let gradeid = params?[K_Grade] as? [Int]{
                    for i in  0 ..< gradeid.count{
                        let grd = String(format: "%d", gradeid[i])
                        multipartData.append(grd.data(using: String.Encoding.utf8)!, withName: "\(K_Grade)[]")
                    }
                }
                if let qualifid = params?[K_Qualification] as? [Int]{
                    for i in  0 ..< qualifid.count{
                        let qul = String(format: "%d", qualifid[i])
                        multipartData.append(qul.data(using: String.Encoding.utf8)!, withName: "\(K_Qualification)[]")
                    }
                }
            
                if let name = params?[K_name] as? String{
                    multipartData.append(name.data(using: String.Encoding.utf8)!, withName: K_name)
                }
                
                if let username = params?[K_User_Name] as? String{
                    multipartData.append(username.data(using: String.Encoding.utf8)!, withName: K_User_Name)
                }
                
                if let schoolName = params?[K_School_Name] as? String {
                    multipartData.append(schoolName.data(using: String.Encoding.utf8)!, withName: K_School_Name)
                }
                if let img = params?[K_Profile_Image] as? UIImage{
                    let timestamp = Date().timeIntervalSince1970 // just for some random name.
                    let fileName = String(format: "%.0f.jpg", timestamp)

                    if let imdata = img.jpegData(compressionQuality: 0.3){
                        multipartData.append(imdata, withName: K_Profile_Image, fileName: fileName, mimeType: "image/jpeg")
                    }
                }
                
            }, to: urls, method: .post, headers: headers).responseJSON(completionHandler: completionHandler)
        }
        else
        {
            SHOW_INTERNET_ALERT()
        }
        
    }
    
    func callPostCreateNewTeam(_ urls : String, params : [String : Any]?,_ headers : HTTPHeaders? = nil, completionHandler :@escaping (AFDataResponse<Any>) -> ())
    {
            
        if IS_INTERNET_AVAILABLE()
        {
            SHOW_CUSTOM_LOADER()
//            SHOW_CUSTOM_LOADER_LOTTIE()
            AF.upload(multipartFormData: { (multipartData) in
                
                if let usersAr = params?[K_Users_Arr] as? [MyFriendsDataModel]{
                    for i in 0 ..< usersAr.count{
                        let modl = usersAr[i]
                        let id = modl.id!
                        multipartData.append(id.data(using: String.Encoding.utf8)!, withName: "\(K_Users_Arr)[]")
                    }
                }
                if let teamName = params?[K_Team_Name] as? String{
                    multipartData.append(teamName.data(using: String.Encoding.utf8)!, withName: K_Team_Name)
                }

                if let img = params?[K_Profile_Image] as? UIImage{
                    let timestamp = Date().timeIntervalSince1970 // just for some random name.
                    let fileName = String(format: "%.0f.jpg", timestamp)

                    if let imdata = img.jpegData(compressionQuality: 0.3){
                        multipartData.append(imdata, withName: K_Profile_Image, fileName: fileName, mimeType: "image/jpeg")
                    }
                }
                
            }, to: urls, method: .post, headers: headers).responseJSON(completionHandler: completionHandler)
        }
        else
        {
            SHOW_INTERNET_ALERT()
        }
        
    }


   
    
    func callPostUploadAddQuiz(_ urls : String, params : [String : Any]?,_ headers : HTTPHeaders? = nil, completionHandler :@escaping (AFDataResponse<Any>) -> ())
    {
            
        if IS_INTERNET_AVAILABLE()
        {
            SHOW_CUSTOM_LOADER()
//            SHOW_CUSTOM_LOADER_LOTTIE()
            AF.upload(multipartFormData: { (multipartData) in
                let isDraft = params?[K_IsDraft] as! Int

                let title = params?[K_Title] as! String
                let description = params?[K_Description] as! String
                let privacy = params?[K_Privacy] as! Int
                let type = params?[K_Type] as! Int
                let typeStr = String(format: "%d", type)
                let privacyStr = String(format:"%d", privacy)
                let draftStr = String(format: "%d", isDraft)

                let subjects = params?[K_Subjects_arr] as? [Int]
                let tags = params?[K_Tags_arr] as? [String]
                let teams = params?[K_Teams_arr] as? [Int]
                var quizes = params?[K_Add_New_Quiz] as? [AddNewQuizDataModel]
                if quizes?[0].addNew == true{
                    quizes?.removeFirst()
                }
                multipartData.append(draftStr.data(using: String.Encoding.utf8)!, withName: K_IsDraft)
                multipartData.append(title.data(using: String.Encoding.utf8)!, withName: K_Title)
                multipartData.append(description.data(using: String.Encoding.utf8)!, withName: K_Description)
                multipartData.append(typeStr.data(using: String.Encoding.utf8)!, withName: K_Type)
                multipartData.append(privacyStr.data(using: String.Encoding.utf8)!, withName: K_Privacy)
                
                for i in 0 ..< tags!.count{
                    multipartData.append((tags?[i].data(using: String.Encoding.utf8)!)!, withName: "\(K_Tags_arr)[]")
                }
                for i in 0 ..< subjects!.count{
                    let subid = subjects?[i]
                    let subData = String(format: "%d", subid!)
                    multipartData.append(subData.data(using: String.Encoding.utf8)!, withName: "\(K_Subjects_arr)[]")

                }
                for i in 0 ..< teams!.count{
                    let teamid = teams?[i]
                    let teamData = String(format: "%d", teamid!)
                    multipartData.append(teamData.data(using: String.Encoding.utf8)!, withName: "\(K_Teams_arr)[]")
                }
                
                for i in 0 ..< quizes!.count{
                    if let quiztitle = quizes![i].question{
                        multipartData.append(quiztitle.data(using: String.Encoding.utf8)!, withName: "quiz[\(i)][question]")
                    }
                    if let quizAnswer = quizes![i].answer{
                        multipartData.append(quizAnswer.data(using: String.Encoding.utf8)!, withName: "quiz[\(i)][answer]")

                    }
                    let questMedia = quizes![i].questionMedia
                    let ansMedia = quizes![i].answerMedia

                    if questMedia.count > 0{
                        for j in 0 ..< questMedia.count{
                            if let vidstr = questMedia[j] as? String{
                                let timestamp = Date().timeIntervalSince1970 // just for some random name.

                                let fileName = "\(timestamp).mp4"//String(format: "%d\(j).mp4", timestamp)

                                if let vidurl = URL.init(string: vidstr) {
                                    multipartData.append(vidurl, withName:"quiz[\(i)][question_media][\(j)]", fileName:fileName, mimeType: "video/mp4")
                                }
                            }else if let med = questMedia[j] as? UIImage{
                                let timestamp = Date().timeIntervalSince1970 // just for some random name.

                                let fileName = "\(timestamp).jpg"//String(format: "%.0f\(j).jpg", timestamp)

                                if let imageData = med.jpegData(compressionQuality: 0.3){
                                    multipartData.append(imageData, withName:"quiz[\(i)][question_media][\(j)]", fileName:fileName, mimeType: "image/jpeg")
                                }
                            }
                        }
                    }
                    
                    if ansMedia.count > 0{
                        for j in 0 ..< ansMedia.count{
                            if let vidstr = ansMedia[j] as? String{
                                let timestamp = Date().timeIntervalSince1970 // just for some random name.

                                let fileName = "\(timestamp).mp4"//String(format: "%d\(j).mp4", timestamp)

                                if let vidurl = URL.init(string: vidstr) {
                                    multipartData.append(vidurl, withName:"quiz[\(i)][answer_media][\(j)]", fileName:fileName, mimeType: "video/mp4")
                                }
                            }else if let med = ansMedia[j] as? UIImage{
                                let timestamp = Date().timeIntervalSince1970 // just for some random name.

                                let fileName = "\(timestamp).jpg"//String(format: "%.0f\(j).jpg", timestamp)

                                if let imageData = med.jpegData(compressionQuality: 0.3){
                                    multipartData.append(imageData, withName:"quiz[\(i)][answer_media][\(j)]", fileName:fileName, mimeType: "image/jpeg")
                                }
                            }
                        }
                    }
                }
            }, to: urls, method: .post, headers: headers).responseJSON(completionHandler: completionHandler)
        }
        else
        {
            SHOW_INTERNET_ALERT()
        }
        
//        responseJSON(queue: .main, options: .allowFragments) { (response) in
//                switch response.result{
//                case .success(let value):
//                    print("Json: \(value)")
//                case .failure(let error):
//                    print("Error: \(error.localizedDescription)")
//                }
//            }.uploadProgress { (progress) in
//                print("Progress: \(progress.fractionCompleted)")
//            }
    }
    
    func callPostEditQuiz(_ urls : String, params : [String : Any]?,_ headers : HTTPHeaders? = nil, completionHandler :@escaping (AFDataResponse<Any>) -> ())
    {
            
        if IS_INTERNET_AVAILABLE()
        {
            SHOW_CUSTOM_LOADER()
//            SHOW_CUSTOM_LOADER_LOTTIE()
            AF.upload(multipartFormData: { (multipartData) in
                let isDraft = params?[K_IsDraft] as! Int
                let qqid = params?[K_QQ_Id] as! Int
                let title = params?[K_Title] as! String
                let description = params?[K_Description] as! String
                let privacy = params?[K_Privacy] as! Int
                let type = params?[K_Type] as! Int
               
                let qqqidStr = String(format: "%d", qqid)
                let typeStr = String(format: "%d", type)
                let privacyStr = String(format:"%d", privacy)
                let draftStr = String(format: "%d", isDraft)

                let subjects = params?[K_Subjects_arr] as? [Int]
                let tags = params?[K_Tags_arr] as? [String]
                let quizids = params?[K_Qid] as? [Int]
                let teams = params?[K_Teams_arr] as? [Int]
                var quizes = params?[K_Add_New_Quiz] as? [AddNewQuizDataModel]
                let existQuiz = params?[K_Exist_Quizs] as? [AddNewQuizDataModel]
                if quizes?[0].addNew == true{
                    quizes?.removeFirst()
                }
                multipartData.append(qqqidStr.data(using: String.Encoding.utf8)!, withName: K_QQ_Id)
                multipartData.append(draftStr.data(using: String.Encoding.utf8)!, withName: K_IsDraft)
                multipartData.append(title.data(using: String.Encoding.utf8)!, withName: K_Title)
                multipartData.append(description.data(using: String.Encoding.utf8)!, withName: K_Description)
                multipartData.append(typeStr.data(using: String.Encoding.utf8)!, withName: K_Type)
                multipartData.append(privacyStr.data(using: String.Encoding.utf8)!, withName: K_Privacy)
                
                for i in 0 ..< tags!.count{
                    multipartData.append((tags?[i].data(using: String.Encoding.utf8)!)!, withName: "\(K_Tags_arr)[]")
                }
                for i in 0 ..< subjects!.count{
                    let subid = subjects?[i]
                    let subData = String(format: "%d", subid!)
                    multipartData.append(subData.data(using: String.Encoding.utf8)!, withName: "\(K_Subjects_arr)[]")

                }
                for i in 0 ..< teams!.count{
                    let teamid = teams?[i]
                    let teamData = String(format: "%d", teamid!)
                    multipartData.append(teamData.data(using: String.Encoding.utf8)!, withName: "\(K_Teams_arr)[]")
                }
                for i in 0 ..< quizids!.count{
                    let quizid = quizids?[i]
                    let quizData = String(format: "%d", quizid!)
                    multipartData.append(quizData.data(using: String.Encoding.utf8)!, withName: "\(K_Qid)[]")
                }
                for a in 0 ..< existQuiz!.count{
                    let questMedia = existQuiz![a].questionMedia
                    let ansMedia = existQuiz![a].answerMedia
                    if questMedia.count > 0{
                        for b in 0 ..< questMedia.count{
                            let str = questMedia[b] as! String
                            multipartData.append(str.data(using: String.Encoding.utf8)!, withName: "quiz[\(a)][exist_question_media][\(b)]")
                        }
                    }
                    if ansMedia.count > 0{
                        for c in 0 ..< ansMedia.count{
                            let str = ansMedia[c] as! String
                            multipartData.append(str.data(using: String.Encoding.utf8)!, withName: "quiz[\(a)][exist_answer_media][\(c)]")
                        }
                    }
                }
                
                for i in 0 ..< quizes!.count{
                    if let quiztitle = quizes![i].question{
                        multipartData.append(quiztitle.data(using: String.Encoding.utf8)!, withName: "quiz[\(i)][question]")
                    }
                    if let quizAnswer = quizes![i].answer{
                        multipartData.append(quizAnswer.data(using: String.Encoding.utf8)!, withName: "quiz[\(i)][answer]")

                    }
                    let questMedia = quizes![i].questionMedia
                    let ansMedia = quizes![i].answerMedia
                    let existingQuestion = quizes![i].questionMediaExisting
                    let exisgingAnswer = quizes![i].answerMediaExisting
                    if questMedia.count > 0{
                        for j in 0 ..< questMedia.count{
                            if let vidstr = questMedia[j] as? String{
                                let timestamp = Date().timeIntervalSince1970 // just for some random name.
                                
                                let fileName = "\(timestamp).mp4"//String(format: "%d\(j).mp4", timestamp)
                                
                                if let vidurl = URL.init(string: vidstr) {
                                    if !existingQuestion{
                                        multipartData.append(vidurl, withName: "quiz[\(i)][question_media][\(j)]", fileName:fileName, mimeType: "video/mp4")
                                    }
                                }
                            }else if let med = questMedia[j] as? UIImage{
                                let timestamp = Date().timeIntervalSince1970 // just for some random name.
                                
                                let fileName = "\(timestamp).jpg"//String(format: "%.0f\(j).jpg", timestamp)
                                
                                if let imageData = med.jpegData(compressionQuality: 0.3){
                                    if !existingQuestion{
                                        multipartData.append(imageData, withName: "quiz[\(i)][question_media][\(j)]", fileName:fileName, mimeType: "image/jpeg")
                                    }
                                }
                            }
                        }
                    }
                    
                    if ansMedia.count > 0{
                        for j in 0 ..< ansMedia.count{
                            if let vidstr = ansMedia[j] as? String{
                                let timestamp = Date().timeIntervalSince1970 // just for some random name.
                                
                                let fileName = "\(timestamp).mp4"//String(format: "%d\(j).mp4", timestamp)
                                
                                if let vidurl = URL.init(string: vidstr) {
                                    if !exisgingAnswer{
                                        multipartData.append(vidurl, withName: "quiz[\(i)][answer_media][\(j)]", fileName:fileName, mimeType: "video/mp4")
                                    }
                                }
                            }else if let med = ansMedia[j] as? UIImage{
                                let timestamp = Date().timeIntervalSince1970 // just for some random name.
                                
                                let fileName = "\(timestamp).jpg"//String(format: "%.0f\(j).jpg", timestamp)
                                
                                if let imageData = med.jpegData(compressionQuality: 0.3){
                                    if !exisgingAnswer{
                                        multipartData.append(imageData, withName: "quiz[\(i)][answer_media][\(j)]", fileName:fileName, mimeType: "image/jpeg")
                                    }
                                }
                            }
                        }
                    }
                }
            }, to: urls, method: .post, headers: headers).responseJSON(completionHandler: completionHandler)
        }
        else
        {
            SHOW_INTERNET_ALERT()
        }
    }
    
    func callPostUploadCardforMultipart(_ urls : String, params : [String : Any]?,_ headers : HTTPHeaders? = nil, completionHandler :@escaping (AFDataResponse<Any>) -> ())
    {
            
        if IS_INTERNET_AVAILABLE()
        {
            //            SHOW_CUSTOM_LOADER()
            SHOW_CUSTOM_LOADER_LOTTIE()
            AF.upload(multipartFormData: { (multipartData) in
                let title = params?[K_Title] as! String
                let type = params?[K_Card_Type] as! String
                guard let imgFront = params?[K_Front] as? UIImage else{return}
                guard let imgBack = params?[K_Back] as? UIImage else{return}
                if let mainID = params?[K_Main_Id] as? Int {
                    if mainID != 0{
                        multipartData.append("\(mainID)".data(using: String.Encoding.utf8)!, withName: K_Main_Id)
                    }
                }
                
                if let imageData = imgFront.jpegData(compressionQuality: 0.3){
                    multipartData.append(imageData, withName:K_Front, fileName:"camera_front.jpg", mimeType: "image/jpeg")
                    if let fileExt = params?[K_File_Ext] as? String{
                        multipartData.append(fileExt.data(using: String.Encoding.utf8)!, withName: K_File_Ext)
                    }
                }
                if let imageDataB = imgBack.jpegData(compressionQuality: 0.3){
                    multipartData.append(imageDataB, withName:K_Back, fileName:"camera_back.jpg", mimeType: "image/jpeg")
                    if let fileExt = params?[K_File_Ext] as? String{
                        multipartData.append(fileExt.data(using: String.Encoding.utf8)!, withName: K_File_Ext)
                    }
                }
                multipartData.append(title.data(using: String.Encoding.utf8)!, withName: K_Title)
                multipartData.append(type.data(using: String.Encoding.utf8)!, withName: K_Card_Type)

                
            }, to: urls, method: .post, headers: headers).responseJSON(completionHandler: completionHandler)
        }
        else
        {
            SHOW_INTERNET_ALERT()
        }

    }
 */
    
    // Post method without loader
    
    func callPostApiWithoutLoader(_ url : String, params : [String : AnyObject]?, completionHandler :@escaping (AFDataResponse<Any>) -> ())
    {
        if IS_INTERNET_AVAILABLE()
        {
           
                
                AF.request(url, method: .post, parameters: params).responseJSON(completionHandler: completionHandler)
                
           
        }else{
            SHOW_INTERNET_ALERT()
        }
    }
    
    
    
    func call_Chat_PostApiWithoutLoader(_ url : String, params : [String : AnyObject]?, completionHandler :@escaping (AFDataResponse<Any>) -> ())
    {
        if IS_INTERNET_AVAILABLE()
        {
            
            AF.request(url, method: .post, parameters: params).responseJSON(completionHandler: completionHandler)
            
            
        }else{
            SHOW_INTERNET_ALERT()
        }
    }
    
    // Post method without Parameter
    
    func callPostApiwithoutParameter(_ url : String, completionHandler :@escaping (AFDataResponse<Any>) -> ())
    {
        if IS_INTERNET_AVAILABLE()
        {
            
            SHOW_CUSTOM_LOADER()
            AF.request(url, method: .post).responseJSON(completionHandler: completionHandler)
            
        }
        else
        {
            SHOW_INTERNET_ALERT()
        }
        
    }
    
    func callPostWithToogleLoaderApi(_ url : String,_ isShow : Bool!, params : [String : AnyObject]?, completionHandler :@escaping (AFDataResponse<Any>) -> ())
    {
        if IS_INTERNET_AVAILABLE()
        {
            if(isShow)
            {
                SHOW_CUSTOM_LOADER()
            }
            
            AF.request(url, method: .post, parameters: params).responseJSON(completionHandler: completionHandler)
            
        }
        else
        {
            if(isShow)
            {
                SHOW_INTERNET_ALERT()
            }
        }
        
    }
    
    // Session Expired
    func handleStatusCodeAction(dictJson : NSDictionary!, completetion : @escaping () -> Void)
    {
        if (dictJson.value(forKey: "success")as! Int == 4){
            displayAlertWithMessageFromVC(CommonUtils.shared().getTopMostViewController()!, message: dictJson.object_forKeyWithValidationForClass_String("message"), buttons: ["Ok"], completion: { (index) in
                //                CommonUtils.logOut()
                
                CommonUtils.shared().setIntroViewController()
            })
        }
        else
        {
            //            displayAlertWithMessage(dictJson.object_forKeyWithValidationForClass_String("message"))
            completetion()
        }
    }
    
}

func createPDF(image: UIImage) -> NSData? {

    let pdfData = NSMutableData()
    let pdfConsumer = CGDataConsumer(data: pdfData as CFMutableData)!

    var mediaBox = CGRect.init(x: 0, y: 0, width: image.size.width, height: image.size.height)

    let pdfContext = CGContext(consumer: pdfConsumer, mediaBox: &mediaBox, nil)!

    pdfContext.beginPage(mediaBox: &mediaBox)
    pdfContext.draw(image.cgImage!, in: mediaBox)
    pdfContext.endPage()

    return pdfData
}
