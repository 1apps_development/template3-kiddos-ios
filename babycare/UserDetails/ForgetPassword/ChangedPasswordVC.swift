//
//  ChangedPasswordVC.swift
//  kiddos
//
//  Created by mac on 08/09/22.
//

import UIKit

class ChangedPasswordVC: UIViewController {
    
    //MARK: - ViewLifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: - IBActions
    
    @IBAction func onclickloginButton(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
    }
    
}
