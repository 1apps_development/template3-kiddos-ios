//
//  ForgetPasswordVC.swift
//  kiddos
//
//  Created by mac on 07/09/22.
//

import UIKit

class ForgetPasswordVC: UIViewController {
    
    //MARK: - Outlets
    
    @IBOutlet weak var enterPasswordTextField: UITextField!
    @IBOutlet weak var varifiedEmailLabel: UILabel!
    
    //MARK: - ViewLifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: - IBActions
    
    @IBAction func onclickContectUS(_ sender: UIButton) {
        guard let url = URL(string: UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CONTECTUS)!) else {
            return
        }
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    @IBAction func onclickSendCode(_ sender: UIButton) {
        if enterPasswordTextField.text! == ""{
            self.view.makeToast("Email Field can not be empty")
        }else if enterPasswordTextField.text! != "" && enterPasswordTextField.text!.isValidEmailBoth(str: enterPasswordTextField.text!) == false{
            self.view.makeToast("Please enter valid Email Address")
        }else{
            let param: [String: Any] = ["email": enterPasswordTextField.text!,
                                        "theme_id": APP_THEMEID]
            if enterPasswordTextField.text == getEmail(){
                sendCodeToEmail(param)
            }
            printD("Error")
        }
    }
    //MARK: - CustomFuctions
    
    //MARK: - APIFuctions
    
    func sendCodeToEmail(_ param: [String: Any]){
        AIServiceManager.sharedManager.callPostApi(URL_SendCode, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    let status = json["status"] as? Int
                       let msg = json["message"] as? String
                        self.view.makeToast(msg!)
                    if status == 1 {
                        if msg == "successfull"{
                            let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "VarificationVC") as! VarificationVC
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }else{
                        showAlertMessage(titleStr: Bundle.main.applicationName!, messageStr: msg!)
                    }
                        
                    printD(json)
                }
            case let .failure(error):
                HIDE_CUSTOM_LOADER()
                printD("Error: \(error.localizedDescription)")
                break
            }
        }
    }
    
}
