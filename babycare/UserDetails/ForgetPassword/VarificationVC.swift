//
//  ResetPasswordVC.swift
//  kiddos
//
//  Created by mac on 07/09/22.
//

import UIKit

class VarificationVC: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var enterOTPTextField: UITextField!
    
    //MARK: - ViewLifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: - IBActions
    
    
    @IBAction func onclickContectus(_ sender: UIButton) {
        guard let url = URL(string: UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CONTECTUS)!) else {
            return
        }
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func sendCodeButtonTapped(_ sender: UIButton) {
        let param: [String: Any] = ["email": getEmail(),
                                    "otp":   enterOTPTextField.text!,
                                    "theme_id": APP_THEMEID]
        varifiedOTP(param)
    }
    //MARK: - CustomFuctions
    
    //MARK: - APIFuctions
    func varifiedOTP(_ param: [String: Any]){
        AIServiceManager.sharedManager.callPostApi(URL_varifiedOTP, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    let status = json["status"] as? Int
                    let msg = json["message"] as? String
                     self.view.makeToast(msg!)
                    if status == 1{
                         if msg == "successfull"{
                             let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "NewpasswordVC") as! NewpasswordVC
                             self.navigationController?.pushViewController(vc, animated: true)
                         }
                    }else{
                        showAlertMessage(titleStr: Bundle.main.applicationName!, messageStr: msg!)
                    }
                    printD(json)
                }
            case let .failure(error):
                HIDE_CUSTOM_LOADER()
                printD("Error: \(error.localizedDescription)")
                break
            }
        }
    }
}
