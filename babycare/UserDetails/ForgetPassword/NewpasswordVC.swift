//
//  NewpasswordVC.swift
//  kiddos
//
//  Created by mac on 08/09/22.
//

import UIKit

class NewpasswordVC: UIViewController {
    
    //MARK: - Outlets
    
    @IBOutlet weak var newpasswordTextField: UITextField!
    @IBOutlet weak var confimpasswordTextField: UITextField!
    
    //MARK: - ViewLifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    //MARK: - IBActions
    
    
    @IBAction func onclickContectUs(_ sender: UIButton) {
        guard let url = URL(string: UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_CONTECTUS)!) else {
            return
        }
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func onclickSubmitbutton(_ sender: UIButton) {
        if newpasswordTextField.text! == ""{
            self.view.makeToast("Password Field can not be empty")
        }else if newpasswordTextField.text! != "" && !newpasswordTextField.text!.isValidPassword(){
            self.view.makeToast("Password should minimum 8 character and must contain 1 special character, 1 upper case, 1 lowercase and 1 number.")
        }else if newpasswordTextField.text != confimpasswordTextField.text{
            self.view.makeToast("Please cheak the password")
        }else{
            let param: [String: Any] = ["email": getEmail(),
                                        "password": newpasswordTextField.text!,
                                        "theme_id": APP_THEMEID]
            changethePassword(param)
        }
    }
    //MARK: - APIFunctions
    func changethePassword(_ param: [String: Any]){
        AIServiceManager.sharedManager.callPostApi(URL_PassWordSave, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    let status = json["status"] as? Int
                       let msg = json["message"] as? String
                        self.view.makeToast(msg!)
                    if status == 1 {
                        if msg == "successfull"{
                            let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "ChangedPasswordVC") as! ChangedPasswordVC
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }else{
                        showAlertMessage(titleStr: Bundle.main.applicationName!, messageStr: msg!)
                    }
                    printD(json)
                }
            case let .failure(error):
                HIDE_CUSTOM_LOADER()
                printD("Error: \(error.localizedDescription)")
                break
            }

        }
    }
}
