//
//  SigninEmailVC.swift
//  kiddos
//
//  Created by mac on 07/09/22.
//

import UIKit
import Alamofire

class SigninEmailVC: UIViewController {
    
    //MARK: - Outlets

    @IBOutlet weak var fullnameTextField: UITextField!
    @IBOutlet weak var phonenumberTextField: UITextField!
    @IBOutlet weak var emialaddressTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var btnHideShow: UIButton!
    
    //MARK: - ViewLifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
    }
    
    //MARK: - ButtonFuctions
    
    func setupInitialUI(){
        self.btnHideShow.setImage(UIImage.init(named: "password eye close"), for: .normal)
        self.btnHideShow.setImage(UIImage(named: "password eye"), for: .selected)
    }
    
    //MARK: - IBActions
    
    @IBAction func onClickHideShow(_ sender: UIButton )
    {
        if sender.isSelected{
            btnHideShow.isSelected = false
            passwordTextField.isSecureTextEntry = true
        }else{
            btnHideShow.isSelected = true
            passwordTextField.isSecureTextEntry = false
        }
    }
    @IBAction func onclickRegister(_ sender: UIButton) {
//        if fullnameTextField.text! == ""{
//            self.view.makeToast("Fullname Field can not be empty")
//        }else if fullnameTextField.text! != "" && !fullnameTextField.text!.contains(find: " "){
//            self.view.makeToast("Please separate first name and lastname by white space")
//        }else if phonenumberTextField.text! == ""{
//            self.view.makeToast("Please enter phonenumber")
//        }else if phonenumberTextField.text! != "" && !phonenumberTextField.text!.isPhone(){
//            self.view.makeToast("Please enter valid Phone number")
//        }else if emialaddressTextField.text! == ""{
//            self.view.makeToast("Email Field can not be empty")
//        }else if emialaddressTextField.text! != "" && emialaddressTextField.text!.isValidEmailBoth(str: emialaddressTextField.text!) == false{
//            self.view.makeToast("Please enter valid Email Address")
//        }else if passwordTextField.text! == ""{
//            self.view.makeToast("Password Field can not be empty")
//        }else if passwordTextField.text! != "" && !passwordTextField.text!.isValidPassword(){
//            self.view.makeToast("Password should minimum 8 character and must contain 1 special character, 1 upper case, 1 lowercase and 1 number.")
//        }else{
            let fullName = fullnameTextField.text
            let fullNameArr = fullName!.components(separatedBy: " ")
            var fName: String = ""
            var lName: String = ""
            if fullNameArr.count > 1{
                fName = fullNameArr[0]
                lName = fullNameArr[1]
            }
            let firstName: String = fName
            UserDefaults.standard.set(firstName, forKey: userDefaultsKeys.KEY_FIRSTNAME)
            let lastName: String = lName
            UserDefaults.standard.set(lastName, forKey: userDefaultsKeys.KEY_LASTNAME)
            let param: [String:Any] = ["first_name": firstName,
                                       "last_name": lastName,
                                       "device_type": "ios",
                                       "register_type": "email",
                                       "mobile": phonenumberTextField.text!,
                                       "token": getFCMToken(),
                                       "email": emialaddressTextField.text!,
                                       "password": passwordTextField.text!,
                                       "theme_id": APP_THEMEID]
            getUserRegistedData(param)
            UserDefaults.standard.set(emialaddressTextField.text, forKey: userDefaultsKeys.KEY_EMAIL)
            UserDefaults.standard.set(phonenumberTextField.text, forKey: userDefaultsKeys.KEY_PHONE)
            UserDefaults.standard.set(fullnameTextField.text, forKey: userDefaultsKeys.KEY_FULLNAME)
//        }
    }
    
    //MARK: - APIFunctions
    
    func getUserRegistedData(_ param: [String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_Ragister, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    let msg = json["message"] as? String
                    if msg == "fail" {
                        if let datas = json["data"] as? [String:Any]{
                            let massage = datas["message"] as? String
                            self.view.makeToast(massage!)
                        }
                    }else{
                        let status = json["status"] as? Int
                        if status == 1 {
                            if let data = json["data"] as? [String:Any]{
                                let userid = data["id"] as? String
                                UserDefaults.standard.set(userid, forKey: userDefaultsKeys.KEY_USERID)
                                let firstname = data["first_name"] as? String
                                UserDefaults.standard.set(firstname, forKey: userDefaultsKeys.KEY_FIRSTNAME)
                                let lastname = data["last_name"] as? String
                                UserDefaults.standard.set(lastname, forKey: userDefaultsKeys.KEY_LASTNAME)
                                let email = data["email"] as? String
                                UserDefaults.standard.set(email, forKey: userDefaultsKeys.KEY_EMAIL)
                                let phonenumber = data["mobile"] as? String
                                UserDefaults.standard.set(phonenumber, forKey: userDefaultsKeys.KEY_PHONE)
                                let image = data["image"] as? String
                                UserDefaults.standard.set("\(URL_BASE)/\(image!)", forKey: userDefaultsKeys.KEY_USERPROFILE)
                                if let token = data["token"] as? String{
                                    setValueToUserDefaults(value: data as AnyObject, key: userDefaultsKeys.KEY_USER_REG_DATA)
                                    setValueToUserDefaults(value: token as AnyObject, key: userDefaultsKeys.KEY_APP_TOKEN)
                                    setValueToUserDefaults(value: true as AnyObject, key: userDefaultsKeys.KEY_IS_USER_LOGGED_IN)
                                    let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "MainTabbarVC") as! MainTabbarVC
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                                UserDefaults.standard.set([[String:Any]](), forKey: userDefaultsKeys.KEY_GESTUSEROBJ)
                                
                            }
                        }else{
                            showAlertMessage(titleStr: Bundle.main.applicationName!, messageStr: msg!)
                        }
                    }
                    printD(json)
                }
            case let .failure(error):
                HIDE_CUSTOM_LOADER()
                printD("Error: \(error.localizedDescription)")
                break
            }
        }
    }
}
