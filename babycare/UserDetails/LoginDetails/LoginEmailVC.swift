//
//  LoginEmailVC.swift
//  kiddos
//
//  Created by mac on 07/09/22.
//

import UIKit

class LoginEmailVC: UIViewController {
    
    //MARK: - Outlets
    
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailaddressTextField: UITextField!
    @IBOutlet weak var btnHideShow: UIButton!
    
    //MARK: - Viewlifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInitialUI()
        if IS_DEMO_MODE == true {
            self.emailaddressTextField.isUserInteractionEnabled = false
            self.emailaddressTextField.isUserInteractionEnabled = false
            self.passwordTextField.text = "123456"
            self.emailaddressTextField.text = "babycare@example.com"
        }else{
            self.passwordTextField.text = ""
            self.emailaddressTextField.text = ""
        }
    }
    //MARK: - Custom SetupFunctions
    func setupInitialUI(){
        self.btnHideShow.setImage(UIImage.init(named: "password eye"), for: .normal)
        self.btnHideShow.setImage(UIImage(named: "password eye close"), for: .selected)
    }
    //MARK: - IBActions
    
 
    @IBAction func onclickForgotpasswordButton(_ sender: UIButton) {
        let forgotpasswordVC = storyboard?.instantiateViewController(withIdentifier: "ForgetPasswordVC") as! ForgetPasswordVC
        navigationController?.pushViewController(forgotpasswordVC, animated: true)
    }
    @IBAction func onclickBtnHide(_ sender: UIButton) {
        if sender.isSelected{
            btnHideShow.isSelected = false
            passwordTextField.isSecureTextEntry = true
        }else{
            btnHideShow.isSelected = true
            passwordTextField.isSecureTextEntry = false
        }
    }
    
    @IBAction func onclickAccount(_ sender: UIButton) {
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "SigninVC") as! SigninVC
        navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func onclickLogin(_ sender: UIButton) {
//        if passwordTextField.text! == ""{
//            self.view.makeToast("Password Field can not be empty")
//        }else if passwordTextField.text! != "" && !passwordTextField.text!.isValidPassword(){
//            self.view.makeToast("Password should minimum 8 character and must contain 1 special character, 1 upper case, 1 lowercase and 1 number.")
//        }else if emailaddressTextField.text! == ""{
//            self.view.makeToast("Email Field can not be empty")
//        }else if emailaddressTextField.text! != "" && emailaddressTextField.text!.isValidEmailBoth(str: emailaddressTextField.text!) == false{
//            self.view.makeToast("Please enter valid Email Address")
//        }else{
            let param: [String:Any] = ["email": emailaddressTextField.text!,
                                       "password": passwordTextField.text!,
                                       "token": getFCMToken(),
                                       "device_type": "ios",
                                       "theme_id": APP_THEMEID]
            getusersLoginData(param)
//        }
    }
    
    //MARK: - APIFunctions
    
    func getusersLoginData(_ param: [String:Any], bypass:Bool = false){
        if bypass{
            let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "MainTabbarVC") as! MainTabbarVC
            self.navigationController?.pushViewController(vc, animated: true)

        }else{
            AIServiceManager.sharedManager.callPostApi(URL_Login, params: param, nil) { response in
                switch response.result{
                case let .success(result):
                    HIDE_CUSTOM_LOADER()
                    if let json = result as? [String:Any]{
                        let msg = json["message"] as? String
                        if msg == "fail" {
                            if let datas = json["data"] as? [String:Any]{
                                let massage = datas["message"] as? String
                                self.view.makeToast(massage!)
                            }
                        }else{
                            if let data = json["data"] as? [String:Any]{
                                if let msg = data["message"] as? String
                                {
                                    self.view.makeToast(msg)
                                }
                                let userid = data["id"] as? String
                                UserDefaults.standard.set(userid, forKey: userDefaultsKeys.KEY_USERID)
                                let firstname = data["first_name"] as? String
                                UserDefaults.standard.set(firstname, forKey: userDefaultsKeys.KEY_FIRSTNAME)
                                let lastname = data["last_name"] as? String
                                UserDefaults.standard.set(lastname, forKey: userDefaultsKeys.KEY_LASTNAME)
                                UserDefaults.standard.set(firstname, forKey: userDefaultsKeys.KEY_FULLNAME)
                                let email = data["email"] as? String
                                UserDefaults.standard.set(email, forKey: userDefaultsKeys.KEY_EMAIL)
                                let phonenumber = data["mobile"] as? String
                                UserDefaults.standard.set(phonenumber, forKey: userDefaultsKeys.KEY_PHONE)
                                let image = data["image"] as? String
                                UserDefaults.standard.set("\(URL_BASE)/\(image!)", forKey: userDefaultsKeys.KEY_USERPROFILE)
                                if let token = data["token"] as? String{
                                    setValueToUserDefaults(value: data as AnyObject, key: userDefaultsKeys.KEY_USER_REG_DATA)
                                    setValueToUserDefaults(value: token as AnyObject, key: userDefaultsKeys.KEY_APP_TOKEN)
                                    setValueToUserDefaults(value: true as AnyObject, key: userDefaultsKeys.KEY_IS_USER_LOGGED_IN)
                                    let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "MainTabbarVC") as! MainTabbarVC
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                                UserDefaults.standard.set([[String:Any]](), forKey: userDefaultsKeys.KEY_GESTUSEROBJ)
                            }
                        }
                        printD(json)
                    }
                case let .failure(error):
                    printD("error:\(error.localizedDescription)")
                    HIDE_CUSTOM_LOADER()
                }
            }
        }
    }
}
