import UIKit
import FBSDKLoginKit
import FacebookLogin
import GoogleSignIn
import AuthenticationServices

class LoginVC: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var loginwithfacebookButton: UIButton!
    
    //MARK: - Global Variables
    var googleSignIn = GIDSignIn.sharedInstance
    
    //MARK: - Viewlifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    //MARK: - IbActions
    @IBAction func onclickLoginEmail(_ sender: UIButton) {
        let loginEmailVC = storyboard?.instantiateViewController(withIdentifier: "LoginEmailVC") as! LoginEmailVC
        navigationController?.pushViewController(loginEmailVC, animated: true)
    }
    
    @IBAction func oncllickAccount(_ sender: UIButton) {
        let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "SigninVC") as! SigninVC
        navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func onclickLoginFacebook(_ sender: UIButton) {
        facebooksignup()
    }
    
    @IBAction func onclickLoginGoogle(_ sender: UIButton) {
        googleAuthSignin()
    }
    @IBAction func onclickLoginApple(_ sender: UIButton) {
        AppleSignup()
    }
    //MARK: - Custom Functions
    
    func facebooksignup()
        {
//            UserDefaults.standard.set(key_facebook, forKey: key_Type)
            let loginManager = LoginManager()
            loginManager.logIn(permissions: [.email], viewController: nil) { (loginResult) in
                switch loginResult {
                case .success( _, _, _):
                    let dictParamaters = ["fields":"id, name, email, first_name, last_name"]
                    let request: GraphRequest = GraphRequest.init(graphPath: "me", parameters: dictParamaters)
                    request.start { (connection, result, error) in
                        let responseData = result as! NSDictionary
                        let facebookId = responseData["id"] as! String
                        let firstname = responseData["first_name"] as! String
                        let lastname = responseData["last_name"] as! String
                        var email = ""
                        if responseData["email"] != nil {
                            email = responseData["email"] as! String
                        }
                        loginManager.logOut()
                        let params: NSDictionary = ["email":email,
                                                    "password":"",
                                                    "first_name": firstname,
                                                    "last_name": lastname,
                                                    "device_type":"ios",
                                                    "google_id":"",
                                                    "register_type":"facebook",
                                                    "facebook_id":facebookId,
                                                    "apple_id":"",
                                                    "mobile":"",
                                                    "token":getFCMToken(),
                                                    "theme_id": APP_THEMEID]
                        self.getUserRegistedData(params as! [String : Any])
                    }
                    break
                case .cancelled:
                    printD("Something Want wrong")
                    break
                    
                case .failed( _):
                    print("Something want Wrong")
                    break
                }
            }
        }
    func googleAuthSignin() {
        let googleConfig = GIDConfiguration(clientID: "338614670794-h4cgn4lr6kd996dj606et5m9jgojqfa7.apps.googleusercontent.com")
        self.googleSignIn.signIn(with: googleConfig, presenting: self) { user, error in
            if error != nil {
                            print("Something Want Wrong")
                        }
                        else {
                            guard let user = user else { return }
                            let googleId = user.userID!
                            let name = user.profile?.name
                            let email = user.profile?.email
                            let lastname = user.profile?.givenName
                            GIDSignIn.sharedInstance.disconnect()
                            GIDSignIn.sharedInstance.signOut()
                            
                            let params: NSDictionary = ["email":email!,
                                                        "password":"",
                                                        "first_name":name!,
                                                        "last_name":lastname!,
                                                        "device_type":"ios",
                                                        "google_id":googleId,
                                                        "register_type":"google",
                                                        "facebook_id":"",
                                                        "apple_id":"",
                                                        "mobile":"",
                                                        "token":getFCMToken(),
                                                        "theme_id": APP_THEMEID]
                            self.getUserRegistedData(params as! [String : Any])
                        }
                        guard error == nil else { return }
                        
                    }
    }
    func AppleSignup(){
            let request = ASAuthorizationAppleIDProvider().createRequest()
            request.requestedScopes = [.fullName, .email]
            let controller = ASAuthorizationController(authorizationRequests: [request])
            controller.delegate = self
            controller.presentationContextProvider = self
            controller.performRequests()
        }
    //MARK: - API
    
    func getUserRegistedData(_ param: [String:Any]){
        AIServiceManager.sharedManager.callPostApi(URL_Ragister, params: param, nil) { response in
            switch response.result{
            case let .success(result):
                HIDE_CUSTOM_LOADER()
                if let json = result as? [String:Any]{
                    let msg = json["message"] as? String
                    if msg == "fail" {
                        if let datas = json["data"] as? [String:Any]{
                            let massage = datas["message"] as? String
                            self.view.makeToast(massage!)
                        }
                    }else{
                        if let data = json["data"] as? [String:Any]{
                            let userid = data["id"] as? Int
                            UserDefaults.standard.set("\(userid!)", forKey: userDefaultsKeys.KEY_USERID)
                            let firstname = data["first_name"] as? String
                            UserDefaults.standard.set(firstname, forKey: userDefaultsKeys.KEY_FIRSTNAME)
                            let lastname = data["last_name"] as? String
                            UserDefaults.standard.set(lastname, forKey: userDefaultsKeys.KEY_LASTNAME)
                            UserDefaults.standard.set(firstname, forKey: userDefaultsKeys.KEY_FULLNAME)
                            let email = data["email"] as? String
                            UserDefaults.standard.set(email, forKey: userDefaultsKeys.KEY_EMAIL)
                            let phonenumber = data["mobile"] as? String
                            UserDefaults.standard.set(phonenumber, forKey: userDefaultsKeys.KEY_PHONE)
                            let image = data["image"] as? String
                            UserDefaults.standard.set("\(URL_BASE)/\(image!)", forKey: userDefaultsKeys.KEY_USERPROFILE)
                            if let token = data["token"] as? String{
                                setValueToUserDefaults(value: data as AnyObject, key: userDefaultsKeys.KEY_USER_REG_DATA)
                                setValueToUserDefaults(value: token as AnyObject, key: userDefaultsKeys.KEY_APP_TOKEN)
                                setValueToUserDefaults(value: true as AnyObject, key: userDefaultsKeys.KEY_IS_USER_LOGGED_IN)
                                let vc = iPhoneStoryBoard.instantiateViewController(withIdentifier: "MainTabbarVC") as! MainTabbarVC
                                self.navigationController?.pushViewController(vc, animated: true)
                        }
                            UserDefaults.standard.set([[String:Any]](), forKey: userDefaultsKeys.KEY_GESTUSEROBJ)
                    }else{
                            showAlertMessage(titleStr: Bundle.main.applicationName!, messageStr: msg!)
                    }
                           printD(json)
                    }
                }
            case let .failure(error):
                HIDE_CUSTOM_LOADER()
                printD("Error: \(error.localizedDescription)")
                break
            }
        }
    }
}
//MARK: - Extensions
extension LoginVC: ASAuthorizationControllerDelegate,ASAuthorizationControllerPresentationContextProviding {
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        switch authorization.credential {
        case let appleIDCredential as ASAuthorizationAppleIDCredential:
            
            let userIdentifier = appleIDCredential.user
            var Email = String()
            var Fullname = String()
            if appleIDCredential.email != nil
            {
                Email = appleIDCredential.email!
                UserDefaults.standard.set(Email, forKey: userDefaultsKeys.KEY_EMAIL)
            }
            else
            {
                Email = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_EMAIL) ?? "N/A"
            }
            //            print(appleIDCredential.fullName)
            if appleIDCredential.fullName?.givenName != nil
            {
                Fullname = (appleIDCredential.fullName?.givenName)! + (appleIDCredential.fullName?.familyName)!
                UserDefaults.standard.set(Fullname, forKey: userDefaultsKeys.KEY_FIRSTNAME)
            }
            else
            {
                Fullname = UserDefaults.standard.string(forKey: userDefaultsKeys.KEY_FIRSTNAME) ?? "N/A"
            }
            
            let params: [String:Any] = ["first_name":Fullname,
                                        "last_name":Fullname,
                                        "email":Email,
                                        "password":"",
                                        "mobile":"",
                                        "device_type":"ios",
                                        "register_type":"apple",
                                        "google_id":"",
                                        "facebook_id":"",
                                        "apple_id":userIdentifier,
                                        "token":getFCMToken(),
                                        "theme_id":APP_THEMEID]
            self.getUserRegistedData(params)
            break
        default:
            break
        }
    }
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
}
